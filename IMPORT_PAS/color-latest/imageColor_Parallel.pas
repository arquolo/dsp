const
  FUNC_DIR_HDR = 2;
  FUNC_DIR     = 1;
  FUNC_INV     =-1;
  FUNC_INV_HDR =-2;
  INTER_DIR = 1;
  INTER_INV =-1;

var
  matrix:^TMatrix;
  vector:^TVector;
  func  : TFunc;
  inter : TInter;

procedure procMatrixDir(idx:PtrInt; data:Pointer; item:TMultiThreadProcItem);
var
  x0,x1,x2: single;
  y,x: integer;
begin
  y:=idx div bmp.Width;
  x:=idx mod bmp.Width;
  x0:=bmp.Pixel[y][x][0];
  x1:=bmp.Pixel[y][x][1];
  x2:=bmp.Pixel[y][x][2];
  bmp.Pixel[y][x][0]:=x0*matrix^[0,0] + x1*matrix^[0,1] + x2*matrix^[0,2] - vector^[0];
  bmp.Pixel[y][x][1]:=x0*matrix^[1,0] + x1*matrix^[1,1] + x2*matrix^[1,2] - vector^[1];
  bmp.Pixel[y][x][2]:=x0*matrix^[2,0] + x1*matrix^[2,1] + x2*matrix^[2,2] - vector^[2];
end;

procedure procMatrixInv(idx:PtrInt; data:Pointer; item:TMultiThreadProcItem);
var
  x0,x1,x2: single;
  y,x: integer;
begin
  y:=idx div bmp.Width;
  x:=idx mod bmp.Width;
  x0:=bmp.Pixel[y][x][0] + vector^[0];
  x1:=bmp.Pixel[y][x][1] + vector^[1];
  x2:=bmp.Pixel[y][x][2] + vector^[2];
  bmp.Pixel[y][x][0]:=x0*matrix^[0,0] + x1*matrix^[0,1] + x2*matrix^[0,2];
  bmp.Pixel[y][x][1]:=x0*matrix^[1,0] + x1*matrix^[1,1] + x2*matrix^[1,2];
  bmp.Pixel[y][x][2]:=x0*matrix^[2,0] + x1*matrix^[2,1] + x2*matrix^[2,2];
end;

procedure procFunc(idx:PtrInt; data:Pointer; item:TMultiThreadProcItem);
var y,x: integer;
begin
  y:=idx div bmp.Width;
  x:=idx mod bmp.Width;
  func(bmp.Pixel[y][x][0]);
  func(bmp.Pixel[y][x][1]);
  func(bmp.Pixel[y][x][2]);
end;

procedure procInter(idx:PtrInt; data:Pointer; item:TMultiThreadProcItem);
var y,x:integer;
begin
  y:=idx div bmp.Width;
  x:=idx mod bmp.Width;
  inter(bmp.Pixel[y][x])
end;

procedure parallelMatrix(matrixID: integer);
begin
  if (matrixID>0) then begin
    matrix:=@arrMatrixDir[+matrixID];
    vector:=@arrVector[+matrixID];
    ProcThreadPool.DoParallel(@procMatrixDir,0,bmp.Size-1,nil);
  end;
  if (matrixID<0) then begin
    matrix:=@arrMatrixInv[-matrixID];
    vector:=@arrVector[-matrixID];
    ProcThreadPool.DoParallel(@procMatrixInv,0,bmp.Size-1,nil);
  end;
end;

procedure parallelFunc(funcID: integer);
begin
  case funcID of
    -2..-1 : func:=arrFunc[funcID];
     1.. 2 : func:=arrFunc[funcID];
    else     func:=arrFunc[0];
  end;
  ProcThreadPool.DoParallel(@procFunc,0,bmp.Size-1,nil);
end;

procedure parallelInter(interID: integer);
begin
  case interID of
  INTER_DIR:
    begin
      inter:=arrInterDir;
      ProcThreadPool.DoParallel(@procInter,0,bmp.Size-1);
    end;
  INTER_INV:
    begin
      inter:=arrInterInv;
      ProcThreadPool.DoParallel(@procInter,0,bmp.Size-1);
    end;
  end;
end;
