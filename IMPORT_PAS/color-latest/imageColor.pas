const
  CS_YCC_CL    = 1;
  CS_YCC       = 2;
  CS_XYZ       = 3;
  CS_XYZ_LUV   = 4;
  CS_XYZ_LAB   = 5;
  CS_XYZ_ICACB = 6;
  CS_XYZ_ICTCP = 7;

procedure icRgbToYcc_dir; //R'G'B' -> YCbCr             //done
procedure icRgbToYcc_inv; //YCbCr -> R'G'B'             //done

procedure icRgbToYccCl_dir; //R'G'B' -> YcCbcCrc        //done
procedure icRgbToYccCl_inv; //YcCbcCrc -> R'G'B'        //done

procedure icRgbToXyz_dir; //R'G'B' -> XYZ               //done
procedure icRgbToXyz_inv; //XYZ -> R'G'B'               //done

procedure icXyzToUcs_dir; //XYZ -> U*V*W*,L*u*v*        //done
procedure icXyzToUcs_inv; //U*V*W*,L*u*v* -> XYZ        //done

procedure icXyzToIcc_dir; //XYZ -> IPT,ICaCb,ICtCp,Lab  //done
procedure icXyzToIcc_inv; //IPT,ICaCb,ICtCp,Lab -> XYZ  //done

procedure icXyzToDin_dir; //XYZ -> DIN99x,IPTe          //done
procedure icXyzToDin_inv; //DIN99x,IPTe -> XYZ          //done

procedure icInitialize(csID,tfID,pvID,rwID,hdrID:integer; Lmin,Lmax:single);
procedure icInitializeTarget(csID: integer);

implementation

procedure icRgbToYcc_dir; begin parallelMatrix(+1); end;
procedure icRgbToYcc_inv; begin parallelMatrix(-1); end;

procedure icRgbToYccCl_dir; begin parallelInter(INTER_DIR); end;
procedure icRgbToYccCl_inv; begin parallelInter(INTER_INV); end;

procedure icRgbToXyz_dir; begin
  parallelFunc  (FUNC_DIR);
  parallelMatrix(+1);
end;
procedure icRgbToXyz_inv; begin
  parallelMatrix(-1);
  parallelFunc  (FUNC_INV);
end;

procedure icXyzToUcs_dir; begin
  parallelMatrix(+2);
  parallelInter (INTER_DIR);
end;
procedure icXyzToUcs_inv; begin
  parallelInter (INTER_INV);
  parallelMatrix(-2);
end;

procedure icXyzToIcc_dir; begin
  parallelMatrix(+2);
  parallelFunc  (FUNC_DIR_HDR);
  parallelMatrix(+3);
end;
procedure icXyzToIcc_inv; begin
  parallelMatrix(-3);
  parallelFunc  (FUNC_INV_HDR);
  parallelMatrix(-2);
end;

procedure icXyzToDin_dir; begin
  parallelMatrix(+2);
  parallelFunc  (FUNC_DIR_HDR);
  parallelMatrix(+3);
  parallelInter (INTER_DIR);
end;
procedure icXyzToDin_inv; begin
  parallelInter (INTER_INV);
  parallelMatrix(-3);
  parallelFunc  (FUNC_INV_HDR);
  parallelMatrix(-2);
end;

procedure icInitialize(csID,tfID,pvID,rwID,hdrID:integer; Lmin,Lmax:single);
begin
  icInitializeTransfer   (tfID,Lmin,Lmax);
  icInitializePrimaries  (pvID,rwID);
  icInitializeTransferHdr(hdrID);
  icInitializeTarget     (csID);
end;

procedure icInitializeTarget(csID: integer);
begin
  case csID of
  CS_YCC_CL:
    begin
      arrInterDir    :=@RgbToYccCl_dir;
      arrInterInv    :=@RgbToYccCl_inv;
    end;
  CS_YCC:
    begin
      arrMatrixDir[1]:= mRgbToYcc_dir;
      arrMatrixInv[1]:= mRgbToYcc_inv; arrVector[1]:=vNull;
    end;
  CS_XYZ:
    begin
      arrMatrixDir[1]:= mRgbToXyz_dir;
      arrMatrixInv[1]:= mRgbToXyz_inv; arrVector[1]:=vNull;
    end;
  CS_XYZ_LUV:
    begin
      arrMatrixDir[2]:= mXyzAdapt_dir;
      arrMatrixInv[2]:= mXyzAdapt_inv; arrVector[2]:=vNull;
      arrInterDir    :=@XyzToLuv_dir;
      arrInterInv    :=@XyzToLuv_inv;
    end;
  CS_XYZ_LAB:
    begin
      arrVector[2]   := vNull;
      arrMatrixDir[2]:= mXyzAdapt_dir;
      arrMatrixInv[2]:= mXyzAdapt_inv;
      arrFunc[+2]    :=@dirTransferHdr_Lab;
      arrFunc[-2]    :=@invTransferHdr_Lab;
      arrVector[3]   := vCieLab;
      arrMatrixDir[3]:= mCieLab;
      matrixInverse(arrMatrixDir[3],arrMatrixInv[3]);
    end;
  CS_XYZ_ICACB:
    begin
      arrVector[2]   := vNull;
      arrVector[3]   := vNull;
      arrMatrixDir[2]:= mLmsAb;
      arrMatrixDir[3]:= mIcacb;
      matrixInverse(arrMatrixDir[2],arrMatrixInv[2]);
      matrixInverse(arrMatrixDir[3],arrMatrixInv[3]);
    end;
  CS_XYZ_ICTCP:
    begin
      arrVector[2]   := vNull;
      arrVector[3]   := vNull;
      arrMatrixDir[2]:= mLmsTp;
      arrMatrixDir[3]:= mIctcp;
      matrixInverse(arrMatrixDir[2],arrMatrixInv[2]);
      matrixInverse(arrMatrixDir[3],arrMatrixInv[3]);
    end;
  end;
end;
