type
  singled   = array[0..1] of single;
  Dsingled = array[1..8] of singled;
  DPrims   = array[0..3] of singled;
  DDPrims   = array[0..4] of DPrims;

const
  PV_CIE        = 0;
  PV_BT601_NTSC = 1;
  PV_BT601_PAL  = 2;
  PV_BT709      = 3;
  PV_BT2020     = 4;

  RW_A   = 1;
  RW_B   = 2;
  RW_D50 = 3;
  RW_E   = 4;
  RW_D55 = 5;
  RW_D65 = 6;
  RW_C   = 7;
  RW_D75 = 8;

  WHITE: Dsingled=(
    (*, *)
    ...
    (*, *)
  );
  DB_PRIMS: DDPrims=(
    ((0.31271, 0.32902), (0.630,0.340), (0.310,0.595), (0.155,0.070)), //D65, bt601_525/bt601_ntsc, SMPTE_C
    ((0.31271, 0.32902), (0.640,0.330), (0.300,0.600), (0.150,0.060)), //D65, bt709, sRGB
    ((0.31271, 0.32902), (0.640,0.330), (0.290,0.600), (0.150,0.060)), //D65, bt601_625/bt601_pal
    ((0.31271, 0.32902), (0.708,0.292), (0.170,0.797), (0.131,0.046)), //D65, bt2020
    ((0.33333, 0.33333), (0.735,0.265), (0.274,0.717), (0.167,0.009))  //E, cie
  );

var
  prims: DPrims;  //current whitepoint and primaries (xy)
  whiteX, whiteZ, //XZ, Y is always 1
  whiteU, whiteV, //uvY
  Kr, Kg, Kb: single;

  mXyzAdapt_dir,          //(XYZ -> XdYdZd) transform matrix
  mXyzAdapt_inv,          //(XdYdZd -> XYZ) transform matrix

  mRgbToXyz_dir,          //(RGB -> XYZ) transform matrix
  mRgbToXyz_inv,          //(XYZ -> RGB) transform matrix

  mRgbToYcc_dir,          //(R'G'B' -> YCbCr) transform matrix
  mRgbToYcc_inv: TMatrix; //(YCbCr -> R'G'B') transform matrix

procedure icInitializePrimaries(pvID,rwID: integer);

implementation

procedure icInitializePrimaries(pvID,rwID: integer);
var c3sum: TVector;
begin
  if (pvID>=PV_CIE) and (pvID<=PV_BT2020) then prims:=DB_PRIMS[pvID];
  if (rwID>=RW_A) and (rwID<=RW_D75) then prims[0]:=WHITE[rwID];

  whiteX :=   prims[0][0]              / prims[0][1];
  whiteZ :=(1-prims[0][0]-prims[0][1]) / prims[0][1];
  whiteU := 4*prims[0][0] / (3-2*prims[0][0]+12*prims[0][1]);
  whiteV := 9*prims[0][1] / (3-2*prims[0][0]+12*prims[0][1]);

  c3sum[0]:=whiteX;
  c3sum[1]:=1;
  c3sum[2]:=whiteZ;

  matrixDiag   (mXyzAdapt_inv, c3sum);
  matrixInverse(mXyzAdapt_inv, mXyzAdapt_dir);
  begin
    mRgbToXyz_inv[0,0] := prims[1][0];
    mRgbToXyz_inv[0,1] := prims[2][0];
    mRgbToXyz_inv[0,2] := prims[3][0];

    mRgbToXyz_inv[1,0] := prims[1][1];
    mRgbToXyz_inv[1,1] := prims[2][1];
    mRgbToXyz_inv[1,2] := prims[3][1];

    mRgbToXyz_inv[2,0] := 1 - prims[1][0] - prims[1][1];
    mRgbToXyz_inv[2,1] := 1 - prims[2][0] - prims[2][1];
    mRgbToXyz_inv[2,2] := 1 - prims[3][0] - prims[3][1];
  end;
  matrixLinSolve(mRgbToXyz_inv, c3sum);
  matrixAmplify (mRgbToXyz_inv, c3sum);
  matrixInverse (mRgbToXyz_inv, mRgbToXyz_dir);

  Kr := mRgbToXyz_dir[1,0];
  Kg := mRgbToXyz_dir[1,1];
  Kb := mRgbToXyz_dir[1,2];
  begin
    mRgbToYcc_dir[0,0] := Kr;
    mRgbToYcc_dir[0,1] := Kg;
    mRgbToYcc_dir[0,2] := Kb;

    mRgbToYcc_dir[1,0] :=-0.5*Kr/(1-Kb);
    mRgbToYcc_dir[1,1] :=-0.5*Kg/(1-Kb);
    mRgbToYcc_dir[1,2] := 0.5;

    mRgbToYcc_dir[2,0] := 0.5;
    mRgbToYcc_dir[2,1] :=-0.5*Kg/(1-Kr);
    mRgbToYcc_dir[2,2] :=-0.5*Kb/(1-Kr);
  end;
  matrixInverse (mRgbToYcc_dir, mRgbToYcc_inv);
end;
