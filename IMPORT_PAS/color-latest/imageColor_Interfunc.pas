type TInter = procedure(var x: TVector);

var
  iPr, iNr, iPb, iNb,
  rPr, rNr, rPb, rNb: single; //function to compute that ain't designed yet
  arrInterDir,
  arrInterInv: TInter;

procedure RgbToYccCl_dir(var x:TVector);
var R,G,B, Yc,Cbc,Crc: single;
begin
  R:=x[0]; arrFunc[1](R);
  G:=x[1]; arrFunc[1](G);
  B:=x[2]; arrFunc[1](B);

  Yc:=R*Kr + G*Kg + B*Kb;
  arrFunc[-1](Yc);
  if (x[2]>Yc)
    then Cbc:=rPb*(x[2]-Yc)
    else Cbc:=rNb*(x[2]-Yc);
  if (x[0]>Yc)
    then Crc:=rPr*(x[0]-Yc)
    else Crc:=rNr*(x[0]-Yc);

  x[0]:=Yc;
  x[1]:=Cbc;
  x[2]:=Crc;
end;

procedure RgbToYccCl_inv(var x:TVector);
var Yc,Bc,Rc, R,G,B: single;
begin
  if (x[2]>0)
    then R:=x[0] + iPr*x[2]
    else R:=x[0] + iNr*x[2];
  if (x[1]>0)
    then B:=x[0] + iPb*x[1]
    else B:=x[0] + iNb*x[1];

  Yc:=x[0]; arrFunc[1](Yc);
  Bc:=B;    arrFunc[1](Bc);
  Rc:=R;    arrFunc[1](Rc);

  G:=(Yc - Bc*Kb - Rc*Kr)/Kg;
  arrFunc[-1](G);

  x[0]:=R;
  x[1]:=G;
  x[2]:=B;
end;


procedure XyzToLuv_dir(var x:TVector);
var Lstar,uu,vv,d: single;
begin
  Lstar:=x[1]; dirTransferHdr_Luv(Lstar);
  if (Lstar>0) then begin
    d:=x[0]+15*x[1]+3*x[2];
    uu:=4*x[0]/d;
    vv:=9*x[1]/d;
    x[0]:=Lstar;
    x[1]:=Lstar*(uu-whiteU)*13;
    x[2]:=Lstar*(vv-whiteV)*13;
  end else begin
    x[0]:=0;
    x[1]:=0;
    x[2]:=0;
  end;
end;

procedure XyzToLuv_inv(var x:TVector);
var u,v,Y: single;
begin
  Y:=x[0];
  u:=x[1]/13/x[0] + whiteU;
  v:=x[2]/13/x[0] + whiteV;
  invTransferHdr_Luv(Y);
  if (v<>0) then begin
    x[0] := Y * 2.25*u/v;
    x[1] := Y;
    x[2] := Y *(3.00-0.75*u-5.00*v)/v;
  end else begin
    x[0] := Y * whiteX;
    x[1] := Y;
    x[2] := Y * whiteZ;
  end;
end;

