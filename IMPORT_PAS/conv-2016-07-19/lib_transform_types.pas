type
    TColor  = array[0..2] of byte;
    TColorF = array[0..2] of single;
    
    TVector = class public
        id:array of double;
    end;
	
	TMask = class public
		n:array of TVector;
	end;
	
	TLift = class public
		stages: word;
		p,u: array of double;
	end;
	TMap = class public
		nZYX,nYX,nZ,nY,nX, //number of layers to pack
		sZ,sY,sX: word;    //spatial and temporal resolution
		px:array[0..2] of array[,,] of single;
	end;
	
	TMaskColor = class public
		dir: TMatrix;
		inv: TMatrix;
		cov: TMatrix;
		constructor Create(var m: TMap; adapt: boolean);
	end;

constructor TMaskColor.Create(var m: TMap; adapt: boolean);
begin
	if adapt then begin
		var n := m.sZ * m.sY * m.sX;
		var a, u, v: double;
		
		for var i := 0 to 2 do
		for var j := 0 to 2 do begin
			cov[i, j] := 0;
			dir[i, j] := (i = j ? 1 : 0);
			inv[i, j] := (i = j ? 1 : 0);			
		end;
		
		for var z := 0 to sZ - 1 do
		for var y := 0 to sY - 1 do
		for var x := 0 to sX - 1 do begin
			cov[0, 0] := m.px[0][z, y, x] * m.px[0][z, y, x];
			cov[0, 1] := m.px[0][z, y, x] * m.px[1][z, y, x];
			cov[0, 2] := m.px[0][z, y, x] * m.px[2][z, y, x];
			cov[1, 1] := m.px[1][z, y, x] * m.px[1][z, y, x];
			cov[1, 2] := m.px[1][z, y, x] * m.px[2][z, y, x];
			cov[2, 2] := m.px[2][z, y, x] * m.px[2][z, y, x];
		end;
		
		cov[1, 0] := cov[0, 1];
		cov[2, 0] := cov[0, 2];
		cov[2, 1] := cov[1, 2];
		for var i := 0 to 2 do
		for var j := 0 to 2 do
		    cov[i,j]/=n;
		
		u := cov[0, 0] - cov[1, 1];
		v := 2 * cov[0, 1];
		if abs(u*u - v*v) <= zero_threshold
		then a := Pi / 4
		else a := arctan(2 * u*v / (u*u - v*v));
		
		MPLeft (cov, MPRotate(0, 1, +a));
		MPLeft (dir, MPRotate(0, 1, +a));
		MPRight(cov, MPRotate(0, 1, -a));
		MPRight(inv, MPRotate(0, 1, -a));
		
		u := cov[0, 0]- cov[2, 2];
		v := 2 * cov[0, 2];
		if abs(u*u - v*v) <= zero_threshold
		then a := Pi / 4
		else a := arctan(2 * u*v / (u*u - v*v));
		
		MPLeft (cov, MPRotate(0, 2, +a));
		MPLeft (dir, MPRotate(0, 2, +a));
		MPRight(cov, MPRotate(0, 2, -a));
		MPRight(inv, MPRotate(0, 2, -a));
		
		u:=cov[1, 1] - cov[2,2];
		v:=2 * cov[1,2];
		if abs(u*u - v*v) <= zero_threshold
		then a := Pi / 4
		else a := arctan(2 * u*v / (u*u - v*v));

		MPLeft (cov, MPRotate(1, 2, +a));
		MPLeft (dir, MPRotate(1, 2, +a));
		MPRight(cov, MPRotate(1, 2, -a));
		MPRight(inv, MPRotate(1, 2, -a));
	else begin
	
	end;
end;
