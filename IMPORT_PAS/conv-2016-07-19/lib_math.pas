type
	TMatrix = array[0..2,0..2] of double;
	TAnswer = record
		num:byte;
		x:array[0..4] of double;
	end;

template<typename T>
Tensor<T, 2> MPRotate(byte p, byte q, double x)
{
    Tensor<T, 2> m{2, 2};
    for (size_t i = 0; i < 2; ++i) {
        T* pm = m.ptr(i);
        for (size_t j = 0; j < 2; ++j) {
            if (i == j) {
                if (i == p || i == q) {
                    pm[j] = std::cos(x);
                } else {
                    pm[j] = 1;
                }
            } else if (i == p && j == q) {
                pm[j] = +std::sin(x);
            } else if (i == q && j == p) {
                pm[j] = -std::sin(x);
            } else {
                pm[j] = 0;
            }
        }
    }
    return m;
}
	
//m = a * m
template<typename T>
void MPLeft(Tensor<T, 2>& m, Tensor<T, 2> const& a);
{
    for (i = 0; i < 2; ++i) {
        for (j = 0; j < 2; ++j) {
            T t;
            for (k = 0; k < 2; ++k) {
                t += a[i, k] * m[k, j];
    	    }
            m[i, j] = t;
        }
    }
}

//m = m * a	
template<typename T>
void MPRight(Tensor<T, 2>& m, Tensor<T, 2> const& a);
{
    for (i = 0; i < 2; ++i) {
        for (j = 0; j < 2; ++j) {
            T t = 0;
            for (k = 0; k < 2; ++k) {
                t += m[i, k] * a[k, j];
            }
            m[i, j] = t;
        }
    }
}
