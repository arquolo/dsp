var
  p0,p1: picture;
  bmp0,bmp1,bmp1T,bmp2T,bmp2: TMatrix;
  str: string;
  mult, area: real;
  h0, h1,
  w0, w1,
  mdid,wdid,idid,ddid: integer;
  
begin
  setWindowSize(350,0); centerWindow;
  //window.Caption:='Enter filename';
  //readLn(str);
  str:=paramStr(1);
  
  try        p0:=new Picture(str);
  except try p0:=new Picture(str+'.jpg');
  except try p0:=new Picture(str+'.png');
  except     p0:=new Picture(str+'.bmp');
  end;end;end;
  
  w0:=p0.width;  
  h0:=p0.height; 
  
  window.Caption:='('+
    intToStr(w0)+'x'+intToStr(h0)+') Enter scaling';
  read(mult);
  
  w1:=floor(w0*mult);
  h1:=floor(h0*mult);
  
  setWindowSize(350,350);
  writeLn('Choose mode:');
  writeLn(' 0-Nearest');
  writeLn(' 1-Linear');
  writeLn(' 2-Cubic');
  writeLn(' 3-GenSpline, generator:');
  writeLn('   0-Quad, radius: 1..'+intToStr(RADMAX)+', smoothness: 0..Infinity');
  writeLn('   1-Cos,  radius: 1..'+intToStr(RADMAX)+', smoothness: 0..Infinity');
  writeLn('   2-PCos, radius: 1..'+intToStr(RADMAX)+', smoothness: 0..5');
  writeLn(' 4-Sinc, windows:');
  writeLn('   0-Rect');
  writeLn('   1-Triangular');
  writeLn('   2-Parabolic');
  writeLn('   3-Biweight');
  writeLn('   4-Triweight');
  writeLn('   5-Tricube');
  writeLn('   6-Cosine');
  writeLn('   7-Sinc (Lanczos)');
  writeLn('   radius: 1..'+intToStr(RADMAX));
  //writeLn('integral: 0 / 1');
  //writeLn('method: 1/2 (1D/2D)');
  window.Caption:=
    intToStr(w0)+'x'+intToStr(h0)+'->'+
    intToStr(w1)+'x'+intToStr(h1);
  read(mdid);
  case mdid of
    4:begin
      window.Caption:='Sinc';
      read(wdid);
      case wdid of
        7:begin
          window.Caption+='-Sinc';
          rsSpline:=sSincSinc;
          end;
        6:begin
          window.Caption+='-Cosine';
          rsSpline:=sSincCosine;
          end;
        5:begin
          window.Caption+='-Tricube';
          rsSpline:=sSincTricube;
          end;
        4:begin
          window.Caption+='-Triweight';
          rsSpline:=sSincTriweight;
          end;
        3:begin
          window.Caption+='-Biweight';
          rsSpline:=sSincBiweight;
          end;
        2:begin
          window.Caption+='-Parabolic';
          rsSpline:=sSincParabolic;
          end;
        1:begin
          window.Caption+='-Triangular';
          rsSpline:=sSincTriangular;
          end;
        else begin
          window.Caption+='-Rect';
          rsSpline:=sSincRect;
        end;
      end;
      read(sgRadius);
      window.Caption+='-'+intToStr(sgRadius);
      end;
    3:begin
      window.Caption:='GenSpline';
      read(wdid); window.Caption:='GS';
      case wdid of
        2:begin
          window.Caption+='-PCos';
          sgSmp:=sgPCos;
          sgGen:=sgPCosGen;
          end;        
        1:begin
          window.Caption+='-Cos';
          sgSmp:=sgCos;
          sgGen:=sgCosGen;
          end;
        else begin
          window.Caption+='-Quad';
          sgSmp:=sgQuad;
          sgGen:=sgQuadGen;
        end;
      end;
      read(sgRadius); window.Caption+='-'+intToStr(sgRadius);
      read(sgSmooth); window.Caption+='x'+intToStr(sgSmooth);
      sgGenerate;
      rsSpline:=sgSpline;
      end;
    2:begin
      window.Caption:='Cubic';
      sgRadius:=2;
      rsSpline:=sCubic;
      end;
    1:begin
      window.Caption:='Linear';
      sgRadius:=1;
      rsSpline:=sLinear;
      end;
    else begin
      window.Caption:='Nearest';
      rsSpline:=sNearest;
      sgRadius:=1;
    end;
  end;
  
  idid:=1; //read(idid);
  if (idid=1) then window.Caption+='-int';
  ddid:=1; //read(ddid);
  if (ddid=2) then window.Caption+='-2D' else window.Caption+='-1D';
  
  pwLoadPicture(p0,bmp0,(idid>0));
  pwSavePicture(p0,bmp0,(idid>0));
  
  setWindowSize(max(350,w0),h0); centerWindow;
  p0.Draw(0,0);
  
  if (idid>0) then begin
    area:=mult*mult;
    if (ddid=2) then begin
      setLength(bmp2,h1+1,w1+1);
      
      rsInitialize(bmp0,bmp2);
      rsPrecomp;
      mIntegral(bmp0);
      {$omp parallel for}
      for var y:=0 to h1 do
      for var x:=0 to w1 do bmp2[y,x] := area * getMid(bmp0,y,x);
      mDerival(bmp2);
    end else begin
      setLength(bmp1, h0+1,w1+1);
      setLength(bmp2T,w1+1,h1+1);
      
      rsInitialize(bmp0,bmp1);
      rsPrecompHorz;
      mIntegralHorz(bmp0);
      {$omp parallel for}
      for var y:=0 to h0 do
      for var x:=0 to w1 do bmp1[y,x] := mult * getMidHorz(bmp0,y,x);
      mDerivalHorz(bmp1);
      
      mTranspose(bmp1,bmp1T);
      
      rsInitialize(bmp1T,bmp2T);
      rsPrecompHorz;
      mIntegralHorz(bmp1T);
      {$omp parallel for}
      for var x:=0 to w1 do
      for var y:=0 to h1 do bmp2T[x,y] := mult * getMidHorz(bmp1T,x,y);
      mDerivalHorz(bmp2T);
      
      mTranspose(bmp2T,bmp2);
    end;
  end else begin
    if (ddid=2) then begin
      setLength(bmp2,h1,w1);
      
      rsInitialize(bmp0,bmp2);
      rsPrecomp;
      
      {$omp parallel for}
      for var y:=0 to h1-1 do
      for var x:=0 to w1-1 do bmp2[y,x]:=getMid(bmp0,y,x);
    end else begin
      setLength(bmp1, h0,w1);
      setLength(bmp2T,w1,h1);
      
      rsInitialize(bmp0,bmp1);
      rsPrecompHorz;
      {$omp parallel for}
      for var y:=0 to h0-1 do
      for var x:=0 to w1-1 do bmp1[y,x]:=getMidHorz(bmp0,y,x);
      
      mTranspose(bmp1,bmp1T);
      
      rsInitialize(bmp1T,bmp2T);
      rsPrecompHorz;
      {$omp parallel for}
      for var x:=0 to w1-1 do
      for var y:=0 to h1-1 do bmp2T[x,y]:=getMidHorz(bmp1T,x,y);
    
      mTranspose(bmp2T,bmp2);
    end;
  end;
 
  p1:=new Picture(w1,h1);  
  pwSavePicture(p1,bmp2,(idid>0));
  
  clearWindow(clBlack);
  setWindowSize(max(350,w1),h1); centerWindow;
  p1.Draw(0,0);
end.
