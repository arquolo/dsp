procedure rsInitialize(const bmp1,bmp2:TMatrix);
var
  t:integer;
begin
  rsHMax:=Length(bmp1,0)-1;
  rsHei2:=Length(bmp2,0)-1;
  t:=gcd(rsHMax,rsHei2);
  rsHei1:=rsHMax div t;
  rsHei2:=rsHei2 div t;
  
  rsWMax:=Length(bmp1,1)-1;
  rsWid2:=Length(bmp2,1)-1;
  t:=gcd(rsWMax,rsWid2);
  rsWid1:=rsWMax div t;
  rsWid2:=rsWid2 div t;
end;

procedure rsPrecomp;
begin
  setLength(rsSplineVert,sgRadius*rsHei2+1);
  setLength(rsSplineHorz,sgRadius*rsWid2+1);
  for var i:=0 to sgRadius*rsHei2 do rsSplineVert[i]:=rsSpline(i/rsHei2);
  for var i:=0 to sgRadius*rsWid2 do rsSplineHorz[i]:=rsSpline(i/rsWid2);
end;

function getMid(var bmp:TMatrix; y1,x1:integer):real;
var
  vx,vy: array[1-RADMAX..RADMAX] of integer;
  z    : array[1-RADMAX..RADMAX,1-RADMAX..RADMAX] of real;
  dx,dy,
  fx,fy: integer;
  tm1  : real;
  tm2  : real:=0;
begin
  dy := (y1*rsHei1) div rsHei2; fy := (y1*rsHei1) mod rsHei2;
  dx := (x1*rsWid1) div rsWid2; fx := (x1*rsWid1) mod rsWid2;
  for var i:=0 to sgRadius-1 do begin
    vx[ -i]:=max(dx-i,0);        vy[ -i]:=max(dy-i,0);
    vx[1+i]:=min(dx+i+1,rsWMax); vy[1+i]:=min(dy+i+1,rsHMax);
  end;
  for var yw:=1-sgRadius to sgRadius do
  for var xw:=1-sgRadius to sgRadius do
    z[yw,xw]:=bmp[vy[yw],vx[xw]];
  for var yw:=1-sgRadius to sgRadius do begin
    tm1:=0;
    for var xw:=1-sgRadius to sgRadius do
      tm1 += rsSplineHorz[abs(xw*rsWid2-fx)]*z[yw,xw];
    tm2 += rsSplineVert[abs(yw*rsHei2-fy)]*tm1;
  end;
  result:=tm2;
end;
