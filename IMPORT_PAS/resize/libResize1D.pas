procedure rsInitializeHorz(const bmp1,bmp2:TMatrix);
begin
  rsHMax:=Length(bmp1,0)-1;
  
  rsWMax:=Length(bmp1,1)-1;
  rsWid2:=Length(bmp2,1)-1;
  var t:=gcd(rsWMax,rsWid2);
  rsWid1:=rsWMax div t;
  rsWid2:=rsWid2 div t;
end;

procedure rsInitializeVert(const bmp1,bmp2:TMatrix);
begin
  rsHMax:=Length(bmp1,0)-1;
  rsHei2:=Length(bmp2,0)-1;
  var t:=gcd(rsHMax,rsHei2);
  rsHei1:=rsHMax div t;
  rsHei2:=rsHei2 div t;
  
  rsWMax:=Length(bmp1,1)-1;
end;

procedure rsPrecompHorz;
begin
  setLength(rsSplineHorz,sgRadius*rsWid2+1);
  for var i:=0 to sgRadius*rsWid2 do rsSplineHorz[i]:=rsSpline(i/rsWid2);
end;

procedure rsPrecompVert;
begin
  setLength(rsSplineVert,sgRadius*rsHei2+1);
  for var i:=0 to sgRadius*rsHei2 do rsSplineVert[i]:=rsSpline(i/rsHei2);
end;

function getMidHorz(var bmp:TMatrix; y0,x1:integer): real;
var
  vx   : array[1-RADMAX..RADMAX] of integer;
  dx,fx: integer;
  tmp  : real:=0;
begin
  dx := (x1*rsWid1) div rsWid2;
  fx := (x1*rsWid1) mod rsWid2;
  for var i:=0 to sgRadius-1 do begin
    vx[ -i]:=max(dx-i,0);
    vx[1+i]:=min(dx+i+1,rsWMax);
  end;
  for var xw:=1-sgRadius to sgRadius do
    tmp += bmp[y0,vx[xw]] * rsSplineHorz[abs(xw*rsWid2-fx)];
  result:=tmp;
end;

function getMidVert(var bmp:TMatrix; y1,x0:integer): real;
var
  vy   : array[1-RADMAX..RADMAX] of integer;
  dy,fy: integer;
  tmp  : real:=0;
begin
  dy := (y1*rsHei1) div rsHei2;
  fy := (y1*rsHei1) mod rsHei2;
  for var i:=0 to sgRadius-1 do begin
    vy[ -i]:=max(dy-i,0);
    vy[1+i]:=min(dy+i+1,rsHMax);
  end;
  for var yw:=1-sgRadius to sgRadius do
    tmp += bmp[vy[yw],x0] * rsSplineVert[abs(yw*rsHei2-fy)];
  result:=tmp;
end;
