const
  RADMAX=10;
    
var
  rsHMax,
  rsWMax,
  rsHei1, rsHei2,
  rsWid1, rsWid2: integer;
  rsSpline: TSpline;
  rsSplineHorz,
  rsSplineVert: TVector;
