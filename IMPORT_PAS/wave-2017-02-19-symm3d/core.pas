unit core;
uses graphabc;

var
  th: array of smallint;
  fx:text;
  k,co:integer;
  w,h,m: array of integer;
  p0, p1, pT: array of picture;
  kind,str:string;
  mode:integer;

function quant(c:color;i:integer):color;
  var r,g,b:smallint;
  begin
    r:=((c.R-128) div th[i]) * th[i] + 128;
	g:=((c.G-128) div th[i]) * th[i] + 128;
	b:=((c.B-128) div th[i]) * th[i] + 128;
    result:=RGBs(r,g,b);
  end;

function sharp(c:color;i:integer):color;
  var r,g,b:smallint;
  begin
    r:=2*c.R-128; 
	g:=2*c.G-128;
	b:=2*c.B-128;
	result:=RGBs(r,g,b);
  end;

function smooth(c:color;i:integer):color;
  var r,g,b:smallint;
  begin
    r:=c.R div 2+64; 
	g:=c.G div 2+64;
	b:=c.B div 2+64;
	result:=RGBs(r,g,b);
  end;

procedure morph;
  begin
    window.caption:=kind+'_x'+inttostr(k);
    //for var i:=0 to m[0]-1 do begin pT[i].Draw(0,0); readln; end;
    case mode of
      0://packing, artefact check
      for var i:=0 to k-1 do begin
	    for var z:=0 to m[i+1]-1 do for var y:=0 to h[i+1]-1 do for var x:=w[i+1] to w[i]-1 do pT[z].PutPixel(x,y,quant(pT[z].GetPixel(x,y),i));
	    for var z:=0 to m[i+1]-1 do for var y:=h[i+1] to h[i]-1 do for var x:=0 to w[i]-1 do pT[z].PutPixel(x,y,quant(pT[z].GetPixel(x,y),i));
        //for var z:=m[i+1] to m[i]-1 do for var y:=0 to h[i]-1 do for var x:=0 to w[i]-1 do pT[z].PutPixel(x,y,quant(pT[z].GetPixel(x,y),i));
      end;
      1://sharp
      for var i:=0 to k-1 do begin
        for var z:=0 to m[i+1]-1 do for var y:=0 to h[i+1]-1 do for var x:=w[i+1] to w[i]-1 do pT[z].PutPixel(x,y,sharp(pT[z].GetPixel(x,y),i));
	    for var z:=0 to m[i+1]-1 do for var y:=h[i+1] to h[i]-1 do for var x:=0 to w[i]-1 do pT[z].PutPixel(x,y,sharp(pT[z].GetPixel(x,y),i));
        //for var z:=m[i+1] to m[i]-1 do for var y:=0 to h[i]-1 do for var x:=0 to w[i]-1 do pT[z].PutPixel(x,y,sharp(pT[z].GetPixel(x,y),i));
      end;
      -1://smooth
      for var i:=0 to k-1 do begin
        for var z:=0 to m[i+1]-1 do for var y:=0 to h[i+1]-1 do for var x:=w[i+1] to w[i]-1 do pT[z].PutPixel(x,y,smooth(pT[z].GetPixel(x,y),i));
	    for var z:=0 to m[i+1]-1 do for var y:=h[i+1] to h[i]-1 do for var x:=0 to w[i]-1 do pT[z].PutPixel(x,y,smooth(pT[z].GetPixel(x,y),i));
        //for var z:=m[i+1] to m[i]-1 do for var y:=0 to h[i]-1 do for var x:=0 to w[i]-1 do pT[z].PutPixel(x,y,smooth(pT[z].GetPixel(x,y),i));
      end;
      else//packing, artefact check
      for var i:=0 to k-1 do begin
        for var z:=0 to m[i+1]-1 do for var y:=0 to h[i+1]-1 do for var x:=w[i+1] to w[i]-1 do pT[z].PutPixel(x,y,thres(pT[z].GetPixel(x,y),i));
	    for var z:=0 to m[i+1]-1 do for var y:=h[i+1] to h[i]-1 do for var x:=0 to w[i]-1 do pT[z].PutPixel(x,y,thres(pT[z].GetPixel(x,y),i));
        //for var z:=m[i+1] to m[i]-1 do for var y:=0 to h[i]-1 do for var x:=0 to w[i]-1 do pT[z].PutPixel(x,y,thres(pT[z].GetPixel(x,y),i));
      end;
    end;
    for var i:=0 to m[0]-1 do begin pT[i].Draw(0,0); readln; end;
  end;

initialization
  assign(fx,'settings.txt'); reset(fx); readln(fx,k); readln(fx,mode); close(fx);
  setlength(w,k+1);
  setlength(h,k+1);
  setlength(m,k+1);
  readln(m[0]);
  setlength(th,k);
  setlength(p0,m[0]);
  setlength(p1,m[0]);
  setlength(pT,m[0]);
  for var i:=0 to m[0]-1 do p0[i] := new Picture((i<10?inttostr(0):'')+(i<100?inttostr(0):'')+inttostr(i)+'.png');
  w[0] := p0[0].Width; 
  h[0] := p0[0].Height;
  th[k-1]:=2; for var i:=k-2 downto 0 do th[i]:=2*th[i+1];
  for var i:=0 to k-1 do begin
    w[i+1]:=w[i] div 2;
	h[i+1]:=h[i] div 2;
	m[i+1]:=m[i] div 2;
  end;
  setwindowsize(w[0], h[0]+20);
  centerwindow;
  //p0.Draw(0,0);
  
  for var i:=0 to m[0]-1 do begin
    pT[i] := new Picture(w[0], h[0]);
    p1[i] := new Picture(w[0], h[0]);
  end;
  
  for var z:=0 to m[0]-1 do 
  for var y:=0 to h[0]-1 do
  for var x:=0 to w[0]-1 do pT[z].PutPixel(x,y,p0[z].GetPixel(x,y));
finalization
  for var z:=0 to m[0]-1 do
  for var y:=0 to h[0]-1 do
  for var x:=0 to w[0]-1 do p1[z].PutPixel(x,y,pT[z].GetPixel(x,y));
  co:=0;
  while true do begin
    readln;
    p1[co].Draw(0,0);
	  co:=(co+1) mod m[0];
  end;
end.
