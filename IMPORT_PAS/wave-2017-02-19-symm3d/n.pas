uses core,graphabc;
var
  pT1: array of picture;
  c000,c001,c010,c011,c100,c101,c110,c111,
  p000,p001,p010,p011,p100,p101,p110,p111: color;
  r, g, b: smallint;
  
begin
  kind:='n';
  setlength(pT1,m[0]);
  for var i:=0 to m[0]-1 do pT1[i]:= new Picture(w[0], h[0]);
  
  for var i:=0 to k-1 do begin
    for var z := 0 to m[i+1]-1 do
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c000 := pT[ 2*z+1         ].GetPixel( 2*x+1,          2*y+1         ); p000:=RGBs(256-c000.R,256-c000.G,256-c000.B);
      c001 := pT[ 2*z+1         ].GetPixel((2*x+2)mod w[i], 2*y+1         ); p001:=RGBs(256-c001.R,256-c001.G,256-c001.B);
      c010 := pT[ 2*z+1         ].GetPixel( 2*x+1,         (2*y+2)mod h[i]); p010:=RGBs(256-c010.R,256-c010.G,256-c010.B);
      c011 := pT[ 2*z+1         ].GetPixel((2*x+2)mod w[i],(2*y+2)mod h[i]); p011:=RGBs(256-c011.R,256-c011.G,256-c011.B);
      c100 := pT[(2*z+2)mod m[i]].GetPixel( 2*x+1,          2*y+1         ); p100:=RGBs(256-c100.R,256-c100.G,256-c100.B);
      c101 := pT[(2*z+2)mod m[i]].GetPixel((2*x+2)mod w[i], 2*y+1         ); p101:=RGBs(256-c101.R,256-c101.G,256-c101.B);
      c110 := pT[(2*z+2)mod m[i]].GetPixel( 2*x+1,         (2*y+2)mod h[i]); p110:=RGBs(256-c110.R,256-c110.G,256-c110.B);
      c111 := pT[(2*z+2)mod m[i]].GetPixel((2*x+2)mod w[i],(2*y+2)mod h[i]); p111:=RGBs(256-c111.R,256-c111.G,256-c111.B);
       r := (3*p000.R + c001.R + c010.R + c011.R + c100.R + c101.R + c110.R + c111.R) div 10;
       g := (3*p000.G + c001.G + c010.G + c011.G + c100.G + c101.G + c110.G + c111.G) div 10;
       b := (3*p000.B + c001.B + c010.B + c011.B + c100.B + c101.B + c110.B + c111.B) div 10;
      pT1[ 2*z+1         ].PutPixel( 2*x+1,          2*y+1,RGBs(r,g,b));
       r := (c000.R + 3*p001.R + c010.R + c011.R + c100.R + c101.R + c110.R + c111.R) div 10;
       g := (c000.G + 3*p001.G + c010.G + c011.G + c100.G + c101.G + c110.G + c111.G) div 10;
       b := (c000.B + 3*p001.B + c010.B + c011.B + c100.B + c101.B + c110.B + c111.B) div 10;
      pT1[ 2*z+1         ].PutPixel((2*x+2)mod w[i], 2*y+1,RGBs(r,g,b));
       r := (c000.R + c001.R + 3*p010.R + c011.R + c100.R + c101.R + c110.R + c111.R) div 10;
       g := (c000.G + c001.G + 3*p010.G + c011.G + c100.G + c101.G + c110.G + c111.G) div 10;
       b := (c000.B + c001.B + 3*p010.B + c011.B + c100.B + c101.B + c110.B + c111.B) div 10;
      pT1[ 2*z+1         ].PutPixel( 2*x+1,         (2*y+2)mod h[i],RGBs(r,g,b));
       r := (c000.R + c001.R + c010.R + 3*p011.R + c100.R + c101.R + c110.R + c111.R) div 10;
       g := (c000.G + c001.G + c010.G + 3*p011.G + c100.G + c101.G + c110.G + c111.G) div 10;
       b := (c000.B + c001.B + c010.B + 3*p011.B + c100.B + c101.B + c110.B + c111.B) div 10;
      pT1[ 2*z+1         ].PutPixel((2*x+2)mod w[i],(2*y+2)mod h[i],RGBs(r,g,b));
       r := (c000.R + c001.R + c010.R + c011.R + 3*p100.R + c101.R + c110.R + c111.R) div 10;
       g := (c000.G + c001.G + c010.G + c011.G + 3*p100.G + c101.G + c110.G + c111.G) div 10;
       b := (c000.B + c001.B + c010.B + c011.B + 3*p100.B + c101.B + c110.B + c111.B) div 10;
      pT1[(2*z+2)mod m[i]].PutPixel( 2*x+1,          2*y+1,RGBs(r,g,b));
       r := (c000.R + c001.R + c010.R + c011.R + c100.R + 3*p101.R + c110.R + c111.R) div 10;
       g := (c000.G + c001.G + c010.G + c011.G + c100.G + 3*p101.G + c110.G + c111.G) div 10;
       b := (c000.B + c001.B + c010.B + c011.B + c100.B + 3*p101.B + c110.B + c111.B) div 10;
      pT1[(2*z+2)mod m[i]].PutPixel((2*x+2)mod w[i], 2*y+1,RGBs(r,g,b));
       r := (c000.R + c001.R + c010.R + c011.R + c100.R + c101.R + 3*p110.R + c111.R) div 10;
       g := (c000.G + c001.G + c010.G + c011.G + c100.G + c101.G + 3*p110.G + c111.G) div 10;
       b := (c000.B + c001.B + c010.B + c011.B + c100.B + c101.B + 3*p110.B + c111.B) div 10;
      pT1[(2*z+2)mod m[i]].PutPixel( 2*x+1,         (2*y+2)mod h[i],RGBs(r,g,b));
       r := (c000.R + c001.R + c010.R + c011.R + c100.R + c101.R + c110.R + 3*p111.R) div 10;
       g := (c000.G + c001.G + c010.G + c011.G + c100.G + c101.G + c110.G + 3*p111.G) div 10;
       b := (c000.B + c001.B + c010.B + c011.B + c100.B + c101.B + c110.B + 3*p111.B) div 10;
      pT1[(2*z+2)mod m[i]].PutPixel((2*x+2)mod w[i],(2*y+2)mod h[i],RGBs(r,g,b));
    end;
	for var z := 0 to m[i+1]-1 do
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c000 := pT1[2*z  ].GetPixel(2*x,  2*y  );
      c001 := pT1[2*z  ].GetPixel(2*x+1,2*y  );
      c010 := pT1[2*z  ].GetPixel(2*x,  2*y+1);
      c011 := pT1[2*z  ].GetPixel(2*x+1,2*y+1);
      c100 := pT1[2*z+1].GetPixel(2*x,  2*y  );
      c101 := pT1[2*z+1].GetPixel(2*x+1,2*y  );
      c110 := pT1[2*z+1].GetPixel(2*x,  2*y+1);
      c111 := pT1[2*z+1].GetPixel(2*x+1,2*y+1);
       r :=(5*(c000.R + c001.R + c010.R + c011.R + c100.R + c101.R + c110.R + c111.R)) div 16 - 192;
       g :=(5*(c000.G + c001.G + c010.G + c011.G + c100.G + c101.G + c110.G + c111.G)) div 16 - 192;
       b :=(5*(c000.B + c001.B + c010.B + c011.B + c100.B + c101.B + c110.B + c111.B)) div 16 - 192;
      pT[z       ].PutPixel(x,       y,       RGBs(r,g,b));
       r :=(5*(c000.R - c001.R + c010.R - c011.R + c100.R - c101.R + c110.R - c111.R)) div 16 + 128;
       g :=(5*(c000.G - c001.G + c010.G - c011.G + c100.G - c101.G + c110.G - c111.G)) div 16 + 128;
       b :=(5*(c000.B - c001.B + c010.B - c011.B + c100.B - c101.B + c110.B - c111.B)) div 16 + 128;
      pT[z       ].PutPixel(x+w[i+1],y,       RGBs(r,g,b));
       r :=(5*(c000.R + c001.R - c010.R - c011.R + c100.R + c101.R - c110.R - c111.R)) div 16 + 128;
       g :=(5*(c000.G + c001.G - c010.G - c011.G + c100.G + c101.G - c110.G - c111.G)) div 16 + 128;
       b :=(5*(c000.B + c001.B - c010.B - c011.B + c100.B + c101.B - c110.B - c111.B)) div 16 + 128;
      pT[z       ].PutPixel(x,       y+h[i+1],RGBs(r,g,b));
       r :=(5*(c000.R - c001.R - c010.R + c011.R + c100.R - c101.R - c110.R + c111.R)) div 16 + 128;
       g :=(5*(c000.G - c001.G - c010.G + c011.G + c100.G - c101.G - c110.G + c111.G)) div 16 + 128;
       b :=(5*(c000.B - c001.B - c010.B + c011.B + c100.B - c101.B - c110.B + c111.B)) div 16 + 128;
      pT[z       ].PutPixel(x+w[i+1],y+h[i+1],RGBs(r,g,b));
       r :=(5*(c000.R + c001.R + c010.R + c011.R - c100.R - c101.R - c110.R - c111.R)) div 16 + 128;
       g :=(5*(c000.G + c001.G + c010.G + c011.G - c100.G - c101.G - c110.G - c111.G)) div 16 + 128;
       b :=(5*(c000.B + c001.B + c010.B + c011.B - c100.B - c101.B - c110.B - c111.B)) div 16 + 128;
      pT[z+m[i+1]].PutPixel(x,       y,       RGBs(r,g,b));
       r :=(5*(c000.R - c001.R + c010.R - c011.R - c100.R + c101.R - c110.R + c111.R)) div 16 + 128;
       g :=(5*(c000.G - c001.G + c010.G - c011.G - c100.G + c101.G - c110.G + c111.G)) div 16 + 128;
       b :=(5*(c000.B - c001.B + c010.B - c011.B - c100.B + c101.B - c110.B + c111.B)) div 16 + 128;
      pT[z+m[i+1]].PutPixel(x+w[i+1],y,       RGBs(r,g,b));
       r :=(5*(c000.R + c001.R - c010.R - c011.R - c100.R - c101.R + c110.R + c111.R)) div 16 + 128;
       g :=(5*(c000.G + c001.G - c010.G - c011.G - c100.G - c101.G + c110.G + c111.G)) div 16 + 128;
       b :=(5*(c000.B + c001.B - c010.B - c011.B - c100.B - c101.B + c110.B + c111.B)) div 16 + 128;
      pT[z+m[i+1]].PutPixel(x,       y+h[i+1],RGBs(r,g,b));
       r :=(5*(c000.R - c001.R - c010.R + c011.R - c100.R + c101.R + c110.R - c111.R)) div 16 + 128;
       g :=(5*(c000.G - c001.G - c010.G + c011.G - c100.G + c101.G + c110.G - c111.G)) div 16 + 128;
       b :=(5*(c000.B - c001.B - c010.B + c011.B - c100.B + c101.B + c110.B - c111.B)) div 16 + 128;
      pT[z+m[i+1]].PutPixel(x+w[i+1],y+h[i+1],RGBs(r,g,b));
    end;
  end;
  
  morph;
  
  for var i:=k-1 downto 0 do begin
    for var z := 0 to m[i+1]-1 do
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c000 := pT[z       ].GetPixel(x,       y       );
      c001 := pT[z       ].GetPixel(x+w[i+1],y       );
      c010 := pT[z       ].GetPixel(x,       y+h[i+1]);
      c011 := pT[z       ].GetPixel(x+w[i+1],y+h[i+1]);
      c100 := pT[z+m[i+1]].GetPixel(x,       y       );
      c101 := pT[z+m[i+1]].GetPixel(x+w[i+1],y       );
      c110 := pT[z+m[i+1]].GetPixel(x,       y+h[i+1]);
      c111 := pT[z+m[i+1]].GetPixel(x+w[i+1],y+h[i+1]);
       r :=(2*(c000.R + c001.R + c010.R + c011.R + c100.R + c101.R + c110.R + c111.R - 704)) div 5;
       g :=(2*(c000.G + c001.G + c010.G + c011.G + c100.G + c101.G + c110.G + c111.G - 704)) div 5;
       b :=(2*(c000.B + c001.B + c010.B + c011.B + c100.B + c101.B + c110.B + c111.B - 704)) div 5;
      pT1[2*z  ].PutPixel(2*x,  2*y,  RGBs(r,g,b));
       r :=(2*(c000.R - c001.R + c010.R - c011.R + c100.R - c101.R + c110.R - c111.R + 320)) div 5;
       g :=(2*(c000.G - c001.G + c010.G - c011.G + c100.G - c101.G + c110.G - c111.G + 320)) div 5;
       b :=(2*(c000.B - c001.B + c010.B - c011.B + c100.B - c101.B + c110.B - c111.B + 320)) div 5;
      pT1[2*z  ].PutPixel(2*x+1,2*y,  RGBs(r,g,b));
       r :=(2*(c000.R + c001.R - c010.R - c011.R + c100.R + c101.R - c110.R - c111.R + 320)) div 5;
       g :=(2*(c000.G + c001.G - c010.G - c011.G + c100.G + c101.G - c110.G - c111.G + 320)) div 5;
       b :=(2*(c000.B + c001.B - c010.B - c011.B + c100.B + c101.B - c110.B - c111.B + 320)) div 5;
      pT1[2*z  ].PutPixel(2*x,  2*y+1,RGBs(r,g,b));
       r :=(2*(c000.R - c001.R - c010.R + c011.R + c100.R - c101.R - c110.R + c111.R + 320)) div 5;
       g :=(2*(c000.G - c001.G - c010.G + c011.G + c100.G - c101.G - c110.G + c111.G + 320)) div 5;
       b :=(2*(c000.B - c001.B - c010.B + c011.B + c100.B - c101.B - c110.B + c111.B + 320)) div 5;
      pT1[2*z  ].PutPixel(2*x+1,2*y+1,RGBs(r,g,b));
       r :=(2*(c000.R + c001.R + c010.R + c011.R - c100.R - c101.R - c110.R - c111.R + 320)) div 5;
       g :=(2*(c000.G + c001.G + c010.G + c011.G - c100.G - c101.G - c110.G - c111.G + 320)) div 5;
       b :=(2*(c000.B + c001.B + c010.B + c011.B - c100.B - c101.B - c110.B - c111.B + 320)) div 5;
      pT1[2*z+1].PutPixel(2*x,  2*y,  RGBs(r,g,b));
       r :=(2*(c000.R - c001.R + c010.R - c011.R - c100.R + c101.R - c110.R + c111.R + 320)) div 5;
       g :=(2*(c000.G - c001.G + c010.G - c011.G - c100.G + c101.G - c110.G + c111.G + 320)) div 5;
       b :=(2*(c000.B - c001.B + c010.B - c011.B - c100.B + c101.B - c110.B + c111.B + 320)) div 5;
      pT1[2*z+1].PutPixel(2*x+1,2*y,  RGBs(r,g,b));
       r :=(2*(c000.R + c001.R - c010.R - c011.R - c100.R - c101.R + c110.R + c111.R + 320)) div 5;
       g :=(2*(c000.G + c001.G - c010.G - c011.G - c100.G - c101.G + c110.G + c111.G + 320)) div 5;
       b :=(2*(c000.B + c001.B - c010.B - c011.B - c100.B - c101.B + c110.B + c111.B + 320)) div 5;
      pT1[2*z+1].PutPixel(2*x,  2*y+1,RGBs(r,g,b));
       r :=(2*(c000.R - c001.R - c010.R + c011.R - c100.R + c101.R + c110.R - c111.R + 320)) div 5;
       g :=(2*(c000.G - c001.G - c010.G + c011.G - c100.G + c101.G + c110.G - c111.G + 320)) div 5;
       b :=(2*(c000.B - c001.B - c010.B + c011.B - c100.B + c101.B + c110.B - c111.B + 320)) div 5;
      pT1[2*z+1].PutPixel(2*x+1,2*y+1,RGBs(r,g,b));
    end;
    for var z := 0 to m[i+1]-1 do
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c000 := pT1[ 2*z+1         ].GetPixel( 2*x+1,          2*y+1         );
      c001 := pT1[ 2*z+1         ].GetPixel((2*x+2)mod w[i], 2*y+1         );
      c010 := pT1[ 2*z+1         ].GetPixel( 2*x+1,         (2*y+2)mod h[i]);
      c011 := pT1[ 2*z+1         ].GetPixel((2*x+2)mod w[i],(2*y+2)mod h[i]);
      c100 := pT1[(2*z+2)mod m[i]].GetPixel( 2*x+1,          2*y+1         );
      c101 := pT1[(2*z+2)mod m[i]].GetPixel((2*x+2)mod w[i], 2*y+1         );
      c110 := pT1[(2*z+2)mod m[i]].GetPixel( 2*x+1,         (2*y+2)mod h[i]);
      c111 := pT1[(2*z+2)mod m[i]].GetPixel((2*x+2)mod w[i],(2*y+2)mod h[i]);
       r :=(5*(-3*c000.R + c001.R + c010.R + c011.R + c100.R + c101.R + c110.R + c111.R))div 8 - 192;
       g :=(5*(-3*c000.G + c001.G + c010.G + c011.G + c100.G + c101.G + c110.G + c111.G))div 8 - 192;
       b :=(5*(-3*c000.B + c001.B + c010.B + c011.B + c100.B + c101.B + c110.B + c111.B))div 8 - 192;
      pT[ 2*z+1         ].PutPixel( 2*x+1,          2*y+1,         RGBs(r,g,b));
       r :=(5*( c000.R - 3*c001.R + c010.R + c011.R + c100.R + c101.R + c110.R + c111.R))div 8 - 192;
       g :=(5*( c000.G - 3*c001.G + c010.G + c011.G + c100.G + c101.G + c110.G + c111.G))div 8 - 192;
       b :=(5*( c000.B - 3*c001.B + c010.B + c011.B + c100.B + c101.B + c110.B + c111.B))div 8 - 192;
      pT[ 2*z+1         ].PutPixel((2*x+2)mod w[i], 2*y+1,         RGBs(r,g,b));
       r :=(5*( c000.R + c001.R - 3*c010.R + c011.R + c100.R + c101.R + c110.R + c111.R))div 8 - 192;
       g :=(5*( c000.G + c001.G - 3*c010.G + c011.G + c100.G + c101.G + c110.G + c111.G))div 8 - 192;
       b :=(5*( c000.B + c001.B - 3*c010.B + c011.B + c100.B + c101.B + c110.B + c111.B))div 8 - 192;
      pT[ 2*z+1         ].PutPixel( 2*x+1,         (2*y+2)mod h[i],RGBs(r,g,b));
       r :=(5*( c000.R + c001.R + c010.R - 3*c011.R + c100.R + c101.R + c110.R + c111.R))div 8 - 192;
       g :=(5*( c000.G + c001.G + c010.G - 3*c011.G + c100.G + c101.G + c110.G + c111.G))div 8 - 192;
       b :=(5*( c000.B + c001.B + c010.B - 3*c011.B + c100.B + c101.B + c110.B + c111.B))div 8 - 192;
      pT[ 2*z+1         ].PutPixel((2*x+2)mod w[i],(2*y+2)mod h[i],RGBs(r,g,b));
       r :=(5*( c000.R + c001.R + c010.R + c011.R - 3*c100.R + c101.R + c110.R + c111.R))div 8 - 192;
       g :=(5*( c000.G + c001.G + c010.G + c011.G - 3*c100.G + c101.G + c110.G + c111.G))div 8 - 192;
       b :=(5*( c000.B + c001.B + c010.B + c011.B - 3*c100.B + c101.B + c110.B + c111.B))div 8 - 192;
      pT[(2*z+2)mod m[i]].PutPixel( 2*x+1,          2*y+1,         RGBs(r,g,b));
       r :=(5*( c000.R + c001.R + c010.R + c011.R + c100.R - 3*c101.R + c110.R + c111.R))div 8 - 192;
       g :=(5*( c000.G + c001.G + c010.G + c011.G + c100.G - 3*c101.G + c110.G + c111.G))div 8 - 192;
       b :=(5*( c000.B + c001.B + c010.B + c011.B + c100.B - 3*c101.B + c110.B + c111.B))div 8 - 192;
      pT[(2*z+2)mod m[i]].PutPixel((2*x+2)mod w[i], 2*y+1,         RGBs(r,g,b));
       r :=(5*( c000.R + c001.R + c010.R + c011.R + c100.R + c101.R - 3*c110.R + c111.R))div 8 - 192;
       g :=(5*( c000.G + c001.G + c010.G + c011.G + c100.G + c101.G - 3*c110.G + c111.G))div 8 - 192;
       b :=(5*( c000.B + c001.B + c010.B + c011.B + c100.B + c101.B - 3*c110.B + c111.B))div 8 - 192;
      pT[(2*z+2)mod m[i]].PutPixel( 2*x+1,         (2*y+2)mod h[i],RGBs(r,g,b));
       r :=(5*( c000.R + c001.R + c010.R + c011.R + c100.R + c101.R + c110.R - 3*c111.R))div 8 - 192;
       g :=(5*( c000.G + c001.G + c010.G + c011.G + c100.G + c101.G + c110.G - 3*c111.G))div 8 - 192;
       b :=(5*( c000.B + c001.B + c010.B + c011.B + c100.B + c101.B + c110.B - 3*c111.B))div 8 - 192;
      pT[(2*z+2)mod m[i]].PutPixel((2*x+2)mod w[i],(2*y+2)mod h[i],RGBs(r,g,b));
    end;
  end;
end.