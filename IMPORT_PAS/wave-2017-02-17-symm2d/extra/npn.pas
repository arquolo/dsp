uses core,graphabc;
var
  pT1, pT2: picture;
  c00,c01,c10,c11,
  p00,p01,p10,p11: color;
  r, g, b: smallint;
  
begin
  kind:='npn';
  pT1:= new Picture(w[0], h[0]);
  pT2:= new Picture(w[0], h[0]);
  
  for var i:=0 to k-1 do begin
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c00 := pT.GetPixel((2*x+1)mod w[i],(2*y+1)mod h[i]); p00:=RGBs(256-c00.R,256-c00.G,256-c00.B);
      c01 := pT.GetPixel((2*x+2)mod w[i],(2*y+1)mod h[i]); p01:=RGBs(256-c01.R,256-c01.G,256-c01.B);
      c10 := pT.GetPixel((2*x+1)mod w[i],(2*y+2)mod h[i]); p10:=RGBs(256-c10.R,256-c10.G,256-c10.B);
      c11 := pT.GetPixel((2*x+2)mod w[i],(2*y+2)mod h[i]); p11:=RGBs(256-c11.R,256-c11.G,256-c11.B);
       r := (c00.R + c01.R + c10.R + p11.R) div 4;
       g := (c00.G + c01.G + c10.G + p11.G) div 4;
       b := (c00.B + c01.B + c10.B + p11.B) div 4;
      pT1.PutPixel((2*x+1)mod w[i],(2*y+1)mod h[i],RGBs(r,g,b));
       r := (c00.R + c01.R + p10.R + c11.R) div 4;
       g := (c00.G + c01.G + p10.G + c11.G) div 4;
       b := (c00.B + c01.B + p10.B + c11.B) div 4;
      pT1.PutPixel((2*x+2)mod w[i],(2*y+1)mod h[i],RGBs(r,g,b));
       r := (c00.R + p01.R + c10.R + c11.R) div 4;
       g := (c00.G + p01.G + c10.G + c11.G) div 4;
       b := (c00.B + p01.B + c10.B + c11.B) div 4;
      pT1.PutPixel((2*x+1)mod w[i],(2*y+2)mod h[i],RGBs(r,g,b));
       r := (p00.R + c01.R + c10.R + c11.R) div 4;
       g := (p00.G + c01.G + c10.G + c11.G) div 4;
       b := (p00.B + c01.B + c10.B + c11.B) div 4;
      pT1.PutPixel((2*x+2)mod w[i],(2*y+2)mod h[i],RGBs(r,g,b));
    end;
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c00 := pT1.GetPixel(2*x,  2*y  ); p00:=RGBs(256-c00.R,256-c00.G,256-c00.B);
      c01 := pT1.GetPixel(2*x+1,2*y  ); p01:=RGBs(256-c01.R,256-c01.G,256-c01.B);
      c10 := pT1.GetPixel(2*x,  2*y+1); p10:=RGBs(256-c10.R,256-c10.G,256-c10.B);
      c11 := pT1.GetPixel(2*x+1,2*y+1); p11:=RGBs(256-c11.R,256-c11.G,256-c11.B);
       r := (p00.R + c01.R + c10.R + c11.R) div 4;
       g := (p00.G + c01.G + c10.G + c11.G) div 4;
       b := (p00.B + c01.B + c10.B + c11.B) div 4;
      pT2.PutPixel(2*x,  2*y,  RGBs(r,g,b));
       r := (c00.R + p01.R + c10.R + c11.R) div 4;
       g := (c00.G + p01.G + c10.G + c11.G) div 4;
       b := (c00.B + p01.B + c10.B + c11.B) div 4;
      pT2.PutPixel(2*x+1,2*y,  RGBs(r,g,b));
       r := (c00.R + c01.R + p10.R + c11.R) div 4;
       g := (c00.G + c01.G + p10.G + c11.G) div 4;
       b := (c00.B + c01.B + p10.B + c11.B) div 4;
      pT2.PutPixel(2*x,  2*y+1,RGBs(r,g,b));
       r := (c00.R + c01.R + c10.R + p11.R) div 4;
       g := (c00.G + c01.G + c10.G + p11.G) div 4;
       b := (c00.B + c01.B + c10.B + p11.B) div 4;
      pT2.PutPixel(2*x+1,2*y+1,RGBs(r,g,b));
    end;
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c00 := pT2.GetPixel((w[i]+2*x-2)mod w[i],(h[i]+2*y-2)mod h[i]); p00:=RGBs(256-c00.R,256-c00.G,256-c00.B);
      c01 := pT2.GetPixel((w[i]+2*x+3)mod w[i],(h[i]+2*y-2)mod h[i]); p01:=RGBs(256-c01.R,256-c01.G,256-c01.B);
      c10 := pT2.GetPixel((w[i]+2*x-2)mod w[i],(h[i]+2*y+3)mod h[i]); p10:=RGBs(256-c10.R,256-c10.G,256-c10.B);
      c11 := pT2.GetPixel((w[i]+2*x+3)mod w[i],(h[i]+2*y+3)mod h[i]); p11:=RGBs(256-c11.R,256-c11.G,256-c11.B);
       r := (p00.R + c01.R + c10.R + c11.R) div 4;
       g := (p00.G + c01.G + c10.G + c11.G) div 4;
       b := (p00.B + c01.B + c10.B + c11.B) div 4;
      pT1.PutPixel(2*x,  2*y,  RGBs(r,g,b));
       r := (c00.R + p01.R + c10.R + c11.R) div 4;
       g := (c00.G + p01.G + c10.G + c11.G) div 4;
       b := (c00.B + p01.B + c10.B + c11.B) div 4;
      pT1.PutPixel(2*x+1,2*y,  RGBs(r,g,b));
       r := (c00.R + c01.R + p10.R + c11.R) div 4;
       g := (c00.G + c01.G + p10.G + c11.G) div 4;
       b := (c00.B + c01.B + p10.B + c11.B) div 4;
      pT1.PutPixel(2*x,  2*y+1,RGBs(r,g,b));
       r := (c00.R + c01.R + c10.R + p11.R) div 4;
       g := (c00.G + c01.G + c10.G + p11.G) div 4;
       b := (c00.B + c01.B + c10.B + p11.B) div 4;
      pT1.PutPixel(2*x+1,2*y+1,RGBs(r,g,b));
    end;
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c00 := pT1.GetPixel((w[i]+2*x-3)mod w[i],(h[i]+2*y-3)mod h[i]);
      c01 := pT1.GetPixel((w[i]+2*x+4)mod w[i],(h[i]+2*y-3)mod h[i]);
      c10 := pT1.GetPixel((w[i]+2*x-3)mod w[i],(h[i]+2*y+4)mod h[i]);
      c11 := pT1.GetPixel((w[i]+2*x+4)mod w[i],(h[i]+2*y+4)mod h[i]);
       r := 2*(c00.R + c01.R + c10.R + c11.R - 448);
       g := 2*(c00.G + c01.G + c10.G + c11.G - 448);
       b := 2*(c00.B + c01.B + c10.B + c11.B - 448);
      pT.PutPixel(x,       y,       RGBs(r,g,b));
       r := (c00.R - c01.R + c10.R - c11.R) div 2 + 128;
       g := (c00.G - c01.G + c10.G - c11.G) div 2 + 128;
       b := (c00.B - c01.B + c10.B - c11.B) div 2 + 128;
      pT.PutPixel(x+w[i+1],y,       RGBs(r,g,b));
       r := (c00.R + c01.R - c10.R - c11.R) div 2 + 128;
       g := (c00.G + c01.G - c10.G - c11.G) div 2 + 128;
       b := (c00.B + c01.B - c10.B - c11.B) div 2 + 128;
      pT.PutPixel(x,       y+h[i+1],RGBs(r,g,b));
       r := (c00.R - c01.R - c10.R + c11.R) div 2 + 128;
       g := (c00.G - c01.G - c10.G + c11.G) div 2 + 128;
       b := (c00.B - c01.B - c10.B + c11.B) div 2 + 128;
      pT.PutPixel(x+w[i+1],y+h[i+1],RGBs(r,g,b));
    end;
  end;
  
  morph;
  
  for var i:=k-1 downto 0 do begin
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c00 := pT.GetPixel(x,       y       );
      c01 := pT.GetPixel(x+w[i+1],y       );
      c10 := pT.GetPixel(x,       y+h[i+1]);
      c11 := pT.GetPixel(x+w[i+1],y+h[i+1]);
       r := (c00.R div 4 + c01.R + c10.R + c11.R - 384 + 224) div 2;
       g := (c00.G div 4 + c01.G + c10.G + c11.G - 384 + 224) div 2;
       b := (c00.B div 4 + c01.B + c10.B + c11.B - 384 + 224) div 2;
      pT1.PutPixel((w[i]+2*x-3)mod w[i],(h[i]+2*y-3)mod h[i],RGBs(r,g,b));
       r := (c00.R div 4 - c01.R + c10.R - c11.R + 128 + 224) div 2;
       g := (c00.G div 4 - c01.G + c10.G - c11.G + 128 + 224) div 2;
       b := (c00.B div 4 - c01.B + c10.B - c11.B + 128 + 224) div 2;
      pT1.PutPixel((w[i]+2*x+4)mod w[i],(h[i]+2*y-3)mod h[i],RGBs(r,g,b));
       r := (c00.R div 4 + c01.R - c10.R - c11.R + 128 + 224) div 2;
       g := (c00.G div 4 + c01.G - c10.G - c11.G + 128 + 224) div 2;
       b := (c00.B div 4 + c01.B - c10.B - c11.B + 128 + 224) div 2;
      pT1.PutPixel((w[i]+2*x-3)mod w[i],(h[i]+2*y+4)mod h[i],RGBs(r,g,b));
       r := (c00.R div 4 - c01.R - c10.R + c11.R + 128 + 224) div 2;
       g := (c00.G div 4 - c01.G - c10.G + c11.G + 128 + 224) div 2;
       b := (c00.B div 4 - c01.B - c10.B + c11.B + 128 + 224) div 2;
      pT1.PutPixel((w[i]+2*x+4)mod w[i],(h[i]+2*y+4)mod h[i],RGBs(r,g,b));
    end;
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c00 := pT1.GetPixel(2*x,   2*y  );
      c01 := pT1.GetPixel(2*x+1, 2*y  );
      c10 := pT1.GetPixel(2*x,   2*y+1);
      c11 := pT1.GetPixel(2*x+1, 2*y+1);
       r :=-c00.R + c01.R + c10.R + c11.R - 128;
       g :=-c00.G + c01.G + c10.G + c11.G - 128;
       b :=-c00.B + c01.B + c10.B + c11.B - 128;
      pT2.PutPixel((w[i]+2*x-2)mod w[i],(h[i]+2*y-2)mod h[i],RGBs(r,g,b));
       r := c00.R - c01.R + c10.R + c11.R - 128;
       g := c00.G - c01.G + c10.G + c11.G - 128;
       b := c00.B - c01.B + c10.B + c11.B - 128;
      pT2.PutPixel((w[i]+2*x+3)mod w[i],(h[i]+2*y-2)mod h[i],RGBs(r,g,b));
       r := c00.R + c01.R - c10.R + c11.R - 128;
       g := c00.G + c01.G - c10.G + c11.G - 128;
       b := c00.B + c01.B - c10.B + c11.B - 128;
      pT2.PutPixel((w[i]+2*x-2)mod w[i],(h[i]+2*y+3)mod h[i],RGBs(r,g,b));
       r := c00.R + c01.R + c10.R - c11.R - 128;
       g := c00.G + c01.G + c10.G - c11.G - 128;
       b := c00.B + c01.B + c10.B - c11.B - 128;
      pT2.PutPixel((w[i]+2*x+3)mod w[i],(h[i]+2*y+3)mod h[i],RGBs(r,g,b));
    end;
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c00 := pT2.GetPixel(2*x,   2*y  );
      c01 := pT2.GetPixel(2*x+1, 2*y  );
      c10 := pT2.GetPixel(2*x,   2*y+1);
      c11 := pT2.GetPixel(2*x+1, 2*y+1);
       r :=-c00.R + c01.R + c10.R + c11.R - 128;
       g :=-c00.G + c01.G + c10.G + c11.G - 128;
       b :=-c00.B + c01.B + c10.B + c11.B - 128;
      pT1.PutPixel(2*x,  2*y,  RGBs(r,g,b));
       r := c00.R - c01.R + c10.R + c11.R - 128;
       g := c00.G - c01.G + c10.G + c11.G - 128;
       b := c00.B - c01.B + c10.B + c11.B - 128;
      pT1.PutPixel(2*x+1,2*y,  RGBs(r,g,b));
       r := c00.R + c01.R - c10.R + c11.R - 128;
       g := c00.G + c01.G - c10.G + c11.G - 128;
       b := c00.B + c01.B - c10.B + c11.B - 128;
      pT1.PutPixel(2*x,  2*y+1,RGBs(r,g,b));
       r := c00.R + c01.R + c10.R - c11.R - 128;
       g := c00.G + c01.G + c10.G - c11.G - 128;
       b := c00.B + c01.B + c10.B - c11.B - 128;
      pT1.PutPixel(2*x+1,2*y+1,RGBs(r,g,b));
    end;
    for var y := 0 to h[i+1]-1 do
    for var x := 0 to w[i+1]-1 do begin
      c00 := pT1.GetPixel((2*x+1)mod w[i],(2*y+1)mod h[i]);
      c01 := pT1.GetPixel((2*x+2)mod w[i],(2*y+1)mod h[i]);
      c10 := pT1.GetPixel((2*x+1)mod w[i],(2*y+2)mod h[i]);
      c11 := pT1.GetPixel((2*x+2)mod w[i],(2*y+2)mod h[i]);
       r := c00.R + c01.R + c10.R - c11.R - 128;
       g := c00.G + c01.G + c10.G - c11.G - 128;
       b := c00.B + c01.B + c10.B - c11.B - 128;
      pT.PutPixel((2*x+1)mod w[i],(2*y+1)mod h[i],RGBs(r,g,b));
       r := c00.R + c01.R - c10.R + c11.R - 128;
       g := c00.G + c01.G - c10.G + c11.G - 128;
       b := c00.B + c01.B - c10.B + c11.B - 128;
      pT.PutPixel((2*x+2)mod w[i],(2*y+1)mod h[i],RGBs(r,g,b));
       r := c00.R - c01.R + c10.R + c11.R - 128;
       g := c00.G - c01.G + c10.G + c11.G - 128;
       b := c00.B - c01.B + c10.B + c11.B - 128;
      pT.PutPixel((2*x+1)mod w[i],(2*y+2)mod h[i],RGBs(r,g,b));
       r :=-c00.R + c01.R + c10.R + c11.R - 128;
       g :=-c00.G + c01.G + c10.G + c11.G - 128;
       b :=-c00.B + c01.B + c10.B + c11.B - 128;
      pT.PutPixel((2*x+2)mod w[i],(2*y+2)mod h[i],RGBs(r,g,b));
    end;
  end;
end.