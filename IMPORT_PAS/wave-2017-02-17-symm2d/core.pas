unit core;
uses graphabc;

var th: array of smallint;
	fx:text;
	k,wd,hd:integer;
	w,h: array of integer;
	p0, p1, pT: picture;
	kind,str:string;
	mode:integer;

function thres(c:color;i:integer):color;
var r,g,b:smallint;
begin
	if abs(c.R-128)<th[i] then r:=128 else r:=c.R;
	if abs(c.G-128)<th[i] then g:=128 else g:=c.G;
	if abs(c.B-128)<th[i] then b:=128 else b:=c.B;
	result:=RGBs(r,g,b);
end;

function quant(c:color;i:integer):color;
var r,g,b:smallint;
begin
	r:=((c.R-128) div th[i]) * th[i] + 128;
	g:=((c.G-128) div th[i]) * th[i] + 128;
	b:=((c.B-128) div th[i]) * th[i] + 128;
	result:=RGBs(r,g,b);
end;

function sharp(c:color;i:integer):color;
var r,g,b:smallint;
begin
	r:=2*c.R-128; 
	g:=2*c.G-128;
	b:=2*c.B-128;
	result:=RGBs(r,g,b);
end;

function smooth(c:color;i:integer):color;
var r,g,b:smallint;
begin
	r:=c.R div 2+64; 
	g:=c.G div 2+64;
	b:=c.B div 2+64;
	result:=RGBs(r,g,b);
end;

procedure morph;
begin
	window.caption:=kind+'_x'+inttostr(k);
	//readln;
	pT.Draw(0,0,wd,hd);
	case mode of
	0://packing, artefact check
	for var i:=0 to k-1 do begin
		for var y:=0 to h[i]-1 do for var x:=w[i+1] to w[i]-1 do pT.PutPixel(x,y,quant(pT.GetPixel(x,y),i));
		for var y:=h[i+1] to h[i]-1 do for var x:=0 to w[i]-1 do pT.PutPixel(x,y,quant(pT.GetPixel(x,y),i));
	end;
	1://sharp
	for var i:=0 to k-1 do begin
		for var y:=0 to h[i]-1 do for var x:=w[i+1] to w[i]-1 do pT.PutPixel(x,y,sharp(pT.GetPixel(x,y),i));
		for var y:=h[i+1] to h[i]-1 do for var x:=0 to w[i]-1 do pT.PutPixel(x,y,sharp(pT.GetPixel(x,y),i));
	end;
	-1://smooth
	for var i:=0 to k-1 do begin
		for var y:=0 to h[i]-1 do for var x:=w[i+1] to w[i]-1 do pT.PutPixel(x,y,smooth(pT.GetPixel(x,y),i));
		for var y:=h[i+1] to h[i]-1 do for var x:=0 to w[i]-1 do pT.PutPixel(x,y,smooth(pT.GetPixel(x,y),i));
	end;
	else//packing, artefact check
	for var i:=0 to k-1 do begin
		for var y:=0 to h[i]-1 do for var x:=w[i+1] to w[i]-1 do pT.PutPixel(x,y,thres(pT.GetPixel(x,y),i));
		for var y:=h[i+1] to h[i]-1 do for var x:=0 to w[i]-1 do pT.PutPixel(x,y,thres(pT.GetPixel(x,y),i));
	end;
	end;
	readln;
	pT.Draw(0,0,wd,hd);
end;

initialization
	assign(fx,'settings.txt'); reset(fx); readln(fx,k); readln(fx,mode); close(fx);
	setlength(w,k+1);
	setlength(h,k+1);
	setlength(th,k);
	readln(str);
	p0 := new Picture(str+'.png');
	w[0] := p0.Width;  wd:=min(w[0],ScreenWidth -80);  
	h[0] := p0.Height; hd:=min(h[0],ScreenHeight-160); var rct:=Rect(0,0,w[0],h[0]);
	if hd*w[0]>wd*h[0]
		then hd:=round(wd*h[0]/w[0])
		else wd:=round(hd*w[0]/h[0]);
	th[k-1]:=2; for var i:=k-2 downto 0 do th[i]:=2*th[i+1];
	for var i:=0 to k-1 do begin
		w[i+1]:=w[i] div 2;
		h[i+1]:=h[i] div 2;
	end;
	setwindowsize(wd, hd);
	centerwindow;
	//p0.Draw(0,0,wd,hd);
	
	pT := new Picture(w[0], h[0]);
	p1 := new Picture(w[0], h[0]);
	
	pT.CopyRect(rct,p0,rct);
	pT.Draw(0,0,wd,hd);
	
finalization
	p1.CopyRect(rct,pT,rct);
	while true do begin
		readln;
		p1.Draw(0,0,wd,hd);
		readln;
		p0.Draw(0,0,wd,hd);
	end;
end.
