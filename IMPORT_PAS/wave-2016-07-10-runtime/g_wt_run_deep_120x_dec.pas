var
	s_globe:single;
	w_first:boolean:=true;
	KIND:string:='Heterofilter 5-3-2-2-2';

var
	p0,pt: picture;
	m0,mc:t_map;
	m1: t_map;
	s,kp,noiseLvl: single;
	//r: single;
	stl,lftk,lft:string;
	//rtl,kpl:string;
	k,c:integer;
begin
	if w_first then begin
		setwindowsize(200,200); centerwindow;
		window.IsFixedSize:=true;
		w_first:=false;
	end;
	
	p0:=new Picture('0.png');
	
	if CCUST and CCONV then begin
 		mc:=new t_map(1,p0.Height,p0.Width,mASymm[1]);
		mc.scan_native(p0);
		init_Space_adapt(mc);
	end else
		init_Space;
	
	m0:=new t_map(1,p0.Height,p0.Width,mASymm[1]);
	m0.scan(p0);
	
	m1:=new t_map(1,p0.Height,p0.Width,mASymm[1]);
	m1.scan(p0);
	
	for c:=0 to 2 do begin
		{$omp parallel for}
		for var i:=0 to m1.sY-1 do
			tf_Direct(m1,c,0,i,0,m1.sX,'horz',mASymm[3]);
		{$omp parallel for}
		for var j:=0 to m1.sX-1 do
			tf_Direct(m1,c,0,0,j,m1.sY,'vert',mASymm[3]);
		{$omp parallel for}
		for var i:=0 to m1.sY div 5-1 do
			tf_Direct(m1,c,0,i,0,m1.sX div 5,'horz',mASymm[2]);
		{$omp parallel for}
		for var j:=0 to m1.sX div 5-1 do
			tf_Direct(m1,c,0,0,j,m1.sY div 5,'vert',mASymm[2]);
		for k:=0 to 2 do begin
			{$omp parallel for}
			for var i:=0 to m1.sY div round(15*power(2,k))-1 do
				tf_Direct(m1,c,0,i,0,m1.sX div round(15*power(2,k)),'horz',mASymm[1]);
			{$omp parallel for}
			for var j:=0 to m1.sX div round(15*power(2,k))-1 do
				tf_Direct(m1,c,0,0,j,m1.sY div round(15*power(2,k)),'vert',mASymm[1]);
		end;
	end;
	
	setwindowsize(m1.sX, m1.sY); centerwindow;
	pt:=new Picture(m1.sX, m1.sY);
	m1.print_core(pt,1,1,0);
	pt.Draw(0,0); redraw;
	readln;
	
	init_Quant(m1,noiseLvl);
	run_Quantize(m1,noiseLvl,kp);

	for c:=0 to 2 do begin
		for k:=2 downto 0 do begin
			{$omp parallel for}
			for var i:=0 to m1.sY div round(15*power(2,k))-1 do
				tf_Inverse(m1,c,0,i,0,m1.sX div round(15*power(2,k)),'horz',mASymm[1]);
			{$omp parallel for}
			for var j:=0 to m1.sX div round(15*power(2,k))-1 do
				tf_Inverse(m1,c,0,0,j,m1.sY div round(15*power(2,k)),'vert',mASymm[1]);
		end;
		{$omp parallel for}
		for var i:=0 to m1.sY div 5-1 do
			tf_Inverse(m1,c,0,i,0,m1.sX div 5,'horz',mASymm[2]);
		{$omp parallel for}
		for var j:=0 to m1.sX div 5-1 do
			tf_Inverse(m1,c,0,0,j,m1.sY div 5,'vert',mASymm[2]);
		{$omp parallel for}
		for var i:=0 to m1.sY-1 do
			tf_Inverse(m1,c,0,i,0,m1.sX,'horz',mASymm[3]);
		{$omp parallel for}
		for var j:=0 to m1.sX-1 do
			tf_Inverse(m1,c,0,0,j,m1.sY,'vert',mASymm[3]);
	end;
		
	//r:=PSNR(m0,m1);
	s:=SSIM(m0,m1);
		
		
	stl :=floattostr(round(-10000*log10(1.0000000001-s))/1000); s_globe:=round(-10000000*log10(1.0000000001-s))/1000000;
	//rtl :=floattostr(round(100*r)/100);
	//kpl :=floattostr(round(1000/(1-kp))/1000);
	lftk:=floattostr(round(100*(1-kp)*m0.sX*m0.sY*m0.sZ*3/1024)/100);
	lft :=floattostr(round(1000*(1-kp))/10);
	//window.Caption:='mp = '+kpl+'x, SSIM = '+stl+' dB, PSNR='+rtl+' dB';
	window.Caption:=KIND+' sz = '+lftk+'kB ('+lft+'%), SSIM = '+stl+' dB';
	
	m1.print_core(pt,1,1,0);
	pt.Draw(0,0); redraw;
end.
