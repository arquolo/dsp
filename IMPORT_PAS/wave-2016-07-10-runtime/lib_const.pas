const
	SPLITLIMIT=10;
	
	FORCENORMALIZE=true;
	WEIGHTEDCOLOR=false;
	
	CCONV=true;     //(default true) colorspace conversion
	CCUST=false;    //(default false)	enable custom colorspace

var
	IMG_MOD:=64;  //(default 16) size, which image stretches to
	TBLOCK:=8;    //(default 8)	 block used only in DCT and Walsh
	MODE:string;  //'DualTree' of 'IndependentTree'
	CSIZE:boolean;
	NOISEGLOBAL:single;
	KEEP:single;  //unchanged part of picture
	TEMPORAL:boolean;	

initialization
	var tmpb:integer;
	var tmpr:real;
	var f:   text;
	assign(f,'settings.bad');
	reset(f);
	read(f,IMG_MOD);
	read(f,TBLOCK); IMG_MOD:=max(IMG_MOD,TBLOCK);
	read(f,tmpb); if tmpb=0 then MODE:='DuaT' else MODE:='IndT';
	read(f,tmpb); if tmpb=0 then CSIZE:=false else CSIZE:=true;
	read(f,tmpr); NOISEGLOBAL:=single(tmpr);
	read(f,tmpr); KEEP       :=single(tmpr);
	read(f,tmpb); if tmpb=0 then TEMPORAL:=false else TEMPORAL:=true;
	close(f);
end.
