type t_filter = class public
	base,data,data2:integer;
	weight:single;
	shift:integer;
	lift:boolean;
	n:array of t_vector;
	constructor create(base,data:integer);
	var i:integer;
	begin
		self.weight:=1;
		self.base:=base;
		self.data:=data;
		self.lift:=false;
		setlength(self.n,base);
		for i:=0 to base-1 do
			self.n[i]:=new t_vector(data);
	end;
	constructor create(data2:integer);
	begin
		weight := 1;
		base := 2;
		data := 4 * data2 + 1;
		self.data2 := data2;
		lift := true;
		setlength(n, 1);
		n[0] := new t_vector(2 * data2 + 1);
	end;
	constructor create(n:byte; ag0:real; ag1:real:=0; ag2:real:=0);
	begin
		self.weight:=1;
		self.base:=2;
		self.data:=2*n;
		self.lift:=false;
		setlength(self.n,2);
		self.n[0]:=new t_vector(2*n);
		self.n[1]:=new t_vector(2*n);
		case n of
		1:  begin
			self.n[0].fill( cos(ag0), sin(ag0));
			self.n[1].fill( sin(ag0),-cos(ag0));
		    end;
		2:  begin
			self.n[0].fill( sin(ag0)*cos(ag1), cos(ag0)*cos(ag1), cos(ag0)*sin(ag1),-sin(ag0)*sin(ag1));
			self.n[1].fill( sin(ag0)*sin(ag1), cos(ag0)*sin(ag1),-cos(ag0)*cos(ag1), sin(ag0)*cos(ag1));
		    end;
		3:  begin
			self.n[0].fill( sin(ag0)*cos(ag1+ag2)*sin(ag2), sin(ag0)*cos(ag1+ag2)*cos(ag2), cos(ag0)*cos(ag1), cos(ag0)*sin(ag1),-sin(ag0)*sin(ag1+ag2)*cos(ag2), sin(ag0)*sin(ag1+ag2)*sin(ag2));
			self.n[1].fill( sin(ag0)*sin(ag1+ag2)*sin(ag2), sin(ag0)*sin(ag1+ag2)*cos(ag2), cos(ag0)*sin(ag1),-cos(ag0)*cos(ag1), sin(ag0)*cos(ag1+ag2)*cos(ag2),-sin(ag0)*cos(ag1+ag2)*sin(ag2));
		    end;
		end;
		self.sync;
	end;
end;

type t_filter_color = class public
	n:array[0..3,0..3] of single;
	procedure fill(i:integer; params a:array of single);
	var j:integer;
	begin
		for j:=0 to 3 do
			self.n[i,j]:=a[j];
	end;
end;

procedure chSp(var x:t_color; rev:boolean:=false); forward;

type t_map = class public
	cumulate:single; //cumulator
	decim, //decimator
	nYX,nZ,nY,nX, //layers
	sZ,sY,sX:integer; //resolutions
	px:array [0..2] of array [,,] of single;
    constructor create(sZ,sY,sX:integer; f:t_filter);
    begin
    	self.sZ:=sZ;
    	self.sY:=IMG_MOD*(sY div IMG_MOD);
		self.sX:=IMG_MOD*(sX div IMG_MOD);
		self.cumulate:=f.weight;
		self.decim:=f.base;
		self.nZ:=0;
		self.nY:=0;
		self.nX:=0;
		self.nYX:=0;
		setlength(px[0],sZ,sY,sX);
		setlength(px[1],sZ,sY,sX);
		setlength(px[2],sZ,sY,sX);
		while (self.sZ mod round(power(self.decim,self.nZ+1))=0)
		  and (self.sZ div round(power(self.decim,self.nZ))>=f.data) do
			self.nZ+=1;
		while (self.sY mod round(power(self.decim,self.nY+1))=0)
		  and (self.sY div round(power(self.decim,self.nY))>=f.data) do
			self.nY+=1;
		while (self.sX mod round(power(self.decim,self.nX+1))=0)
		  and (self.sX div round(power(self.decim,self.nX))>=f.data) do
			self.nX+=1;
		if MODE='DuaT' then
			self.nYX:=min(self.nX,self.nY);
		self.nZ:=min(self.nZ,SPLITLIMIT);
		self.nY:=min(self.nY,SPLITLIMIT);
		self.nX:=min(self.nX,SPLITLIMIT);
		self.nYX:=min(self.nYX,SPLITLIMIT);
	end;
	procedure scan_native(const p:picture; k:integer:=0);
	var
		cl: color;
		clt:t_color;
		c,i,j:integer;
	begin
		for i:=0 to self.sY-1 do
		for j:=0 to self.sX-1 do begin
			cl:=p.GetPixel(j,i);
			clt[0]:=cl.R/255.0;
			clt[1]:=cl.G/255.0;
			clt[2]:=cl.B/255.0;
			for c:=0 to 2 do
				self.px[c][k,i,j]:=clt[c];
		end;
	end;
    procedure scan(const p:picture; k:integer:=0);
    var
		cl: color;
		clt:t_color;
		c,i,j:integer;
	begin
		for i:=0 to self.sY-1 do
		for j:=0 to self.sX-1 do begin
			cl:=p.GetPixel(j,i);
			clt[0]:=cl.R/255.0;
			clt[1]:=cl.G/255.0;
			clt[2]:=cl.B/255.0;
			chSp(clt);
			for c:=0 to 2 do
				self.px[c][k,i,j]:=clt[c];
		end;
	end;
	procedure print(var p:picture; mY,mX:integer; k:integer:=0);
	var
		clt:t_color;
		cl: t_color_byte;
		c,i,j:integer;
	begin
		for i:=0 to (self.sY div mY)-1 do
		for j:=0 to (self.sX div mX)-1 do begin
			for c:=0 to 2 do
				clt[c]:=self.px[c][k,i,j];
			chSp(clt,true);
			for c:=0 to 2 do
				cl[c]:=max(round(255*min(clt[c],1)),0);
			p.PutPixel(j,i,RGB(cl[0],cl[1],cl[2]));
		end;
		for i:=0 to (self.sY div mY)-1 do
		for j:=(self.sX div mX) to (self.sX)-1 do begin
			for c:=0 to 2 do
				clt[c]:=self.px[c][k,i,j];
			for c:=0 to 2 do
				cl[c]:=round(255*min(abs(clt[c]),1));
			p.PutPixel(j,i,RGB(cl[0],cl[1],cl[2]));
	    end;
		for i:=(self.sY div mY) to (self.sY)-1 do
		for j:=0 to (self.sX)-1 do begin
			for c:=0 to 2 do
				clt[c]:=self.px[c][k,i,j];
			for c:=0 to 2 do
				cl[c]:=round(255*min(abs(clt[c]),1));
			p.PutPixel(j,i,RGB(cl[0],cl[1],cl[2]));
		end;
	end;
	procedure print_core(var p:picture; mY,mX:integer; k:integer:=0);
	var
		clt:t_color;
		cl: t_color_byte;
		c,i,j:integer;
	begin
		for i:=0 to (self.sY div mY)-1 do
		for j:=0 to (self.sX div mX)-1 do begin
			for c:=0 to 2 do
				clt[c]:=self.px[c][k,i,j];
			chSp(clt,true);
			for c:=0 to 2 do
				cl[c]:=max(round(255*min(clt[c],1)),0);
			brush.Color:=RGB(cl[0],cl[1],cl[2]);
			pen  .Color:=brush.Color;
			if (mX=1) and (mY=1) then
				p.PutPixel(j,i,brush.Color);
			if (mX>1) and (mY=1) then
				p.Line(mX*j,i,mX*j+mX,i);
			if (mX=1) and (mY>1) then
				p.Line(j,mY*i,j,mY*i+mY);
			if (mX>1) and (mY>1) then
				p.Rectangle(mX*j,mY*i,mX*j+mX,mY*i+mY);
		end;
	end;
end;

var
	cMapD,cMapR:t_filter_color;
	mim: record
		ag0x,ag0y,ag0z,
		ag1x,ag1y,ag1z,
		ag2x,ag2y,ag2z: array[0..20] of real;
	end;
	mTest: t_filter;
	agx,agy,agz:word;

//transformation
procedure tf_Direct(var m:t_map; c,z,y,x,line: integer; way: string; f:t_filter);
var
    a,s,d:array of single;
	i,j,j2,line2:integer;
	t: single;
begin
	setlength(a,line);
	case way of
	'horz':
		for i:=0 to line-1 do
			a[i]:=m.px[c][z,y,x+i];
	'vert':
		for i:=0 to line-1 do
			a[i]:=m.px[c][z,y+i,x];
	'time':
		for i:=0 to line-1 do
			a[i]:=m.px[c][z+i,y,x];
	end;
	if f.lift then begin
		line2:=line div 2;
		setlength(s,line2);
		setlength(d,line2);
		for i:=0 to line2-1 do begin
			s[i] := a[2*i];
			d[i] := a[2*i+1];
		end;
	for j:=0 to f.data2-1 do begin
			for i:=0 to line2-1 do d[i] += (s[i]+s[min(i+1,line2-1)]) * f.n[0].id[2*j];
			for i:=0 to line2-1 do s[i] += (d[i]+d[max(i-1,0)]) * f.n[0].id[2*j+1];
		end;
		for i:=0 to line2-1 do begin
			a[i]      := s[i] * f.n[0].id[2*f.data2];
			a[i+line2]:= d[i] / f.n[0].id[2*f.data2];
		end;
	end else begin
		line2:=line div f.base;
		setlength(s,line);
		for i:=0 to line2-1 do
		for j:=0 to f.base-1 do
		begin
			t:=0;
			for j2:=0 to f.data-1 do 
				t += a[(f.base*i+j2) mod line] * f.n[j].id[j2];
			s[j*line2+i]:=t;
		end;
		for i:=0 to line-1 do
			a[i]:=s[i];
	end;
	case way of
	'horz':
		for i:=0 to line-1 do
			m.px[c][z,y,x+i]:=a[i];
	'vert':
		for i:=0 to line-1 do
			m.px[c][z,y+i,x]:=a[i];
	'time':
		for i:=0 to line-1 do
			m.px[c][z+i,y,x]:=a[i];
	end;
end;

procedure tf_Inverse(var m:t_map; c,z,y,x,line: integer; way: string; f:t_filter);
var
	a,s,d:array of single;
	i,j,j2,line2:integer;
	t: single;
begin
	setlength(a,line);
	case way of
	'horz':
		for i:=0 to line-1 do
			a[i]:=m.px[c][z,y,x+i];
	'vert':
		for i:=0 to line-1 do
			a[i]:=m.px[c][z,y+i,x];
	'time':
		for i:=0 to line-1 do
			a[i]:=m.px[c][z+i,y,x];
	end;
	if f.lift then begin
		line2:=line div 2;
		setlength(s,line2);
		setlength(d,line2);
		for i:=0 to line2-1 do begin
			d[i] := a[i+line2] * f.n[0].id[2*f.data2];
			s[i] := a[i] / f.n[0].id[2*f.data2];
		end;
		for j:=f.data2-1 downto 0 do begin
			for i:=0 to line2-1 do s[i] -= (d[i]+d[max(i-1,0)]) * f.n[0].id[2*j+1];
			for i:=0 to line2-1 do d[i] -= (s[i]+s[min(i+1,line2-1)]) * f.n[0].id[2*j];
		end;
		for i:=0 to line2-1 do begin
			a[2*i] := s[i];
			a[2*i+1] := d[i];
		end;
	end else begin
		line2:=line div f.base;
		setlength(s,line);
		for i:=0 to line2-1 do
		for j:=0 to f.base-1 do begin
			t:=0;
			for j2:=0 to f.base-1 do
			for var i2:=0 to (f.data div f.base)-1 do 
				t += a[j2*line2+(line2+i-i2) mod line2] * f.n[j2].id[f.base*i2+j];
			s[i*f.base+j]:=t;
		end;
		for i:=0 to line-1 do
			a[i]:=s[i];
	end;
	case way of
	'horz':
		for i:=0 to line-1 do
			m.px[c][z,y,x+i]:=a[i];
	'vert':
		for i:=0 to line-1 do
			m.px[c][z,y+i,x]:=a[i];
	'time':
		for i:=0 to line-1 do
			m.px[c][z+i,y,x]:=a[i];
	end;
end;

procedure run_Analysis(var m:t_map; const f:t_filter);
var
	c,g,i,j,k:integer;
begin
	for c:=0 to 2 do begin
		if MODE='IndT' then begin
			for k:=0 to m.nZ-1 do
				{$omp parallel for}
				for i:=0 to m.sY-1 do
				for j:=0 to m.sX-1 do
					tf_Direct(m,c,0,i,j,m.sZ div round(power(m.decim,k)),'time',f);
			for i:=0 to m.nY-1 do
				{$omp parallel for}
				for k:=0 to m.sZ-1 do
				for j:=0 to m.sX-1 do
					tf_Direct(m,c,k,0,j,m.sY div round(power(m.decim,i)),'vert',f);
			for j:=0 to m.nX-1 do
				{$omp parallel for}
				for k:=0 to m.sZ-1 do
				for i:=0 to m.sY-1 do
					tf_Direct(m,c,k,i,0,m.sX div round(power(m.decim,j)),'horz',f);
		end;
		if MODE='DuaT' then begin
			for g:=0 to m.nZ-1 do begin
				{$omp parallel for}
				for i:=0 to m.sY div round(power(m.decim,g))-1 do
				for j:=0 to m.sX div round(power(m.decim,g))-1 do
					tf_Direct(m,c,0,i,j,m.sZ div round(power(m.decim,g)),'time',f);
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(m.decim,g))-1 do
				for j:=0 to m.sX div round(power(m.decim,g))-1 do
					tf_Direct(m,c,k,0,j,m.sY div round(power(m.decim,g)),'vert',f);
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(m.decim,g))-1 do
				for i:=0 to m.sY div round(power(m.decim,g))-1 do
					tf_Direct(m,c,k,i,0,m.sX div round(power(m.decim,g)),'horz',f);
			end;
			for g:=m.nZ to m.nYX-1 do begin
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(m.decim,m.nZ))-1 do
				for j:=0 to m.sX div round(power(m.decim,g))-1 do
					tf_Direct(m,c,k,0,j,m.sY div round(power(m.decim,g)),'vert',f);
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(m.decim,m.nZ))-1 do
				for i:=0 to m.sY div round(power(m.decim,g))-1 do
					tf_Direct(m,c,k,i,0,m.sX div round(power(m.decim,g)),'horz',f);
			end;
		end;
	end;
end;

procedure run_Synthesys(var m:t_map; const f:t_filter);
var
	c,g,i,j,k:integer;
begin
	for c:=0 to 2 do begin
		if MODE='DuaT' then begin
			for g:=m.nYX-1 downto m.nZ do begin
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(m.decim,m.nZ))-1 do
				for i:=0 to m.sY div round(power(m.decim,g))-1 do
					tf_Inverse(m,c,k,i,0,m.sX div round(power(m.decim,g)),'horz',f);		
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(m.decim,m.nZ))-1 do
				for j:=0 to m.sX div round(power(m.decim,g))-1 do
					tf_Inverse(m,c,k,0,j,m.sY div round(power(m.decim,g)),'vert',f);
			end;
			for g:=m.nZ-1 downto 0 do begin
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(m.decim,g))-1 do
				for i:=0 to m.sY div round(power(m.decim,g))-1 do
					tf_Inverse(m,c,k,i,0,m.sX div round(power(m.decim,g)),'horz',f);		
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(m.decim,g))-1 do
				for j:=0 to m.sX div round(power(m.decim,g))-1 do
					tf_Inverse(m,c,k,0,j,m.sY div round(power(m.decim,g)),'vert',f);
				{$omp parallel for}
				for i:=0 to m.sY div round(power(m.decim,g))-1 do
				for j:=0 to m.sX div round(power(m.decim,g))-1 do
					tf_Inverse(m,c,0,i,j,m.sZ div round(power(m.decim,g)),'time',f);
			end;
		end;
		if MODE='IndT' then begin
			for j:=m.nX-1 downto 0 do
				{$omp parallel for}
				for k:=0 to m.sZ-1 do
				for i:=0 to m.sY-1 do
					tf_Inverse(m,c,k,i,0,m.sX div round(power(m.decim,j)),'horz',f);		
			for i:=m.nY-1 downto 0 do
				{$omp parallel for}
				for k:=0 to m.sZ-1 do
				for j:=0 to m.sX-1 do
					tf_Inverse(m,c,k,0,j,m.sY div round(power(m.decim,i)),'vert',f);
			for k:=m.nZ-1 downto 0 do
				{$omp parallel for}
				for i:=0 to m.sY-1 do
				for j:=0 to m.sX-1 do
					tf_Inverse(m,c,0,i,j,m.sZ div round(power(m.decim,k)),'time',f);
		end;
	end;
end;

procedure tf_Mimic(var m:t_map; nZ,nY,nX:integer; way: string);
var
	c,i,j,k:integer;
begin
	var k0,k1,k00,k01,k10,k11: real;
	var m0,m1,m2,m3:real;
	var p0,p1,p2,p3,p4:real;
	var q:answ;
	var wZ,wY,wX: integer;
	wZ:=m.sZ div round(power(2,nZ));
	wY:=m.sY div round(power(2,nY));
	wX:=m.sX div round(power(2,nX));
	case way of
	'horz':
		for c:=0 to 2 do
		for k:=0 to wZ-1 do
		for i:=0 to wY-1 do
		for j:=0 to wX div 2-1 do begin
			k0  += m.px[c][k,i,2*j  ] * m.px[c][k,i, 2*j         ] + m.px[c][k,i,2*j+1] * m.px[c][k,i, 2*j+1       ];
			k1  += m.px[c][k,i,2*j+1] * m.px[c][k,i,(2*j+2)mod wX] - m.px[c][k,i,2*j  ] * m.px[c][k,i,(2*j+3)mod wX];
			k00 += m.px[c][k,i,2*j+1] * m.px[c][k,i, 2*j+1       ] - m.px[c][k,i,2*j  ] * m.px[c][k,i, 2*j         ];
			k01 += m.px[c][k,i,2*j+1] * m.px[c][k,i,(2*j+2)mod wX] + m.px[c][k,i,2*j  ] * m.px[c][k,i,(2*j+3)mod wX];
			k10 += m.px[c][k,i,2*j  ] * m.px[c][k,i, 2*j+1       ];
			k11 += m.px[c][k,i,2*j  ] * m.px[c][k,i,(2*j+2)mod wX] - m.px[c][k,i,2*j+1] * m.px[c][k,i,(2*j+3)mod wX];
		end;
	'vert':
		for c:=0 to 2 do
		for k:=0 to wZ-1 do
		for i:=0 to wY div 2-1 do
		for j:=0 to wX-1 do begin
			k0  += m.px[c][k,2*i  ,j] * m.px[c][k, 2*i         ,j] + m.px[c][k,2*i+1,j] * m.px[c][k, 2*i+1       ,j];
			k1  += m.px[c][k,2*i+1,j] * m.px[c][k,(2*i+2)mod wY,j] - m.px[c][k,2*i  ,j] * m.px[c][k,(2*i+3)mod wY,j];
			k00 += m.px[c][k,2*i+1,j] * m.px[c][k, 2*i+1       ,j] - m.px[c][k,2*i  ,j] * m.px[c][k, 2*i         ,j];
			k01 += m.px[c][k,2*i+1,j] * m.px[c][k,(2*i+2)mod wY,j] + m.px[c][k,2*i  ,j] * m.px[c][k,(2*i+3)mod wY,j];
			k10 += m.px[c][k,2*i  ,j] * m.px[c][k, 2*i+1       ,j];
			k11 += m.px[c][k,2*i  ,j] * m.px[c][k,(2*i+2)mod wY,j] - m.px[c][k,2*i+1,j] * m.px[c][k,(2*i+3)mod wY,j];
		end;
	'time':
		for c:=0 to 2 do
		for k:=0 to wZ div 2-1 do
		for i:=0 to wY-1 do
		for j:=0 to wX-1 do begin
			k0  += m.px[c][2*k  ,i,j] * m.px[c][ 2*k         ,i,j] + m.px[c][2*k+1,i,j] * m.px[c][ 2*k+1       ,i,j];
			k1  += m.px[c][2*k+1,i,j] * m.px[c][(2*k+2)mod wZ,i,j] - m.px[c][2*k  ,i,j] * m.px[c][(2*k+3)mod wZ,i,j];
			k00 += m.px[c][2*k+1,i,j] * m.px[c][ 2*k+1       ,i,j] - m.px[c][2*k  ,i,j] * m.px[c][ 2*k         ,i,j];
			k01 += m.px[c][2*k+1,i,j] * m.px[c][(2*k+2)mod wZ,i,j] + m.px[c][2*k  ,i,j] * m.px[c][(2*k+3)mod wZ,i,j];
			k10 += m.px[c][2*k  ,i,j] * m.px[c][ 2*k+1       ,i,j];
			k11 += m.px[c][2*k  ,i,j] * m.px[c][(2*k+2)mod wZ,i,j] - m.px[c][2*k+1,i,j] * m.px[c][(2*k+3)mod wZ,i,j];
		end;
	end;
	k0 /= wZ*wY*wX*3*2;
	k1 /= wZ*wY*wX*3*4;
	k00/= wZ*wY*wX*3*2;
	k01/= wZ*wY*wX*3*4;
	k10/= wZ*wY*wX*3;
	k11/= wZ*wY*wX*3*2;
	m0 := k00*k10 + k01*k11;
	m1 := k00*k00 - k01*k01 - k10*k10 - k11*k11;
	m2 := k1*k11;
	m3 := k1*k01;
	p0 := m0*m0 - m2*m2;
	p1 := 2*(m2*m3 + m0*m1);
	p2 := m1*m1 - 2*m0*m0 - m2*m2 - m3*m3;
	p3 := 2*(m2*m3 - m0*m1);
	p4 := m0*m0 - m3*m3;
	q := solve4(p4,p3,p2,p1,p0);
	var a:real:=0;
	var x,y,xt,yt,ft:real; x:=0; y:=0;
	for i:=1 to q.num do
	for j:=0 to 1 do
	for k:=0 to 1 do begin
		xt:=arctan(q.x[i]) + Pi*j;
		yt:=arctan((k00*q.x[i]-k10)/(k11-k01*q.x[i])) + Pi*k;
		ft:=(k0+k1*sin(yt)) + (k10*cos(yt)+k11*sin(yt))*sin(xt) + (k00*cos(yt)+k01*sin(yt))*cos(xt);
		if ft>a then begin
			x:=xt/2;
			y:=yt/2;
			a:=ft;
		end;
	end;
	mTest := new t_filter(2,x,y);
	case way of
	'horz': begin mim.ag0x[agx]:=x; mim.ag1x[agx]:=y; agx+=1; end;
	'vert': begin mim.ag0y[agy]:=x; mim.ag1y[agy]:=y; agy+=1; end;
	'time': begin mim.ag0z[agz]:=x; mim.ag1z[agz]:=y; agz+=1; end;
	end;
end;

procedure run_AnalysisAdapt(var m:t_map);
var
	c,g,i,j,k:integer;
begin
	for c:=0 to 2 do begin
		if MODE='IndT' then begin
			for k:=0 to m.nZ-1 do begin
				tf_Mimic(m,k,0,0,'time');
				{$omp parallel for}
				for i:=0 to m.sY-1 do
				for j:=0 to m.sX-1 do
					tf_Direct(m,c,0,i,j,m.sZ div round(power(2,k)),'time',mTest);
		end;
			for i:=0 to m.nY-1 do begin
				tf_Mimic(m,0,i,0,'vert');
				{$omp parallel for}
				for k:=0 to m.sZ-1 do
				for j:=0 to m.sX-1 do
					tf_Direct(m,c,k,0,j,m.sY div round(power(2,i)),'vert',mTest);
			end;
			for j:=0 to m.nX-1 do begin
				tf_Mimic(m,0,0,j,'horz');
				{$omp parallel for}
				for k:=0 to m.sZ-1 do
				for i:=0 to m.sY-1 do
					tf_Direct(m,c,k,i,0,m.sX div round(power(2,j)),'horz',mTest);
			end;
		end;
		if MODE='DuaT' then begin
			for g:=0 to m.nZ-1 do begin
				tf_Mimic(m,g,g,g,'time');
				{$omp parallel for}
				for i:=0 to m.sY div round(power(2,g))-1 do
				for j:=0 to m.sX div round(power(2,g))-1 do
					tf_Direct(m,c,0,i,j,m.sZ div round(power(2,g)),'time',mTest);
				tf_Mimic(m,g,g,g,'vert');
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(2,g))-1 do
				for j:=0 to m.sX div round(power(2,g))-1 do
					tf_Direct(m,c,k,0,j,m.sY div round(power(2,g)),'vert',mTest);
				tf_Mimic(m,g,g,g,'horz');
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(2,g))-1 do
				for i:=0 to m.sY div round(power(2,g))-1 do
					tf_Direct(m,c,k,i,0,m.sX div round(power(2,g)),'horz',mTest);
			end;
			for g:=m.nZ to m.nYX-1 do begin
				tf_Mimic(m,m.nZ,g,g,'vert');
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(2,m.nZ))-1 do
				for j:=0 to m.sX div round(power(2,g))-1 do
					tf_Direct(m,c,k,0,j,m.sY div round(power(2,g)),'vert',mTest);
				tf_Mimic(m,m.nZ,g,g,'horz');
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(2,m.nZ))-1 do
				for i:=0 to m.sY div round(power(2,g))-1 do
					tf_Direct(m,c,k,i,0,m.sX div round(power(2,g)),'horz',mTest);
			end;
		end;
	end;
end;

procedure run_SynthesysAdapt(var m:t_map);
var
	c,g,i,j,k:integer;
begin
	for c:=0 to 2 do begin
		if MODE='DuaT' then begin
			for g:=m.nYX-1 downto m.nZ do begin
				//mTest:=t_filter.Create(2,mim.ag0x[g],mim.ag1x[g]);
				agx-=1;
				mTest:=t_filter.Create(2,mim.ag0x[agx],mim.ag1x[agx]);
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(2,m.nZ))-1 do
				for i:=0 to m.sY div round(power(2,g))-1 do
					tf_Inverse(m,c,k,i,0,m.sX div round(power(2,g)),'horz',mTest);
				//mTest:=t_filter.Create(2,mim.ag0y[g],mim.ag1y[g]);
				agy-=1;
				mTest:=t_filter.Create(2,mim.ag0y[agy],mim.ag1y[agy]);
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(2,m.nZ))-1 do
				for j:=0 to m.sX div round(power(2,g))-1 do
					tf_Inverse(m,c,k,0,j,m.sY div round(power(2,g)),'vert',mTest);
			end;
			for g:=m.nZ-1 downto 0 do begin
				//mTest:=t_filter.Create(2,mim.ag0x[g],mim.ag1x[g]);
				agx-=1;
				mTest:=t_filter.Create(2,mim.ag0x[agx],mim.ag1x[agx]);
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(2,g))-1 do
				for i:=0 to m.sY div round(power(2,g))-1 do
					tf_Inverse(m,c,k,i,0,m.sX div round(power(2,g)),'horz',mTest);
				//mTest:=t_filter.Create(2,mim.ag0y[g],mim.ag1y[g]);
				agy-=1;
				mTest:=t_filter.Create(2,mim.ag0y[agy],mim.ag1y[agy]);
				{$omp parallel for}
				for k:=0 to m.sZ div round(power(2,g))-1 do
				for j:=0 to m.sX div round(power(2,g))-1 do
					tf_Inverse(m,c,k,0,j,m.sY div round(power(2,g)),'vert',mTest);
				//mTest:=t_filter.Create(2,mim.ag0z[g],mim.ag1z[g]);
				agz-=1;
				mTest:=t_filter.Create(2,mim.ag0z[agz],mim.ag1z[agz]);
				{$omp parallel for}
				for i:=0 to m.sY div round(power(2,g))-1 do
				for j:=0 to m.sX div round(power(2,g))-1 do
					tf_Inverse(m,c,0,i,j,m.sZ div round(power(2,g)),'time',mTest);
			end;
		end;
		if MODE='IndT' then begin
			for j:=m.nX-1 downto 0 do begin
				//mTest:=t_filter.Create(2,mim.ag0x[j],mim.ag1x[j]);
				agx-=1;
				mTest:=t_filter.Create(2,mim.ag0x[agx],mim.ag1x[agx]);  
				{$omp parallel for}
				for k:=0 to m.sZ-1 do
				for i:=0 to m.sY-1 do
					tf_Inverse(m,c,k,i,0,m.sX div round(power(2,j)),'horz',mTest);
			end;
			for i:=m.nY-1 downto 0 do begin
				//mTest:=t_filter.Create(2,mim.ag0y[i],mim.ag1y[i]);
				agy-=1;
				mTest:=t_filter.Create(2,mim.ag0y[agy],mim.ag1y[agy]);
				{$omp parallel for}
				for k:=0 to m.sZ-1 do
				for j:=0 to m.sX-1 do
					tf_Inverse(m,c,k,0,j,m.sY div round(power(2,i)),'vert',mTest);
			end;
			for k:=m.nZ-1 downto 0 do begin
				//mTest:=t_filter.Create(2,mim.ag0z[k],mim.ag1z[k]);
				agz-=1;
				mTest:=t_filter.Create(2,mim.ag0z[agz],mim.ag1z[agz]); 
				{$omp parallel for}
				for i:=0 to m.sY-1 do
				for j:=0 to m.sX-1 do
					tf_Inverse(m,c,0,i,j,m.sZ div round(power(2,k)),'time',mTest);
			end;
		end;
	end;
end;
	
//filtration
procedure sort(var a:array of single;l,r: integer);
var
	i,j: integer;
	x:single;
begin
	i:=l;
	j:=r;
	x:=a[(r+l) div 2];
	repeat
		while a[i]<x do i+=1;
		while x<a[j] do j-=1;
		if i<=j then begin
			if a[i] > a[j] then swap(a[i],a[j]);
			i+=1;
			j-=1;
		end;
	until i>=j;
	if l<j then sort(a,l,j);
	if i<r then sort(a,i,r);
end;

procedure init_Quant(var m:t_map; var noiseLvl:single);
var
	r: array of single;
	c,z,y,x:integer;
begin
	setlength(r,3*m.sZ*m.sX*m.sY);
	if CSIZE then begin
		{$omp parallel for}
		for c:=0 to 2 do
		for z:=0 to m.sZ-1 do
		for y:=0 to m.sY-1 do
		for x:=0 to m.sX-1 do
			r[((m.sZ*c+z)*m.sY+y)*m.sX+x]:=abs(m.px[c][z,y,x]);
		sort(r,0,r.Length-1);
		noiseLvl:=r[round((r.Length-1)*(1-KEEP))];
	end else
		noiseLvl:=NOISEGLOBAL;
end;

procedure run_Quantize(var m:t_map; const noiseLvl:single; var p:single);
var
	c,z,y,x,
	t,tmp:integer;
begin
	t:=0;
	{$omp parallel for reduction(+:t) private(tmp)}
	for c:=0 to 2 do
	for z:=0 to m.sZ-1 do
	for y:=0 to m.sY-1 do
	for x:=0 to m.sX-1 do begin
		tmp:=round(m.px[c][z,y,x]/noiseLvl/2);
		if tmp=0 then t+=1;
		m.px[c][z,y,x]:=tmp*noiseLvl*2;
	end;
	p:=t/(m.sZ*m.sX*m.sY*3);
end;

//color conversion
procedure init_Space_adapt(const m:t_map);
var
	zx2,zy2,xy,xz,yz,x,y,z,
	tu,ctv,u,v:single;
	k,i,j:integer;
begin
	for k:=0 to m.sZ-1 do
	for i:=0 to m.sY-1 do
	for j:=0 to m.sX-1 do begin
		x += m.px[0][k,i,j];
		y += m.px[1][k,i,j];
		z += m.px[2][k,i,j];
	end;
	x/=m.sX*m.sY*m.sZ; y/=m.sX*m.sY*m.sZ; z/=m.sX*m.sY*m.sZ;
	for k:=0 to m.sZ-1 do
	for i:=0 to m.sY-1 do
	for j:=0 to m.sX-1 do begin
		zx2 += (m.px[2][k,i,j]-z)*(m.px[2][k,i,j]-z) - (m.px[0][k,i,j]-x)*(m.px[0][k,i,j]-x);
		zy2 += (m.px[2][k,i,j]-z)*(m.px[2][k,i,j]-z) - (m.px[1][k,i,j]-y)*(m.px[1][k,i,j]-y);
		xy  += (m.px[0][k,i,j]-x)*(m.px[1][k,i,j]-y);
		xz  += (m.px[0][k,i,j]-x)*(m.px[2][k,i,j]-z);
		yz  += (m.px[1][k,i,j]-y)*(m.px[2][k,i,j]-z);
	end;
	tu:=(yz*zx2+xy*xz)/(xz*zy2+xy*yz);
	ctv:=(zx2-2*xy*tu+zy2*tu*tu)/(xz+yz*tu)/sqrt(1+tu*tu);
	u:=arctan(tu);
	v:=Pi/4-arctan(ctv)/2;
	cMapD:=new t_filter_color;
	cMapD.fill(0, cos(u)*cos(v), sin(u), cos(u)*sin(v), 0                    );
	cMapD.fill(1,-sin(u)*cos(v), cos(u),-sin(u)*sin(v),(sin(v)+cos(v))*sin(u));
	cMapD.fill(2,-sin(v),        0,      cos(v),        sin(v)               );
	cMapR:=new t_filter_color;
	cMapR.fill(0, cos(u)*cos(v),-sin(u)*cos(v),-sin(v), (sin(v)+cos(v))*sin(u)*sin(u)*cos(v) + sin(v)*sin(v));
	cMapR.fill(1, sin(u),        cos(u),        0,     -(sin(v)+cos(v))*sin(u)*cos(u)                       );
	cMapR.fill(2, cos(u)*sin(v),-sin(u)*sin(v), cos(v),-(sin(v)+cos(v))*cos(u)*cos(u)*sin(v) + sin(v)*sin(v));
	for i:=0 to 2 do begin
		cMapD.n[0,i] /=(sin(v)+cos(v))*cos(u)+sin(u);
		cMapD.n[1,i] /=(sin(v)+cos(v))*sin(u)+cos(u);
		cMapD.n[2,i] /= sin(v)+cos(v);
		cMapR.n[i,0] *=(sin(v)+cos(v))*cos(u)+sin(u);
		cMapR.n[i,1] *=(sin(v)+cos(v))*sin(u)+cos(u);
		cMapR.n[i,2] *= sin(v)+cos(v);
	end;
	cMapD.fill(3, x,y,z,0);
	cMapD.n[0,3] /=(sin(v)+cos(v))*cos(u)+sin(u);
	cMapD.n[1,3] /=(sin(v)+cos(v))*sin(u)+cos(u);
	cMapD.n[2,3] /= sin(v)+cos(v);
end;

procedure init_Space;
begin
	if not CCONV then begin
		cMapD:=new t_filter_color;
		cMapD.fill(0,1,0,0,0);
		cMapD.fill(1,0,1,0,0);
		cMapD.fill(2,0,0,1,0);
		cMapR:=new t_filter_color;
		cMapR.fill(0,1,0,0,0);
		cMapR.fill(1,0,1,0,0);
		cMapR.fill(2,0,0,1,0);
	end else begin
		cMapD:=new t_filter_color;
		cMapD.fill(0, 0.299,   0.587,   0.114,  0  );
		cMapD.fill(1,-0.14713,-0.28886, 0.436,  0.5);
		cMapD.fill(2, 0.615,  -0.51499,-0.10001,0.5);
		cMapR:=new t_filter_color;
		cMapR.fill(0, 1, 0,       1.13983,-0.569915);
		cMapR.fill(1, 1,-0.39465,-0.5806,  0.487625);
		cMapR.fill(2, 1, 2.03211, 0,      -1.016055);
	end;
end;

procedure chSp(var x:t_color; rev:boolean);
var
	clt:t_color;
	i,j:integer;
begin
	if rev then begin
		{$omp parallel for}
		for i:=0 to 2 do begin
			for j:=0 to 2 do
				clt[i]+=x[j]*cMapR.n[i,j];
			clt[i]+=cMapR.n[i,3]+cMapD.n[3,i];
		end;
	end else begin
		{$omp parallel for}
		for i:=0 to 2 do begin
			for j:=0 to 2 do
				clt[i]+=(x[j]-cMapD.n[3,j])*cMapD.n[i,j];
			clt[i]+=cMapD.n[i,3];
		end;
	end;
	x:=clt;
end;

