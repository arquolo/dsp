var
	s_globe:single;
	w_first:boolean:=true;

procedure Init(KIND:string; params f: array of t_filter);
	var
		p0,pt:array of picture;
		m0,mc:t_map;
		m1:array of t_map;
		s,kp,noiseLvl:array of single;
		stl,lftk,lft:string;
		z,q,p,n:integer;
	begin
		if w_first then begin
			setwindowsize(200,200);
			centerwindow;
			window.IsFixedSize:=true;
			w_first:=false;
		end;
		setlength(s, f.length);
		setlength(kp,f.length);
		setlength(m1,f.length);
		setlength(noiseLvl,f.length);
		
		n:=0;
		while fileexists(inttostr(n)+'.png') do begin
			setlength(p0,n+1);
			p0[n]:=new Picture(inttostr(n)+'.png');
			n+=1;
		end;
		setlength(pt,n);
		
		if CCUST and CCONV then begin
			mc:=new t_map(n,p0[0].Height,p0[0].Width,f[f.length-1]);
			for z:=0 to n-1 do
				mc.scan_native(p0[z],z);
			init_Space_adapt(mc);
		end else
			init_Space;
		
		m0:=new t_map(n,p0[0].Height,p0[0].Width,f[f.length-1]);
		for z:=0 to n-1 do
			m0.scan(p0[z],z);
		if w_first then window.Caption:=KIND;
		
		q:=-1;
		repeat
			q+=1;
			m1[q]:=new t_map(n,p0[0].Height,p0[0].Width,f[q]);
			for z:=0 to n-1 do
				m1[q].scan(p0[z],z);
		
			run_Analysis(m1[q],f[q]);
			if n=1 then begin
				pt[0]:=new Picture(m1[0].sX, m1[0].sY);
				m1[0].print_core(pt[0],1,1,0);
				setwindowsize(m1[0].sX, m1[0].sY);
				centerwindow;
				pt[0].Draw(0,0);
				redraw;
				readln;
			end;
			init_Quant(m1[q],noiseLvl[q]);
			run_Quantize(m1[q],noiseLvl[q],kp[q]);
			run_Synthesys(m1[q],f[q]);
			
			s[q]:=SSIM(m0,m1[q]);
		until (s[q]<s[max(q-1,0)]) or (q+1=f.length);
		if (s[q]<s[max(q-1,0)]) then p:=q-1 else p:=q;
		
		if n>1 then begin
			for z:=0 to n-1 do begin
				pt[z]:=new Picture(m1[p].sX, m1[p].sY);
				m1[p].print_core(pt[z],1,1,z);
			end;
			setwindowsize(m1[p].sX, m1[p].sY);
			centerwindow;
		end else m1[0].print_core(pt[0],1,1,0);

		stl :=floattostr(round(-10000*log10(1.0000000001-s[p]))/1000); s_globe:=round(-10000000*log10(1.0000000001-s[p]))/1000000;
		lftk:=floattostr(round(100*(1-kp[p])*m0.sX*m0.sY*m0.sZ*3/1024)/100);
		lft :=floattostr(round(1000*(1-kp[p]))/10);
		window.Caption:=KIND+'-'+inttostr(f[p].data)+', sz = '+lftk+'kB ('+lft+'%), SSIM = '+stl+' dB';
		
		z:=0;
		if n>1 then
			while true do begin
				pt[z].Draw(0,0);
				redraw;
				z:=(z+1) mod n;
				readln;
			end
		else begin
			pt[0].Draw(0,0);
			redraw;
		end;
	end;
