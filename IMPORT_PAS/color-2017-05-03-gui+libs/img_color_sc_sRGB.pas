///Transform sRGB to CIEXYZ
function sRGBtoCIEXYZ(c: System.Drawing.Color): float3;
  const k=1/255;
  var r, g, b: float;
  begin
    r:=sRGBtoLinear(k*c.R);
    g:=sRGBtoLinear(k*c.G);
    b:=sRGBtoLinear(k*c.B);
    result[0] := 0.4124564*r + 0.3575761*g + 0.1804375*b;
    result[1] := 0.2126729*r + 0.7151522*g + 0.0721750*b;
    result[2] := 0.0193339*r + 0.1191920*g + 0.9503041*b;
  end;

///Transform CIEXYZ to sRGB
function CIEXYZtoSRGB(c: float3): System.Drawing.Color;
  var r,g,b:float;
  begin
    r :=   3.2404542*c[0] - 1.5371385*c[1] - 0.4985314*c[2];
    g := - 0.9692660*c[0] + 1.8760108*c[1] + 0.0415560*c[2];
    b :=   0.0556434*c[0] - 0.2040259*c[1] + 1.0572252*c[2];
    r:=LinearToSRGB(r);
    g:=LinearToSRGB(g);
    b:=LinearToSRGB(b);
    result:=Color.FromARGB(255,limitb(255*r),limitb(255*g),limitb(255*b));
  end;

///Transform CIEXYZ to sRGB
function CIEXYZtoSRGBcrop(c: float3): System.Drawing.Color;
  var r,g,b:float;
  begin
    r :=   3.2404542*c[0] - 1.5371385*c[1] - 0.4985314*c[2];
    g := - 0.9692660*c[0] + 1.8760108*c[1] + 0.0415560*c[2];
    b :=   0.0556434*c[0] - 0.2040259*c[1] + 1.0572252*c[2];
    if (r>=0) and (g>=0) and (b>=0) and (r<=1) and (g<=1) and (b<=1) then begin 
      r:=LinearToSRGB(r);
      g:=LinearToSRGB(g);
      b:=LinearToSRGB(b);
      result:=Color.FromARGB(255,limitb(255*r),limitb(255*g),limitb(255*b));
    end else result:=Color.Black;
  end;

end.
