///Transform CIERGB-1931 to CIEXYZ-1931 (E-opt)
function CIERGBtoCIEXYZ(c: System.Drawing.Color): float3;
  const k=1/255;
  var x,y,z:float;
  begin
    x:=0.4887180*c.R + 0.3106803*c.G + 0.2006017*c.B;
    y:=0.1762044*c.R + 0.8129847*c.G + 0.0108109*c.B;
    z:=                0.0102048*c.G + 0.9897952*c.B;
    result[0]:=x*k;
    result[1]:=y*k;
    result[2]:=z*k;
  end;

///Transform CIEXYZ-1931 to CIERGB-1931 (E-opt)
function CIEXYZtoCIERGB(c: float3): System.Drawing.Color;
  var r,g,b:float;
  begin
    r:= 2.3706743*c[0] - 0.9000405*c[1] - 0.4706338*c[2];
    g:=-0.5138850*c[0] + 1.4253036*c[1] + 0.0885814*c[2];
    b:= 0.0052982*c[0] - 0.0146949*c[1] + 1.0093968*c[2];
    result:=Color.FromARGB(255,limitb(255*r),limitb(255*g),limitb(255*b));
  end;

///Transform CIEXYZ-1931 to CIERGB-1931 (E-opt)
function CIEXYZtoCIERGBcrop(c: float3): System.Drawing.Color;
  var r,g,b:float;
  begin
    r:= 2.3706743*c[0] - 0.9000405*c[1] - 0.4706338*c[2];
    g:=-0.5138850*c[0] + 1.4253036*c[1] + 0.0885814*c[2];
    b:= 0.0052982*c[0] - 0.0146949*c[1] + 1.0093968*c[2];
    if (r>=0) and (g>=0) and (b>=0) and (r<=1) and (g<=1) and (b<=1)
      then result:=Color.FromARGB(255,limitb(255*r),limitb(255*g),limitb(255*b))
      else result:=Color.Black;
  end;
