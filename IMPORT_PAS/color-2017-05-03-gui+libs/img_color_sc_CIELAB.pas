///Transform CIEXYZ-1931 to CIELAB-1976
function CIEXYZtoCIELAB(c: float3): float3;
  var x,y,z:float;
  begin
    x:=funcLABf(c[0]/RWhite[0]);
    y:=funcLABf(c[1]/RWhite[1]);
    z:=funcLABf(c[2]/RWhite[2]);
    result[0]:=1.16*y-0.16;
    result[1]:=5.00*(x-y);
    result[2]:=2.00*(y-z);
  end;

///Transform CIELAB-1976 to CIEXYZ-1931
function CIELABtoCIEXYZ(c: float3): float3;
  var xx,yy,zz:float;
  begin
    yy:=(c[0]+0.16)/1.16;
    xx:=yy+0.2*c[1];
    zz:=yy-0.5*c[2];
    result[0]:=funcLABr(xx)*RWhite[0];
    result[1]:=funcLABr(yy)*RWhite[1];
    result[2]:=funcLABr(zz)*RWhite[2];
  end;
