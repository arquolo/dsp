function IPTnonlin(x:float):float:=(x<0 ? -power(-x,0.43) : power(x,0.43));
function IPTnlback(x:float):float:=(x<0 ? -power(-x,1/0.43) : power(x,1/0.43));

///Transform CIEXYZ to IPT
function CIEXYZtoIPT(c: float3): float3;
  var x,y,z,lS,mS,sS:float;
  begin
    x := c[0]/RWhite[0];
    y := c[1]/RWhite[1];
    z := c[2]/RWhite[2];
    lS := IPTnonlin( 0.38971*x + 0.68898*y - 0.07868*z); //Hunt-Pointer-Estevez transformation matrix
    mS := IPTnonlin(-0.22981*x + 1.18340*y + 0.04641*z); //M(HPE)
    sS := IPTnonlin(                                 z);
    result[0] := 0.4000*lS + 0.4000*mS + 0.2000*sS;
    result[1] := 4.4550*lS - 4.8510*mS + 0.3960*sS;
    result[2] := 0.8056*lS + 0.3572*mS - 1.1628*sS;
  end;

///Transform IPT to CIEXYZ
function IPTtoCIEXYZ(c: float3): float3;
  var l,m,s:float;
  begin
    l := IPTnlback(c[0] + 0.09756891*c[1] + 0.20522643*c[2]);
    m := IPTnlback(c[0] - 0.11387649*c[1] + 0.13321716*c[2]);
    s := IPTnlback(c[0] + 0.03261511*c[1] - 0.67688718*c[2]);
    result[0] :=(1.91019683*l - 1.11212389*m + 0.20190796*s)*RWhite[0];
    result[1] :=(0.37095009*l + 0.62905426*m - 0.00000806*s)*RWhite[1];
    result[2] := s*RWhite[2];
  end;

///Transform CIEXYZ to IPT-Euclidean
function CIEXYZtoIPTE(c: float3): float3;
  var
    at:float3;
    G,k:float;
  begin
    at:=CIEXYZtoIPT(c);
    result[0]:=0.22*3.267936*ln(1 + 3*at[0]);
    if (at[1]=0) and (at[2]=0) then begin
      result[1]:=0;
      result[2]:=0;
    end else begin
      G:=sqrt(at[1]*at[1]+at[2]*at[2]);
      k:=0.92*ln(1 + 3*G)/(3*G);
      result[1]:=at[1]*k;
      result[2]:=at[2]*k;
    end;
  end;

///Transform IPT-Euclidean to CIEXYZ
function IPTEtoCIEXYZ(c: float3): float3;
  var
    at:float3;
    G,k:float;
  begin
    at[0]:=(exp(c[0]/0.22/3.267936) - 1)/3;
    if (c[1]=0) and (c[2]=0) then begin
      at[1]:=0;
      at[2]:=0;
    end else begin
      G:=sqrt(c[1]*c[1]+c[2]*c[2]);
      k:=(exp(3*G/0.92) - 1)/(3*G);
      at[1]:=c[1]*k;
      at[2]:=c[2]*k;
    end;
    result:=IPTtoCIEXYZ(at);
  end;
