///Transform CIEXYZ to DIN99
function CIEXYZtoDIN99(c: float3): float3;
  const
    ct=cos(4*Pi/45);
    st=sin(4*Pi/45);
  var
    at:float3;
    px,py,G,k: float;
  begin
    at:=CIEXYZtoCIELAB(c); 
    result[0] := 1.055*ln(1+1.58*at[0]);
    if (at[1]=0) and (at[2]=0) then begin
      result[1]:=0;
      result[2]:=0;
    end else begin
      px := at[1]*ct + at[2]*st;
      py :=(at[2]*ct - at[1]*st)*0.7;
      G :=sqrt(px*px+py*py);
      k:=ln(1+4.5*G)/4.5/G;
      result[1]:=k*px;
      result[2]:=k*py;
    end;
  end;

///Transform DIN99 to CIEXYZ
function DIN99toCIEXYZ(c: float3): float3;
  const
    ct=cos(4*Pi/45);
    st=sin(4*Pi/45);
  var
    at:float3;
    px,py,G,k: float;
  begin
    at[0] := (exp(c[0]/1.055)-1)/1.58;
    if (c[1]=0) and (c[2]=0) then begin
      at[1] := 0;
      at[2] := 0;
    end else begin
      G :=sqrt(c[1]*c[1]+c[2]*c[2]);
      k:=(exp(4.5*G)-1)/4.5/G;
      px :=c[1]*k;
      py :=c[2]*k/0.7;
      at[1] := px*ct - py*st;
      at[2] := py*ct + px*st;
    end;
    result:=CIELABtoCIEXYZ(at);
  end;


///Transform CIEXYZ to DIN99b/DIN99o, Euclidean
function CIEXYZtoDIN99b(c: float3): float3;
  const
    ang=26*Pi/180;
    ct=cos(26*Pi/180);
    st=sin(26*Pi/180);
  var
    at:float3;
    px,py,G,h: float;
  begin
    at:=CIEXYZtoCIELAB(c);
    result[0] := 3.03671*ln(1+0.39*at[0]);
    if (at[1]=0) and (at[2]=0) then begin
      result[1]:=0;
      result[2]:=0;
    end else begin
      px := at[1]*ct + at[2]*st;
      py :=(at[2]*ct - at[1]*st)*0.83;
      G :=sqrt(px*px+py*py);
      G:=0.23*ln(1 + 7.5*G);
      h:=atan2(py,px) + ang;
      result[1]:=G*cos(h);
      result[2]:=G*sin(h);
    end;
  end;

///Transform DIN99b/DIN99o to CIELAB, Euclidean
function DIN99BtoCIEXYZ(c: float3): float3;
  const
    ang=26*Pi/180;
    ct=cos(26*Pi/180);
    st=sin(26*Pi/180);
  var
    at:float3;
    px,py,G,h: float;
  begin
    at[0] := (exp(c[0]/3.03671)-1)/0.39;
    if (c[1]=0) and (c[2]=0) then begin
      at[1]:=0;
      at[2]:=0;
    end else begin
      G :=(exp(sqrt(c[1]*c[1]+c[2]*c[2])/0.23)-1)/7.5;
      h :=atan2(c[2],c[1])-ang;
      px:=G*cos(h);
      py:=G*sin(h)/0.83;
      at[1]:= px*ct - py*st;
      at[2]:= py*ct + px*st;
    end;
    result:=CIELABtoCIEXYZ(at);
  end;


///Transform CIEXYZ to DIN99c, Euclidean
function CIEXYZtoDIN99c(c: float3): float3;
  var
    at:float3;
    px,py,G,h: float;
  begin
    at:=c;
    at[0]:=1.1*at[0]-0.1*at[2];
    at:=CIEXYZtoCIELAB(at);
    result[0] := 3.17651*ln(1+0.37*at[0]);
    if (at[1]=0) and (at[2]=0) then begin
      result[1]:=0;
      result[2]:=0;
    end else begin
      px := at[1];
      py := at[2]*0.94;
      G :=sqrt(px*px+py*py);
      G:=0.23*ln(1 + 6.6*G);
      h:=atan2(py,px);
      result[1]:=G*cos(h);
      result[2]:=G*sin(h);
    end;
  end;

///Transform DIN99c to CIELAB, Euclidean
function DIN99CtoCIEXYZ(c: float3): float3;
  var
    at:float3;
    G,h: float;
  begin
    at[0] := (exp(c[0]/3.17651)-1)/0.37;
    if (c[1]=0) and (c[2]=0) then begin
      at[1]:=0;
      at[2]:=0;
    end else begin
      G :=sqrt(c[1]*c[1]+c[2]*c[2]);
      G:=(exp(G/0.23)-1)/6.6;
      h :=atan2(c[2],c[1]);
      at[1]:=G*cos(h);
      at[2]:=G*sin(h)/0.94;
    end;
    at:=CIELABtoCIEXYZ(at);
    at[0]:=(at[0]+0.1*at[2])/1.1;
    result:=at;
  end;


///Transform CIEXYZ to DIN99d, Euclidean
function CIEXYZtoDIN99d(c: float3): float3;
  const
    ang=50*Pi/180;
    ct=cos(50*Pi/180);
    st=sin(50*Pi/180);
  var
    at:float3;
    px,py,G,h: float;
  begin
    at:=c;
    at[0]:=1.12*at[0]-0.12*at[2];
    at:=CIEXYZtoCIELAB(at);
    result[0] := 3.25221*ln(1+0.36*at[0]);
    if (at[1]=0) and (at[2]=0) then begin
      result[1]:=0;
      result[2]:=0;
    end else begin
      px := at[1]*ct + at[2]*st;
      py :=(at[2]*ct - at[1]*st)*1.14;
      G :=sqrt(px*px+py*py);
      G:=0.225*ln(1+6*G);
      h:=atan2(py,px)+ang;
      result[1]:=G*cos(h);
      result[2]:=G*sin(h);
    end;
  end;

///Transform DIN99d to CIELAB, Euclidean
function DIN99DtoCIEXYZ(c: float3): float3;
  const
    ang=50*Pi/180;
    ct=cos(50*Pi/180);
    st=sin(50*Pi/180);
  var
    at:float3;
    px,py,G,h: float;
  begin
    at[0] := (exp(c[0]/3.25221)-1)/0.36;
    if (c[1]=0) and (c[2]=0) then begin
      at[1] := 0;
      at[2] := 0;
    end else begin
      G :=sqrt(c[1]*c[1]+c[2]*c[2]);
      G :=(exp(G/0.225)-1)/6;
      h :=atan2(c[2],c[1])-ang;
      px:=G*cos(h);
      py:=G*sin(h)/1.14;
      at[1]:=px*ct - py*st;
      at[2]:=py*ct + px*st;
    end;
    at:=CIELABtoCIEXYZ(at);
    at[0]:=(at[0]+0.12*at[2])/1.12;
    result:=at;
  end;
