function CIEXYZtoCIELUV(c: float3): float3;
  var L,uu,vv,d:float;
  begin
    L:=funcLUVf(c[1]/RWhite[1]);
    result[0]:=L;
    d:=c[0]+15*c[1]+3*c[2];
    if L>0 then begin
      uu:=4*c[0]/d;
      vv:=9*c[1]/d;
      result[1]:=13*L*(uu-RWhiteU);
      result[2]:=13*L*(vv-RWhiteV);
    end else begin
      result[1]:=0;
      result[2]:=0;
    end;
  end;

function CIELUVtoCIEXYZ(c: float3): float3;
  var u,v,x,y,z:float;
  begin
    if c[0]>0 then begin
      u:=c[1]/13/c[0]+RWhiteU;
      v:=c[2]/13/c[0]+RWhiteV;
      y:=funcLUVr(c[0])*RWhite[1];
      if v<>0 then begin
        x := y * 9*u/4/v;
        z := y * (12-3*u-20*v)/4/v;
      end else begin
        x := 0;
        z := 0;
      end;
    end else begin
      x := 0;
      y := 0;
      z := 0;
    end;
    result[0]:=x;
    result[1]:=y;
    result[2]:=z;
  end;
