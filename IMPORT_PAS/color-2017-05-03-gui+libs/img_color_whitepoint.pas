unit img_color_whitepoint;
uses
  img_color_math;

const
  white*:  float3=(*, *, *);

var
  RWhite: float3;
  RWhiteU, RWhiteV, kHuntA, kHuntB: float;

procedure updateIllum;
  begin
    var di := (RWhite[0]+15*RWhite[1]+3*RWhite[2]);
    RWhiteU := 4*RWhite[0]/di;
    RWhiteV := 9*RWhite[1]/di;
    kHuntA :=175/198.04*(RWhite[0] + RWhite[1]);
    kHuntB := 70/218.11*(RWhite[1] + RWhite[2]);
  end;

end.
