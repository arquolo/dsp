///Transform CIEXYZ to HunterLab
function CIEXYZtoHunterLab(c: float3): float3;
  var L:float;
  begin
    L:=sqrt(c[1]/RWhite[1]);
    result[0]:=L;
    if L>0 then begin
      result[1]:=kHuntA*(c[0]/RWhite[0]-c[1]/RWhite[1])/result[0];
      result[2]:=kHuntB*(c[1]/RWhite[1]-c[2]/RWhite[2])/result[0];
    end else begin
      result[1]:=0;
      result[2]:=0;
    end;
  end;

///Transform HunterLab to CIEXYZ
function HunterLabToCIEXYZ(c: float3): float3;
  var Y:float;
  begin
    Y:=sqr(c[0]);
    result[1]:=Y*RWhite[1];
    if Y>0 then begin
      result[0]:=(Y+c[1]*c[0]/kHuntA)*RWhite[0];
      result[2]:=(Y-c[2]*c[0]/kHuntB)*RWhite[2];
    end else begin
      result[0]:=0;
      result[2]:=0;
    end;
  end;

end.
