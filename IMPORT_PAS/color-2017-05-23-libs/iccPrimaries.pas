type
	p3w=array[0..3] of float2;

const
	iccWhite*:  float2=(*, *);
	
	p3w_bt601_NTSC: p3w=(iccWhiteD65, (0.630,0.340), (0.310,0.595), (0.155,0.070)); //bt601_525, SMPTE_C
	p3w_bt709:      p3w=(iccWhiteD65, (0.640,0.330), (0.300,0.600), (0.150,0.060)); //sRGB
	p3w_bt601_PAL:  p3w=(iccWhiteD65, (0.640,0.330), (0.290,0.600), (0.150,0.060)); //bt601_625
	p3w_bt2020:     p3w=(iccWhiteD65, (0.708,0.292), (0.170,0.797), (0.131,0.046));
	p3w_cie:        p3w=(iccWhiteE,   (0.735,0.265), (0.274,0.717), (0.167,0.009));

var
	pw: p3w;    //current whitepoint and primaries (xy)
	m3xyz: Matrix; //(rgb -> xyz) transform matrix
	m3rgb: Matrix; //(xyz -> rgb) transform matrix
	///whitepoint
	whiteX, whiteZ,        //XZ, Y is always 1
	whiteU, whiteV: float; //uvY

procedure iccSpecPrims(prm:string:='cie');
	var c3sum: float3;
	begin
		case prm of
		'cie':       pw:=p3w_cie;
		'bt601_NTSC':pw:=p3w_bt601_NTSC;
		'bt601_PAL': pw:=p3w_bt601_PAL;
		'bt709':     pw:=p3w_bt709;
		'bt2020':    pw:=p3w_bt2020;
		end;

		whiteX :=   pw[0][0]/pw[0][1];
		whiteZ :=(1-pw[0][0]-pw[0][1])/pw[0][1];
		whiteU := 4*pw[0][0] / (3-2*pw[0][0]+12*pw[0][1]);
		whiteV := 9*pw[0][1] / (3-2*pw[0][0]+12*pw[0][1]);
		
		m3rgb[0,0]:=pw[1][0];  m3rgb[1,0]:=pw[1][1];  m3rgb[2,0]:=1-pw[1][0]-pw[1][1];
		m3rgb[0,1]:=pw[2][0];  m3rgb[1,1]:=pw[2][1];  m3rgb[2,1]:=1-pw[2][0]-pw[2][1];
		m3rgb[0,2]:=pw[3][0];  m3rgb[1,2]:=pw[3][1];  m3rgb[2,2]:=1-pw[3][0]-pw[3][1];
		c3sum[0]:=whiteX;
		c3sum[1]:=1;
		c3sum[2]:=whiteZ;
		
		LinearSolve(m3rgb, c3sum);
		MatrixMultiply(m3rgb, c3sum);
		MatrixInverse(m3rgb, m3xyz);
	end;
