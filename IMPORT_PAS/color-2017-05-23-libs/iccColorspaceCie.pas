//m_mp(m_xyz)
procedure iccRgbToXyz(var c:float3);
	var X,Y,Z:float;
	begin
		X:=m3xyz[0,0]*c[0] + m3xyz[0,1]*c[1] + m3xyz[0,2]*c[2];
		Y:=m3xyz[1,0]*c[0] + m3xyz[1,1]*c[1] + m3xyz[1,2]*c[2];
		Z:=m3xyz[2,0]*c[0] + m3xyz[2,1]*c[1] + m3xyz[2,2]*c[2];
		c[0]:=X;
		c[1]:=Y;
		c[2]:=Z;
	end;

//m_mp(m_xyzi)
procedure iccXyzToRgb(var c:float3);
	var R,G,B:float;
	begin
		R:=m3rgb[0,0]*c[0] + m3rgb[0,1]*c[1] + m3rgb[0,2]*c[2];
		G:=m3rgb[1,0]*c[0] + m3rgb[1,1]*c[1] + m3rgb[1,2]*c[2];
		B:=m3rgb[2,0]*c[0] + m3rgb[2,1]*c[1] + m3rgb[2,2]*c[2];
		c[0]:=R;
		c[1]:=G;
		c[2]:=B;
	end;

//f_oetf_lab
//m_mp(m_lab)
procedure iccXyzToLab(var c:float3);
	var fx,fy,fz:float;
	begin
		fx:=iccLabDir(c[0]/whiteX);
		fy:=iccLabDir(c[1]);
		fz:=iccLabDir(c[2]/whiteZ);
		c[0]:=1.16*fy-0.16;
		c[1]:=5.00*(fx-fy);
		c[2]:=2.00*(fy-fz);
	end;

//m_mp(m_lab_i)
//f_eotf_lab
procedure iccLabToXyz(var c:float3);
	var fx,fy,fz:float;
	begin
		fy :=(c[0]+0.16)/1.16;
		fx := fy + 0.2*c[1];
		fz := fy - 0.5*c[2];
		c[0]:=iccLabInv(fx)*whiteX;
		c[1]:=iccLabInv(fy);
		c[2]:=iccLabInv(fz)*whiteZ;
	end;

//cf_xyz2luv
procedure iccXyzToLuv(var c:float3);
	var Lstar,uu,vv,d:float;
	begin
		Lstar:=iccLuvDir(c[1]);
		if Lstar>0 then begin
			d:=c[0]+15*c[1]+3*c[2];
			uu:=4*c[0]/d;
			vv:=9*c[1]/d;
			c[0]:=Lstar;
			c[1]:=Lstar*(uu-whiteU)*13;
			c[2]:=Lstar*(vv-whiteV)*13;
		end else begin
			c[0]:=0;
			c[1]:=0;
			c[2]:=0;
		end;
	end;

//cf_luv2xyz
procedure iccLuvToXyz(var c:float3);
	var u,v,Y:float;
	begin
		u:=c[1]/13/c[0] + whiteU;
		v:=c[2]/13/c[0] + whiteV;
		Y:=iccLuvInv(c[0]);
		if v<>0 then begin
			c[0] := Y * 2.25*u/v;
			c[1] := Y;
			c[2] := Y *(3.00-0.75*u-5.00*v)/v;
		end else begin
			c[0] := Y * whiteX;
			c[1] := Y;
			c[2] := Y * whiteZ;
		end;
	end;
