var
	rgb_to_ycc, ycc_to_rgb,
	rgb_to_yc2, yc2_to_rgb,
	rgb_to_lms, lms_to_rgb,
	rgb_to_icc,	icc_to_rgb,
	rgb_to_luv,	luv_to_rgb,
	rgb_to_lab,	lab_to_rgb,
	rgb_to_din,	din_to_rgb: warp3;

///tf1 = ('srgb', 'bt709', 'bt1886', 'bt1886crt')
///tf2 = ('hlg', 'pq')
///prm = ('cie', 'bt601_NTSC', 'bt601_PAL', 'bt709', 'bt2020')
///ycc = ('bt601', 'bt709', 'bt2020')
///icc = ('icacb', 'ictcp')
///din = ('din99', 'din99b', 'din99c', 'din99d')
procedure pragma_all(dim,amp:float; tf1,tf2,prm,ycc,icc,din:string);
begin
	iccSpecDisplay(dim,amp);
	iccSpecTransfer(tf1);
	iccSpecHdr  (tf2);
	iccSpecPrims(prm);
	iccSpecYcc  (ycc);
	iccSpecIcc  (icc);
	iccSpecDin  (din);

	rgb_to_ycc:=iccRgbToYcbcr;
	ycc_to_rgb:=iccYcbcrToRgb;
	
	rgb_to_yc2:=iccRgbToYcbcrL;
	yc2_to_rgb:=iccYcbcrLToRgb;
	
	rgb_to_lms:=iccGammaDir + iccRgbToXyz + iccXyzToLms;
	lms_to_rgb:=iccLmsToXyz + iccXyzToRgb + iccGammaInv;
	
	rgb_to_icc:=iccGammaDir + iccRgbToXyz + iccXyzToLms + oetf + iccLmsToIcc;
	icc_to_rgb:=iccIccToLms + eotf + iccLmsToXyz + iccXyzToRgb + iccGammaInv;
	
	rgb_to_luv:=iccGammaDir + iccRgbToXyz + iccXyzToLuv;
	luv_to_rgb:=iccLuvToXyz + iccXyzToRgb + iccGammaInv;
	
	rgb_to_lab:=iccGammaDir + iccRgbToXyz + iccXyzToLab;
	lab_to_rgb:=iccLabToXyz + iccXyzToRgb + iccGammaInv;
	
	rgb_to_din:=iccGammaDir + iccRgbToXyz + iccXyzPatchDir + iccXyzToLab + iccLabToDin;
	din_to_rgb:=iccDinToLab + iccLabToXyz + iccXyzPatchInv + iccXyzToRgb + iccGammaInv;
end;
