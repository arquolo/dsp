var
	Kr, Kg, Kb,
	iKr, iPr, iNr, iKb, iPb, iNb,
	rKr, rPr, rNr, rKb, rPb, rNb:float;
	
procedure iccSpecYcc(ycc:string:='bt2020');
	begin
		case ycc of
		'bt601':
			begin
				Kr:=0.299;
				Kb:=0.114;
			end;
		'bt709':
			begin
				Kr:=0.2126;
				Kb:=0.0722;
			end;
		'bt2020':
			begin
				Kr:=0.2627;
				Kb:=0.0593;
			end;
		end;
		Kg:=1-Kr-Kb;
		iKb := 2*(1-Kb); rKb:=1/iKb;
		iKr := 2*(1-Kr); rKr:=1/iKr;
		iPb := 2*tf_a1 * (1-power(Kb,tf_deg)); rPb:=1/iPb;
		iPr := 2*tf_a1 * (1-power(Kr,tf_deg)); rPr:=1/iPr;
		iNb := 2 - 2*tf_a1 * (1-power(1-Kb,tf_deg)); rNb:=1/iNb;
		iNr := 2 - 2*tf_a1 * (1-power(1-Kr,tf_deg)); rNr:=1/iNr;
	end;

///optimized for BT.601, BT.709, BT.2020
//m_mp(m_ycc)
procedure iccRgbToYcbcr(var x:float3);
	var Y,Cb,Cr: float;
	begin
		Y := x[0]*Kr + x[1]*Kg + x[2]*Kb;
		Cb:= (x[2]-y)*rKr;
		Cr:= (x[0]-y)*rKb;
		x[0]:=Y;
		x[1]:=Cb;
		x[2]:=Cr;
	end;

///optimized for BT.601, BT.709, BT.2020
//m_mp(m_ycci)
procedure iccYcbcrToRgb(var x:float3);
	var R,G,B: float;
	begin
		R:=x[0]         +iKr*x[2];
		G:=x[0]-iKb*x[1]-iKr*x[2];
		B:=x[0]+iKb*x[1];
		x[0]:=R;
		x[1]:=G;
		x[2]:=B;
	end;

///optimized for BT.2020
//m_mp(m_yccl)
procedure iccRgbToYcbcrL(var x:float3);
	var Yc,Cbc,Crc: float;
	begin
		Yc:=pipe1_iccGammaInv(
			pipe1_iccGammaDir(x[0])*Kr + 
			pipe1_iccGammaDir(x[1])*Kg + 
			pipe1_iccGammaDir(x[2])*Kb
		);
		Cbc := (x[2]>Yc ? rPb : rNb) * (x[2]-Yc);
		Crc := (x[0]>Yc ? rPr : rNr) * (x[0]-Yc);
		x[0]:=Yc;
		x[1]:=Cbc;
		x[2]:=Crc;
	end;

///optimized for BT.2020
//m_mp(m_yccli)
procedure iccYcbcrLToRgb(var x:float3);
	var R,G,B: float;
	begin
		R:= x[0] + (x[2]>0 ? iPr : iNr) * x[2];
		B:= x[0] + (x[1]>0 ? iPb : iNb) * x[1];
		G:= pipe1_iccGammaInv((
			pipe1_iccGammaDir(x[0]) -
			pipe1_iccGammaDir(B)*Kb -
			pipe1_iccGammaDir(R)*Kr
		)/Kg);
		x[0]:=R;
		x[1]:=G;
		x[2]:=B;
	end;

procedure iccRgbToYcgco(var x:float3);
	var Y,Co,Cg: float;
	begin
		Y := 0.25*x[0] + 0.50*x[1] + 0.25*x[2];
		Cg:=-0.25*x[0] + 0.50*x[1] - 0.25*x[2];
		Co:= 0.50*x[0]             - 0.50*x[2];
		x[0]:=Y;
		x[1]:=Cg;
		x[2]:=Co;
	end;

procedure iccYcgcoToRgb(var x:float3);
	var R,G,B: float;
	begin
		R:=x[0] - x[1] + x[2];
		G:=x[0] + x[1];
		B:=x[0] - x[1] - x[2];
		x[0]:=R;
		x[1]:=G;
		x[2]:=B;
	end;
