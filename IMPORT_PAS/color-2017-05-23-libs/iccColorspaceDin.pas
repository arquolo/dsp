var
	dn_an1, dn_an2,
	dn_ct, dn_st,
	dn_shift,
	dn_L1,dn_L2,
	dn_C1,dn_C2,
	dn_py: float;

procedure iccSpecDin(din:string:='din99');
	begin
		case din of
		'din99':  //1999, Rohner and Rich
			begin
				dn_shift:=0.0;
				dn_L1:=1.0551;     dn_L2:=1.58;
				dn_C1:=1/4.5;      dn_C2:=4.5;
				dn_an1:=Pi*16/180; dn_an2:=0;
				dn_py:=0.7;
			end;
		'din99b': //2001, Cui and Luo
			begin
				dn_shift:=0.0;
				dn_L1:=3.03671;    dn_L2:=0.39;
				dn_C1:=0.23;       dn_C2:=7.5;
				dn_an1:=Pi*26/180; dn_an2:=dn_an1;
				dn_py:=0.83;
			end;
		'din99c': //2001, Cui and Luo
			begin
				dn_shift:=0.1; //optimization of blue region
				dn_L1:=3.17651; dn_L2:=0.37;
				dn_C1:=0.23;    dn_C2:=6.6;
				dn_an1:=0;		dn_an2:=0;
				dn_py:=0.94;
			end;
		'din99d': //2001, Cui and Luo
			begin
				dn_shift:=0.12; //optimization of blue region
				dn_L1:=3.25221;    dn_L2:=0.36; 
				dn_C1:=0.225;      dn_C2:=6.0;
				dn_an1:=Pi*50/180; dn_an2:=dn_an1;
				dn_py:=1.14;
			end;
		end;
		dn_ct:=cos(dn_an1);
		dn_st:=sin(dn_an1);
	end;

//m_mp(m_xyzp)
procedure iccXyzPatchDir(var c:float3);
	begin
		// [(1+s) 0 (-s)]
		// [  0   1   0 ]
		// [  0   0   1 ]
		c[0] := c[0]*(1+dn_shift) - c[2]*dn_shift;
	end;

//m_mp(m_xyzpi)
procedure iccXyzPatchInv(var c:float3);
	begin
		// [1/(1+s) 0 s/(1+s)]
		// [   0    1    0   ]
		// [   0    0    1   ]
		c[0]:=(c[0] + c[2]*dn_shift)/(1+dn_shift);
	end;

//cf_lab2din
procedure iccLabToDin(var c:float3);
	var px,py,G,h: float;
	begin
		c[0]:=dn_L1*ln(1 + c[0]*dn_L2);
		if (c[1]<>0) or (c[2]<>0) then begin
			px := c[1]*dn_ct + c[2]*dn_st;
			py :=(c[2]*dn_ct - c[1]*dn_st)*dn_py;
			G :=sqrt(px*px+py*py);
			G:=dn_C1*ln(1 + G*dn_C2);
			h:=atan2(py,px) + dn_an2;
			c[1]:=G*cos(h);
			c[2]:=G*sin(h);
		end;
	end;

//cf_din2lab
procedure iccDinToLab(var c:float3);
	var px,py,G,h: float;
	begin
		c[0]:=(exp(c[0]/dn_L1) - 1)/dn_L2;
		if (c[1]<>0) or (c[2]<>0) then begin
			G :=sqrt(c[1]*c[1]+c[2]*c[2]);
			G :=(exp(G/dn_C1) - 1)/dn_C2;
			h :=atan2(c[2],c[1])-dn_an2;
			px:=G*cos(h);
			py:=G*sin(h)/dn_py;
			c[1] := px*dn_ct - py*dn_st;
			c[2] := py*dn_ct + px*dn_st;
		end;
	end;
