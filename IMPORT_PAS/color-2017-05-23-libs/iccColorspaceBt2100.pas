var
	pipe1_oetf,
	pipe1_eotf: warp1;
	mLmsDir, mLmsInv,
	mIccDir, mIccInv: Matrix;

procedure oetf(var x:float3);
	begin
		x[0]:=pipe1_oetf(x[0] * hdr_coverage);
		x[1]:=pipe1_oetf(x[1] * hdr_coverage);
		x[2]:=pipe1_oetf(x[2] * hdr_coverage);
	end;

procedure eotf(var x:float3);
	begin
		x[0]:=pipe1_eotf(x[0]) * sdr_coverage;
		x[1]:=pipe1_eotf(x[1]) * sdr_coverage;
		x[2]:=pipe1_eotf(x[2]) * sdr_coverage;
	end;


//m_mp(m_lms)
procedure iccXyzToLms(var c:float3);
	var L,M,S:float;
	begin
		L := mLmsDir[0,0]*c[0] + mLmsDir[0,1]*c[1] + mLmsDir[0,2]*c[2]; //red cone
		M := mLmsDir[1,0]*c[0] + mLmsDir[1,1]*c[1] + mLmsDir[1,2]*c[2]; //green cone
		S := mLmsDir[2,0]*c[0] + mLmsDir[2,1]*c[1] + mLmsDir[2,2]*c[2]; //blue cone
		c[0]:=L; c[1]:=M; c[2]:=S;
	end;

//m_mp(m_lmsi)
procedure iccLmsToXyz(var c:float3);
	var X,Y,Z:float;
	begin
		X := mLmsInv[0,0]*c[0] + mLmsInv[0,1]*c[1] + mLmsInv[0,2]*c[2];
		Y := mLmsInv[1,0]*c[0] + mLmsInv[1,1]*c[1] + mLmsInv[1,2]*c[2];
		Z := mLmsInv[2,0]*c[0] + mLmsInv[2,1]*c[1] + mLmsInv[2,2]*c[2];
		c[0]:=X; c[1]:=Y; c[2]:=Z;
	end;

//m_mp(m_icc)
procedure iccLmsToIcc(var x:float3);
	var I,C1,C2:float;
	begin
		I  := mIccDir[0,0]*x[0] + mIccDir[0,1]*x[1] + mIccDir[0,2]*x[2]; //I;
		C1 := mIccDir[1,0]*x[0] + mIccDir[1,1]*x[1] + mIccDir[1,2]*x[2]; //Ca/Ct;
		C2 := mIccDir[2,0]*x[0] + mIccDir[2,1]*x[1] + mIccDir[2,2]*x[2]; //Cb/Cp;
		x[0]:=I; x[1]:=C1; x[2]:=C2;
	end;

//m_mp(m_icci)
procedure iccIccToLms(var x:float3);
	var L,M,S:float;
	begin
		L:= mIccInv[0,0]*x[0] + mIccInv[0,1]*x[1] + mIccInv[0,2]*x[2]; //red cone
		M:= mIccInv[1,0]*x[0] + mIccInv[1,1]*x[1] + mIccInv[1,2]*x[2]; //green cone
		S:= mIccInv[2,0]*x[0] + mIccInv[2,1]*x[1] + mIccInv[2,2]*x[2]; //blue cone
		x[0]:=L; x[1]:=M; x[2]:=S;
	end;

///'icacb' - derivative of IPT from 2015
///'ictcp' - derivative of IPT from 2016, ITU-R Rec. BT.2100
procedure iccSpecIcc(icc:string);
	begin
		case icc of
		'icacb':
			begin
				mLmsDir:=mIcacb1; 
				mIccDir:=mIcacb2;
			end;
		'ictcp':
			begin
				mLmsDir:=mIctcp1; 
				mIccDir:=mIctcp2;
			end;
		end;
		MatrixInverse(mLmsDir, mLmsInv);
		MatrixInverse(mIccDir, mIccInv);
	end;
