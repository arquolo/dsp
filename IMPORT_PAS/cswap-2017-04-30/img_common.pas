type
	par_ptc4 = (SHIFT, DEF, REMIX);
	//ctype = smallint;
	//ctypebig = integer;
	ctype = integer;
	ctypebig = int64;
	ctype4 = array[0..3] of ctype;

var
	h,w,h2,w2: integer;
	arrTm: array[,] of ctype;
	cell4: array[,] of ctype4;
	debug_str: string:='';

///arrTm->cells
procedure PixToCell4(bMode:par_ptc4);
	var	u1,u2,v1,v2: integer;
	begin
		case bMode of
		SHIFT: 
			begin
				setlength(cell4,h2+1,w2+1);
				{$omp parallel for private(u1,u2,v1,v2)}
				for var y:=0 to h2 do
				for var x:=0 to w2 do begin
					u1:=max(0,2*x-1); v1:=max(0,2*y-1);
					u2:=min(w-1,2*x); v2:=min(h-1,2*y);
					cell4[y,x][0]:=arrTm[v1,u1];
					cell4[y,x][1]:=arrTm[v1,u2];
					cell4[y,x][2]:=arrTm[v2,u1];
					cell4[y,x][3]:=arrTm[v2,u2];
				end;
			end;
		DEF:
			begin
				setlength(cell4,h2,w2);
				{$omp parallel for}
				for var y:=0 to h2-1 do
				for var x:=0 to w2-1 do begin
					cell4[y,x][0]:=arrTm[2*y,  2*x  ];
					cell4[y,x][1]:=arrTm[2*y,  2*x+1];
					cell4[y,x][2]:=arrTm[2*y+1,2*x  ];
					cell4[y,x][3]:=arrTm[2*y+1,2*x+1];
				end;
			end;
		REMIX:
			begin
				setlength(cell4,h2,w2);
				{$omp parallel for}
				for var y:=0 to h2-1 do
				for var x:=0 to w2-1 do begin
					cell4[y,x][0]:=arrTm[y,   x   ];
					cell4[y,x][1]:=arrTm[y,   x+w2];
					cell4[y,x][2]:=arrTm[y+h2,x   ];
					cell4[y,x][3]:=arrTm[y+h2,x+w2];
				end;
			end;
		end;
	end;

///cells->arrTm
procedure Cell4ToPix(bMode:par_ptc4);
	var u1,u2,v1,v2: integer;
	begin
		case bMode of
		SHIFT: 
			begin
				{$omp parallel for private(u1,u2,v1,v2)}
				for var y:=0 to h2 do
				for var x:=0 to w2 do begin
					u1:=max(0,2*x-1); v1:=max(0,2*y-1);
					u2:=min(w-1,2*x); v2:=min(h-1,2*y);
					arrTm[v1,u1]:=cell4[y,x][0];
					arrTm[v1,u2]:=cell4[y,x][1];
					arrTm[v2,u1]:=cell4[y,x][2];
					arrTm[v2,u2]:=cell4[y,x][3];
				end;
			end;
		DEF:
			begin
				{$omp parallel for}
				for var y:=0 to h2-1 do
				for var x:=0 to w2-1 do begin
					arrTm[2*y,  2*x  ]:=cell4[y,x][0];
					arrTm[2*y,  2*x+1]:=cell4[y,x][1];
					arrTm[2*y+1,2*x  ]:=cell4[y,x][2];
					arrTm[2*y+1,2*x+1]:=cell4[y,x][3];
				end;
			end;
		REMIX:
			begin
				{$omp parallel for}
				for var y:=0 to h2-1 do
				for var x:=0 to w2-1 do begin
					arrTm[y,   x   ]:=cell4[y,x][0];
					arrTm[y,   x+w2]:=cell4[y,x][1];
					arrTm[y+h2,x   ]:=cell4[y,x][2];
					arrTm[y+h2,x+w2]:=cell4[y,x][3];
				end;
			end;
		end;
	end;
