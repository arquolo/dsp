var
	Psy: real;
	Layers, GQuantizer, pow, std: integer;

///arrTm: quantize
procedure Quantize;
var disp: real;
begin
  if GQuantizer>1 then begin
    if (Psy>0) then for var zid:=0 to Layers-1 do begin
      h:=z[zid].rc.Y; h2:=z[zid].rc2.Y;
      w:=z[zid].rc.X; w2:=z[zid].rc2.X;
      for var yy:=0 to 1 do
      for var xx:=0 to 1 do if (xx+yy>0) then begin
        disp:=0;
        {$omp parallel for reduction(+:disp)}
        for var y:=h2*yy to h2*(yy+1)-1 do
        for var x:=w2*xx to w2*(xx+1)-1 do begin
          var tmp1   := round(arrTm[y,x]/GQuantizer);
          var tmp2   := arrTm[y,x] - tmp1*GQuantizer;
          arrTm[y,x] := tmp1;
          disp += tmp2*tmp2;
        end;
        z[zid].disp[yy,xx]:=sqrt(disp/w2/h2);
        if pow=1 then z[zid].disp[yy,xx]*=power(2,-zid);
      end;
    end else
    {$omp parallel for}
    for var y:=0 to hG-1 do
    for var x:=0 to wG-1 do
      arrTm[y,x] := round(arrTm[y,x]/GQuantizer);
  end;
end;

///arrTm: unquantize
procedure UnQuantize;
var disp: real;
begin
  if GQuantizer>1 then begin
    if (Psy>0) then for var zid:=Layers-1 downto 0 do begin
      h:=z[zid].rc.Y; h2:=z[zid].rc2.Y;
      w:=z[zid].rc.X; w2:=z[zid].rc2.X;
      for var yy:=0 to 1 do
      for var xx:=0 to 1 do if (xx+yy>0) then begin
      	disp := z[zid].disp[yy,xx] * Psy;
      	if std=1 then
      	  {$omp parallel for}
      	  for var y:=h2*yy to h2*(yy+1)-1 do
      	  for var x:=w2*xx to w2*(xx+1)-1 do
      	  	arrTm[y,x] := round(GQuantizer*arrTm[y,x] + randn(disp))
      	else
      	  {$omp parallel for}
      	  for var y:=h2*yy to h2*(yy+1)-1 do
      	  for var x:=w2*xx to w2*(xx+1)-1 do
      	    arrTm[y,x] := round(GQuantizer*arrTm[y,x] + randr(disp));
      end;
    end else
      {$omp parallel for}
      for var y:=0 to hG-1 do
      for var x:=0 to wG-1 do arrTm[y,x] := GQuantizer*arrTm[y,x];
  end;
end;

procedure GetStatsSize;
var nnzv: uint64;
begin
  nnzv:=0;
  {$omp parallel for reduction(+:nnzv)}
  for var y:=0 to hG-1 do
  for var x:=0 to wG-1 do if (arrTm[y,x]<>0) then nnzv+=1;
  debug_str :=
    inttostr(SearchOpts) +
    ':k=' +
    floattostr(round(10000*nnzv/hG/wG)/10000) +
    ' (' +
    floattostr(round(10*nnzv/1024)/10) +
    ' KiB)';
end;
