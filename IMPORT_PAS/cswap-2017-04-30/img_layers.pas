type
	ZonePf = record
		rc,
		rc2: Point;
		disp: real22;
		map: array[,] of byte;
	end;

var
	hG, wG: integer;
	Layers: integer;
	z:array of ZonePf;

///z->(blmap,coords,divid)
procedure LoadZone(zid:integer);
	begin
		h:=z[zid].rc.Y; h2:=z[zid].rc2.Y;
		w:=z[zid].rc.X; w2:=z[zid].rc2.X;
		blmap:=copy(z[zid].map);
	end;

///(blmap,coords,divid)->z
procedure SaveZone(zid:integer);
	begin
		z[zid].map:=copy(blmap);
	end;
