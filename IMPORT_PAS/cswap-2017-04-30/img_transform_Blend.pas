var
	keys: array of par_bltf;

///Householder's Transform
procedure TransformBlend(bDir:boolean);
	begin
		if bDir then begin
			SetLength(cell4tmp,h2+1,w2+1,SearchOpts);
			for var key:=0 to SearchOpts-1 do Blend_global(key,keys[key]);
			GetBlendMap;
			{$omp parallel for}
			for var y:=0 to h2 do
			for var x:=0 to w2 do cell4[y,x] := cell4tmp[y,x,blmap[y,x]];
		end else begin
			{$omp parallel for}
			for var y:=0 to h2 do
			for var x:=0 to w2 do Blend_atomic(x,y,keys[blmap[y,x]]);
		end;
	end;
