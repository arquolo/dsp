type
  par_prdc = (PF_NONE, PF_AVERAGE, PF_PAETH);

var
  PQuantizer: integer;
  PreFilter, PostFilter: par_prdc;

procedure ToPrePCM;
var pred:ctype;
begin
  var arr:=copy(arrTm);
  for var x:=1 to wG-1 do begin
    pred       := arr[0,x-1]; //Sub
    arrTm[0,x] := round((arr[0,x]-pred)/PQuantizer)*PQuantizer;
    arr  [0,x] := pred + arrTm[0,x];
  end;
  for var y:=1 to hG-1 do begin
    pred       := arr[y-1,0]; //Up
    arrTm[y,0] := round((arr[y,0]-pred)/PQuantizer)*PQuantizer;
    arr  [y,0] := pred + arrTm[y,0];
  end;
  case PreFilter of
    PF_AVERAGE:
      for var y:=1 to hG-1 do
      for var x:=1 to wG-1 do begin
	    pred       := floor((arr[y-1,x]+arr[y,x-1])/2); //Average
        arrTm[y,x] := round((arr[y,x]-pred)/PQuantizer)*PQuantizer;
        arr  [y,x] := pred + arrTm[y,x];
      end;
    PF_PAETH:
      for var y:=1 to hG-1 do
      for var x:=1 to wG-1 do begin
        pred       := predPaeth(arr[y-1,x-1], arr[y-1,x], arr[y,x-1]); //Paeth
        arrTm[y,x] := round((arr[y,x]-pred)/PQuantizer)*PQuantizer;
        arr  [y,x] := pred + arrTm[y,x];
      end;
  end;
end;

procedure FromPrePCM;
var pred:ctype;
begin
  for var x:=1 to wG-1 do begin		    
    pred       := arrTm[0,x-1]; //Sub
    arrTm[0,x] := pred + arrTm[0,x];
  end;
  for var y:=1 to hG-1 do begin
    pred       := arrTm[y-1,0]; //Up
    arrTm[y,0] := pred + arrTm[y,0];
  end;
  case PreFilter of
    PF_AVERAGE:
      for var y:=1 to hG-1 do
      for var x:=1 to wG-1 do begin
        pred       := floor((arrTm[y-1,x] + arrTm[y,x-1])/2); //Average
        arrTm[y,x] := pred + arrTm[y,x];
    end;
    PF_PAETH:
      for var y:=1 to hG-1 do
      for var x:=1 to wG-1 do begin
        pred       := predPaeth(arrTm[y-1,x-1], arrTm[y-1,x], arrTm[y,x-1]); //Paeth
        arrTm[y,x] := pred + arrTm[y,x];
      end;
  end;
end;
	
procedure ToPostPCM;
var pred:ctype;
begin
  var arr:=copy(arrTm);
  for var zid:=0 to Layers-1 do begin
    h:=z[zid].rc.Y; h2:=z[zid].rc2.Y;
    w:=z[zid].rc.X; w2:=z[zid].rc2.X;
    for var yy:=0 to 1 do
    for var xx:=0 to 1 do if (xx+yy>0) or (zid=Layers-1) then begin
      for var x:=w2*xx+1 to w2*(xx+1)-1 do begin
        pred           := arr[h2*yy,x-1]; //Sub
        arrTm[h2*yy,x] := round((arr[h2*yy,x]-pred)/PQuantizer)*PQuantizer;
        arr  [h2*yy,x] := pred + arrTm[h2*yy,x];
      end;
      for var y:=h2*yy+1 to h2*(yy+1)-1 do begin
        pred           := arr[y-1,w2*xx]; //Up
        arrTm[y,w2*xx] := round((arr[y,w2*xx]-pred)/PQuantizer)*PQuantizer;
        arr  [y,w2*xx] := pred + arrTm[y,w2*xx];
      end;
      case PreFilter of
        PF_AVERAGE:
          for var y:=h2*yy+1 to h2*(yy+1)-1 do
          for var x:=w2*xx+1 to w2*(xx+1)-1 do begin
  	        pred       := floor((arr[y-1,x]+arr[y,x-1])/2); //Average
            arrTm[y,x] := round((arr[y,x]-pred)/PQuantizer)*PQuantizer;
            arr  [y,x] := pred + arrTm[y,x];
          end;
        PF_PAETH:
          for var y:=h2*yy+1 to h2*(yy+1)-1 do
          for var x:=w2*xx+1 to w2*(xx+1)-1 do begin
            pred       := predPaeth(arr[y-1,x-1], arr[y-1,x], arr[y,x-1]); //Paeth
            arrTm[y,x] := round((arr[y,x]-pred)/PQuantizer)*PQuantizer;
            arr  [y,x] := pred + arrTm[y,x];
          end;
      end;
	end;
  end;
end;

procedure FromPostPCM;
var pred:ctype;
begin
  for var zid:=0 to Layers-1 do begin
    h:=z[zid].rc.Y; h2:=z[zid].rc2.Y;
    w:=z[zid].rc.X; w2:=z[zid].rc2.X;
    for var yy:=0 to 1 do
    for var xx:=0 to 1 do if (xx+yy>0) or (zid=Layers-1) then begin
      for var x:=w2*xx+1 to w2*(xx+1)-1 do begin
        pred           := arrTm[h2*yy,x-1]; //Sub
        arrTm[h2*yy,x] := pred + arrTm[h2*yy,x];
      end;
      for var y:=h2*yy+1 to h2*(yy+1)-1 do begin
        pred           := arrTm[y-1,w2*xx]; //Up
        arrTm[y,w2*xx] := pred + arrTm[y,w2*xx];
      end;
      case PreFilter of
        PF_AVERAGE:
          for var y:=h2*yy+1 to h2*(yy+1)-1 do
          for var x:=w2*xx+1 to w2*(xx+1)-1 do begin
            pred       := floor((arrTm[y-1,x] + arrTm[y,x-1])/2); //Average
            arrTm[y,x] := pred + arrTm[y,x];
        end;
        PF_PAETH:
          for var y:=h2*yy+1 to h2*(yy+1)-1 do
          for var x:=w2*xx+1 to w2*(xx+1)-1 do begin
            pred       := predPaeth(arrTm[y-1,x-1], arrTm[y-1,x], arrTm[y,x-1]); //Paeth
            arrTm[y,x] := pred + arrTm[y,x];
          end;
      end;
   end;
  end;		
end;
