const
	DIRECT = true;
	INVERSE = false;

var
	hD, wD: integer;
	bmpSr, bmpIn,
	bmpOu, bmpPk: Picture;
	inp_str: string;

procedure applyPack;
	begin
		GrayPush(bmpIn);
		if (PreFilter<>PF_NONE) then ToPrePCM;
		for var k:=0 to Layers-1 do begin
			LoadZone(k);
			
			PixToCell4(SHIFT);
			TransformBlend(DIRECT);
			Cell4ToPix(SHIFT);
		
			PixToCell4(DEF);
			TransformHaar(DIRECT);
			Cell4ToPix(REMIX);
		
			SaveZone(k);
		end;
		Quantize;
		if (PostFilter<>PF_NONE) then ToPostPCM;
		GrayPop(bmpPk);
	end;

procedure applyUnpack;
	begin 
		//GrayPush(bmpPk);
		if (PostFilter<>PF_NONE) then FromPostPCM;
		UnQuantize;
		for var k:=Layers-1 downto 0 do begin
			LoadZone(k);
			
			PixToCell4(REMIX);
			TransformHaar(INVERSE);
			Cell4ToPix(DEF);
		
			PixToCell4(SHIFT);
			TransformBlend(INVERSE);
			Cell4ToPix(SHIFT);
		end;
		if (PreFilter<>PF_NONE) then FromPrePCM;
		GrayPop(bmpOu);
	end;
