var
	SearchMode,
	SearchOpts,
	SearchSteps: integer;
	blmp0,
	blmap: array[,] of byte;

///--non final variant--
///estimation of merging options 
procedure GetBlendMap;
	begin
		setlength(blmap,h2+1,w2+1);
		case SearchMode of
		0:	for var k:=0 to SearchSteps-1 do begin //minimize gradient, parallel
				blmp0:=copy(blmap);
				{$omp parallel for}
				for var y:=1 to h2-1 do
				for var x:=1 to w2-1 do begin
					var t0:=blmp0[y-1,x-1];
					var t1:=blmp0[y-1,x+1];
					var t2:=blmp0[y+1,x-1];
					var t3:=blmp0[y+1,x+1];
					var dd:array of real; setlength(dd,SearchOpts);
					for var i:=0 to SearchOpts-1 do begin
						dd[i]:=0;
						for var j:=0 to SearchOpts-1 do	dd[i] +=
							sqr(cell4tmp[y,x,i][0] - cell4tmp[y-1,x-1,j][3]) +
							sqr(cell4tmp[y,x,i][1] - cell4tmp[y-1,x+1,j][2]) +
							sqr(cell4tmp[y,x,i][2] - cell4tmp[y+1,x-1,j][1]) +
							sqr(cell4tmp[y,x,i][3] - cell4tmp[y+1,x+1,j][0]);
						if k>0 then dd[i] +=
							sqr(cell4tmp[y,x,i][0] - cell4tmp[y-1,x-1,t0][3])*k +
							sqr(cell4tmp[y,x,i][1] - cell4tmp[y-1,x+1,t1][2])*k +
							sqr(cell4tmp[y,x,i][2] - cell4tmp[y+1,x-1,t2][1])*k +
							sqr(cell4tmp[y,x,i][3] - cell4tmp[y+1,x+1,t3][0])*k;
					end;
					blmap[y,x]:=dd.IndexMin;
				end;
			end;
		1:	for var k:=0 to SearchSteps-1 do begin //maximize gradient, parallel
				blmp0:=copy(blmap);
				{$omp parallel for}
				for var y:=1 to h2-1 do
				for var x:=1 to w2-1 do begin
					var t0:=blmp0[y-1,x-1];
					var t1:=blmp0[y-1,x+1];
					var t2:=blmp0[y+1,x-1];
					var t3:=blmp0[y+1,x+1];
					var dd:array of real; setlength(dd,SearchOpts);
					for var i:=0 to SearchOpts-1 do begin
						for var j:=0 to SearchOpts-1 do	dd[i] +=
							sqr(cell4tmp[y,x,i][0] - cell4tmp[y-1,x-1,j][3]) +
							sqr(cell4tmp[y,x,i][1] - cell4tmp[y-1,x+1,j][2]) +
							sqr(cell4tmp[y,x,i][2] - cell4tmp[y+1,x-1,j][1]) +
							sqr(cell4tmp[y,x,i][3] - cell4tmp[y+1,x+1,j][0]);
						if k>0 then dd[i] +=
							sqr(cell4tmp[y,x,i][0] - cell4tmp[y-1,x-1,t0][3])*k +
							sqr(cell4tmp[y,x,i][1] - cell4tmp[y-1,x+1,t1][2])*k +
							sqr(cell4tmp[y,x,i][2] - cell4tmp[y+1,x-1,t2][1])*k +
							sqr(cell4tmp[y,x,i][3] - cell4tmp[y+1,x+1,t3][0])*k;
					end;
					blmap[y,x]:=dd.IndexMax;
				end;
			end;
		2:	for var k:=0 to SearchSteps-1 do
			for var p:=0 to (h2*w2-h2-w2) do begin //minimize gradient, genetic
				var y:=1+random(h2-1);
				var x:=1+random(w2-1);
				var t0:=blmap[y-1,x-1];
				var t1:=blmap[y-1,x+1];
				var t2:=blmap[y+1,x-1];
				var t3:=blmap[y+1,x+1];
				var dd:array of real; setlength(dd,SearchOpts);
				{$omp parallel for reduction(+:dd)}
				for var i:=0 to SearchOpts-1 do begin
					for var j:=0 to SearchOpts-1 do dd[i] +=
						sqr(cell4tmp[y,x,i][0] - cell4tmp[y-1,x-1,j][3]) +
						sqr(cell4tmp[y,x,i][1] - cell4tmp[y-1,x+1,j][2]) +
						sqr(cell4tmp[y,x,i][2] - cell4tmp[y+1,x-1,j][1]) +
						sqr(cell4tmp[y,x,i][3] - cell4tmp[y+1,x+1,j][0]);
					if k>0 then dd[i] +=
						sqr(cell4tmp[y,x,i][0] - cell4tmp[y-1,x-1,t0][3])*k +
						sqr(cell4tmp[y,x,i][1] - cell4tmp[y-1,x+1,t1][2])*k +
						sqr(cell4tmp[y,x,i][2] - cell4tmp[y+1,x-1,t2][1])*k +
						sqr(cell4tmp[y,x,i][3] - cell4tmp[y+1,x+1,t3][0])*k;
				end;
				blmap[y,x]:=dd.IndexMin;
			end;
		3:	for var k:=0 to SearchSteps-1 do
			for var p:=0 to (h2*w2-h2-w2) do begin //maximize gradient, genetic
				var y:=1+random(h2-1);
				var x:=1+random(w2-1);
				var t0:=blmap[y-1,x-1];
				var t1:=blmap[y-1,x+1];
				var t2:=blmap[y+1,x-1];
				var t3:=blmap[y+1,x+1];
				var dd:array of real; setlength(dd,SearchOpts);
				{$omp parallel for reduction(+:dd)}
				for var i:=0 to SearchOpts-1 do begin
					for var j:=0 to SearchOpts-1 do dd[i] +=
						sqr(cell4tmp[y,x,i][0] - cell4tmp[y-1,x-1,j][3]) +
						sqr(cell4tmp[y,x,i][1] - cell4tmp[y-1,x+1,j][2]) +
						sqr(cell4tmp[y,x,i][2] - cell4tmp[y+1,x-1,j][1]) +
						sqr(cell4tmp[y,x,i][3] - cell4tmp[y+1,x+1,j][0]);
					if k>0 then dd[i] +=
						sqr(cell4tmp[y,x,i][0] - cell4tmp[y-1,x-1,t0][3])*k +
						sqr(cell4tmp[y,x,i][1] - cell4tmp[y-1,x+1,t1][2])*k +
						sqr(cell4tmp[y,x,i][2] - cell4tmp[y+1,x-1,t2][1])*k +
						sqr(cell4tmp[y,x,i][3] - cell4tmp[y+1,x+1,t3][0])*k;
				end;
				blmap[y,x]:=dd.IndexMax;
			end;
		end;
	end;
