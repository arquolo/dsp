var
  fx:text;
  fname:string;
	
begin
  assign(fx,'settings.txt');
  reset(fx);
  readln(fx,Layers);     //frequency layers
  readln(fx,GQuantizer); //global quantizer
  readln(fx,PQuantizer); //prediction quantizer
  readln(fx,Psy);        //strength of noise-based quantizer compensation
  readln(fx,pow);        //1:power-based decay of noise in LF-coeffs
  readln(fx,std);        //1:normal noise distribution
  readln(fx,inp_str); par_prdc.TryParse(inp_str,PreFilter);
  readln(fx,inp_str); par_prdc.TryParse(inp_str,PostFilter);
  readln(fx,SearchMode); //estimation target: 0-Minimize grad, 1-Maximize grad
  readln(fx,SearchSteps);//steps of estimation of optimal transformation of cell
  close(fx);
  
  fname:=GetEXEFileName;
  fname:=copy(fname,0,length(fname)-4)+'.txt';
  assign(fx,fname);
  reset(fx);
  readln(fx,SearchOpts); //variants of optimal transformation
  setlength(keys,SearchOpts);
  for var key:=0 to SearchOpts-1 do begin
    readln(fx,inp_str);
    par_bltf.TryParse(inp_str,keys[key]);
  end;
  close(fx);
  
  var mp:byte:=1;
  for var k:=0 to Layers-1 do mp*=2;
  
  //readln(inp_str);
  inp_str:=paramStr(1);
  try        bmpSr := new Picture(inp_str+'.png');
  except try bmpSr := new Picture(inp_str+'.jpg');
  except try bmpSr := new Picture(inp_str+'.bmp');
  except     bmpSr := new Picture(inp_str);
  end;end;end;
  hG := mp*(bmpSr.Height div mp);
  wG := mp*(bmpSr.Width  div mp);
  bmpIn := new Picture(wG,hG);
  bmpPk := new Picture(wG,hG);
  bmpOu := new Picture(wG,hG);
  var c:byte;
  for var y:=0 to hG-1 do
  for var x:=0 to wG-1 do begin
    c:=round(255*bmpSr.GetPixel(x,y).GetBrightness);
    bmpIn.SetPixel(x,y,RGB(c,c,c));
  end;
  
  setlength(z,Layers);
  for var k:=Layers-1 downto 0 do begin
    mp:=mp div 2;
    z[k].rc  := new Point(wG div mp,    hG div mp   );
    z[k].rc2 := new Point(wG div(mp*2), hG div(mp*2));
    setlength(z[k].map,0,0);
  end;
  
  hD:=min(hG,ScreenHeight-160);
  wD:=min(wG,ScreenWidth -80);
  if hD*wG>wD*hG then hD:=round(wD*hG/wG) else wD:=round(hD*wG/hG);
  setwindowsize(wD,hD);
  centerwindow;
  bmpIn.Draw(0,0,wD,hD);
  
  applyPack;
  bmpPk.Draw(0,0,wD,hD);
  GetStatsSize; window.caption:=debug_str;
  
  applyUnpack;
  readln; bmpOu.Draw(0,0,wD,hD);
  
  GetStatsDiff; window.caption:=debug_str;
  while true do begin
    readln; bmpIn.Draw(0,0,wD,hD);
    readln; bmpOu.Draw(0,0,wD,hD);
  end;
end.
