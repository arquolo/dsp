﻿import qbs
import qbs.Probes

Product {
    Depends {
        name: "cpp"
    }

    // cpp.compilerName: "clang"
    // cpp.cxxCompilerName: "clang++"

    buildDirectory: "build"
    type: ["application"]
    files: [
        "include/**/*.h",
        "src/**/*.cpp",
        "tests/main.cpp"
    ]
    cpp.includePaths: "./include"
    cpp.systemIncludePaths: { return base.concat("/usr/include/c++/7") }

    Probes.PkgConfigProbe {
        id: pkgConfig
        name: "opencv"
    }
    cpp.linkerFlags: pkgConfig.libs
    cpp.dynamicLibraries: [ "pthread" ]
    cpp.cxxLanguageVersion: "c++17"

    cpp.debugInformation: false
    cpp.enableExceptions: false
    cpp.enableRtti: false

    // cpp.discardUnusedData: true // qbs>=1.10
    Properties {
        condition: cpp.compilerName == "gcc"
        cpp.cxxFlags: {
            return base.concat([
                "-W", "-Wall", "-Wextra",
                "-Waggressive-loop-optimizations",
                "-Wcast-align", "-Wcast-qual",
                "-Wdouble-promotion", "-Wduplicated-branches", "-Wduplicated-cond",
                "-Wfloat-equal", "-Wformat=2", "-Wformat-signedness", "-Wframe-larger-than=32768",
                "-Wlogical-op",
                "-Wnull-dereference",
                "-Wodr", "-Wold-style-cast",
                "-Wshadow=local", "-Wshift-overflow=2", "-Wstrict-aliasing=2", "-Wsuggest-final-methods", "-Wsuggest-final-types", "-Wsync-nand",
                "-Wtrampolines",
                "-Wuseless-cast",
                "-Wwrite-strings"
            ])
        }
    }
    Properties {
        condition: cpp.compilerName == "clang"
        cpp.cxxFlags: {
            return base.concat([
                "-Weverything",
                "-Wno-class-varargs", "-Wno-padded", "-Wno-switch-enum", "-Wno-unused-macros",
                "-Wc++17-compat"
                // "-Wno-c++98-compat", "-Wno-c++98-compat-pedantic",
                // "-Wno-c++14-extensions", "-Wno-c++17-extensions"
            ])
        }
    }

    Properties {
        condition: qbs.buildVariant == "debug"
        cpp.cxxFlags: {
            return base.concat(["-g", "-Og"])
        }
    }
    Properties {
        condition: qbs.buildVariant == "release"
        cpp.cxxFlags: {
            return base.concat(["-O3", "-ffast-math", "-funroll-loops"])
        }
    }

    Group {
        fileTagsFilter: "application"
        qbs.install: false
    }
}
