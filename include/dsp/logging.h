﻿#pragma once

#include <chrono>
#include <cstddef>

/*-------------------------------------------------------*/
// Presets
#define dspLogInfo 0
#define dspLogDebug 1
#define dspLogTrace 2

/*-------------------------------------------------------*/
// Configuration
#ifndef dspLog
#define dspLog dspLogDebug
#endif
/*-------------------------------------------------------*/

#define echo(...) \
    { \
        char str[100]; \
        sprintf(str, __VA_ARGS__); \
        printf("\33[2K%s\n", str); \
        fflush(stdout); \
    }

#define echo_status(...) \
    { \
        char str[100]; \
        sprintf(str, __VA_ARGS__); \
        printf("\33[2K%s\r", str); \
        fflush(stdout); \
    }

#ifndef NDEBUG
#define echod(...) echo_status(__VA_ARGS__)
#else
#define echod(...)
#endif

#if (dspLog >= dspLogInfo)
#define echo_info(...) echod(__VA_ARGS__)
#else
#define echo_info(...)
#endif

#if (dspLog >= dspLogDebug)
#define echo_debug(...) echod(__VA_ARGS__)
#else
#define echo_debug(...)
#endif

#if (dspLog >= dspLogTrace)
#define echo_trace(...) echod(__VA_ARGS__)
#else
#define echo_trace(...)
#endif

/*-------------------------------------------------------*/

#define dsp_speed_loop(states, name) \
    { \
        using namespace std::chrono; \
        auto t0_ = high_resolution_clock::now(); \
        auto msg_ = (name); \
        size_t states_ = (states); \
        for (size_t i_ = 0; i_ < states_; ++i_) {

// clang-format off
#define dsp_speed_end() \
        } \
        auto t1_ = high_resolution_clock::now(); \
        auto d_ = 1000 * duration<double>(t1_ - t0_).count(); \
        echo("%.3f ms : %s", d_ / states_, msg_); \
    }
// clang-format on
