﻿#pragma once

#include <cmath>
#include <cstddef>
#include <limits>
#include <type_traits>

namespace math {

template <typename T>
inline constexpr T lerp(T x0, T x1, T t) noexcept {
    return x0 + (x1 - x0) * t;
}

/*-------------------------------------------------------*/
// accuracy

template <typename T>
constexpr T quant = 0;

template <>
constexpr float quant<float> = 1e-6f;

template <>
constexpr double quant<double> = 1e-15;

/*-------------------------------------------------------*/
// normalization

template <typename _ValueType>
static constexpr _ValueType norm
    = !std::is_floating_point_v<std::decay_t<_ValueType>>
    ? std::numeric_limits<_ValueType>::max()
    : 1;

/*-------------------------------------------------------*/
// scaled values

template <typename T>
constexpr T pi = static_cast<T>(M_PIl);

template <typename T>
constexpr T pi2 = static_cast<T>(M_PI_2l);

/*-------------------------------------------------------*/

template <typename T>
constexpr bool is_plus(T x) noexcept {
    return x > +quant<T>;
}

template <typename T>
constexpr bool is_minus(T x) noexcept {
    return x < -quant<T>;
}

template <typename T>
constexpr bool is_null(T x) noexcept {
    return !is_plus(x) && !is_minus(x);
}

template <typename T>
constexpr int sign(T x) noexcept {
    return is_plus(x) - is_minus(x);
}

/*-------------------------------------------------------*/

// fit<uint16_t>(T x)
// fit<uint8_t>(T x)
// fit<float>(T x)
template <typename Tdst, typename T>
constexpr Tdst fit(T x) noexcept {
    constexpr auto max = static_cast<T>(norm<Tdst>);
    if (x >= max)
        return max;
    if (x >= 0)
        return x;
    return 0;
}

// fit<uint16_t>(T x)
// fit<uint8_t>(T x)
// fit<float>(T x)
template <typename Tdst, typename T>
constexpr Tdst fit_half(T x) noexcept {
    constexpr auto shift = static_cast<T>(0.5 * norm<T>);
    return fit<Tdst, T>(0.5 * x + shift);
}

/*-------------------------------------------------------*/

size_t rand() noexcept;
double rand_p() noexcept;
double rand_uniform(double disp) noexcept;
double rand_normal(double disp) noexcept;

size_t floor(size_t, size_t) noexcept;
size_t ceil(size_t, size_t) noexcept;

} // namespace math
