﻿#pragma once

#include "dsp/engine/tensor.h"

#ifndef __clang__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#endif

#include <opencv2/core/types.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#ifndef __clang__
#pragma GCC diagnostic pop
#endif

namespace dsp {

template <typename T, size_t N>
void operator<<(tensor<T, N>& t, cv::Mat const& m) noexcept {
    if constexpr (N == 2)
        t.create(static_cast<size_t>(m.rows), static_cast<size_t>(m.cols));
    else
        t.create(static_cast<size_t>(m.rows), static_cast<size_t>(m.cols), 3);
    size_t step1 = m.step1(1);
    size_t step2 = t.step[1];

    for (size_t i = 0; i < t.size[0]; ++i) {
        auto row2 = &t(i);
        switch (m.type() & (CV_DEPTH_MAX - 1)) {
        case CV_32F: {
            float scale = math::norm<T> / math::norm<float>;
            auto row1 = m.ptr<float const>(static_cast<int>(i));
            for (size_t j = 0; j < t.size[1]; ++j) {
                row2[0 % step2] = static_cast<T>(row1[2 % step1] * scale);
                row2[1 % step2] = static_cast<T>(row1[1 % step1] * scale);
                row2[2 % step2] = static_cast<T>(row1[0 % step1] * scale);
                row1 += step1;
                row2 += step2;
            }
        } break;
        case CV_16U: {
            float scale = math::norm<T> / math::norm<uint16_t>;
            auto row1 = m.ptr<uint16_t const>(static_cast<int>(i));
            for (size_t j = 0; j < t.size[1]; ++j) {
                row2[0 % step2] = static_cast<T>(row1[2 % step1] * scale);
                row2[1 % step2] = static_cast<T>(row1[1 % step1] * scale);
                row2[2 % step2] = static_cast<T>(row1[0 % step1] * scale);
                row1 += step1;
                row2 += step2;
            }
        } break;
        default: {
            float scale = math::norm<T> / math::norm<uint8_t>;
            auto row1 = m.ptr<uint8_t const>(static_cast<int>(i));
            for (size_t j = 0; j < t.size[1]; ++j) {
                row2[0 % step2] = static_cast<T>(row1[2 % step1] * scale);
                row2[1 % step2] = static_cast<T>(row1[1 % step1] * scale);
                row2[2 % step2] = static_cast<T>(row1[0 % step1] * scale);
                row1 += step1;
                row2 += step2;
            }
        } break;
        }
    }
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
void operator>>(tensor<T, N> const& t, cv::Mat& m) noexcept {
    int id = cv::DataDepth<T>::value + ((N == 2) ? 0 : 16);
    if (t.empty())
        return;

    m.create(static_cast<int>(t.size[0]), static_cast<int>(t.size[1]), id);
    size_t step1 = t.step[1];
    size_t step2 = m.step1(1);
    for (size_t i = 0; i < t.size[0]; ++i) {
        auto row1 = &t(i);
        auto row2 = m.ptr<T>(static_cast<int>(i));
        for (size_t j = 0; j < t.size[1]; ++j) {
            row2[2 % step2] = row1[0 % step1];
            row2[1 % step2] = row1[1 % step1];
            row2[0 % step2] = row1[2 % step1];
            row2 += step2;
            row1 += step1;
        }
    }
}

template <typename T>
void operator>>(tensor<T, 1> const& t, cv::Mat& m) noexcept {
    size_t n = t.size[0];
    m.create(static_cast<int>(n), static_cast<int>(n), CV_8U);
    m.setTo(255);

    T const* p = &t(0);
    auto [min_it, max_it] = std::minmax_element(p, p + n);
    auto min = *min_it;
    auto max = *max_it;

    double k0 = static_cast<double>(n) / static_cast<double>(max - min);
    double k1 = static_cast<double>(max) * k0;

    for (size_t i = 0; i < n; ++i) {
        auto xx = static_cast<int>(i);
        auto y1 = static_cast<int>(k1 - k0 * static_cast<double>(p[i]));
        auto y0 = static_cast<int>(n);
        cv::line(m, cv::Point(xx, y1), cv::Point(xx, y0), 0);
    }
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
bool operator<<(tensor<T, N>& t, std::string const& path) noexcept {
    auto flags = cv::IMREAD_ANYDEPTH
        + (N == 2 ? cv::IMREAD_GRAYSCALE : cv::IMREAD_COLOR);
    cv::Mat m = cv::imread(path, flags);
    if (m.empty())
        return false;

    t << m;
    return true;
}

template <typename T>
bool operator<<(tensor<T, 1>& t, std::string const& path) noexcept {
    cv::Mat m = cv::imread(path, 0);
    if (m.empty())
        return false;

    t << m;
    return true;
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
void operator>>(tensor<T, N> const& t, std::string const& path) noexcept {
    cv::Mat m;
    t >> m;
    cv::imwrite(path, m);
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
#ifdef QUIET
void show(tensor<T, N> const&, int = 0) noexcept {
}
#else
void show(tensor<T, N> const& t, int delay = 0) noexcept {
    cv::Mat mat;
    t >> mat;

    cv::imshow("Image", mat);
    cv::waitKey(delay);
}
#endif

} // namespace dsp
