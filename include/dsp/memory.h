﻿#pragma once

#include <list>
#include <memory>

class bank {
    friend class cache;

    char* _M_data{nullptr};
    size_t _M_size{};

public:
    bank() = default;
    bank(size_t);
    bank(bank const&) = delete;
    bank(bank const&&) = delete;
    ~bank();

    void operator=(bank const&) = delete;
    void operator=(bank const&&) = delete;

    bool empty() const noexcept;
    size_t size() const noexcept;

    template <typename _Tp>
    _Tp* as() {
        return static_cast<_Tp*>(static_cast<void*>(_M_data));
    }
};

using resource = std::shared_ptr<bank>;
class cache {
    friend class bank;

    static constexpr size_t _S_min_size = 0x0;
    static constexpr size_t _S_max_size = 0x1000000; //!< 16 MiB

    std::list<resource> _M_resources{}; //!< List of cached blocks

    size_t _M_size{};    //!< Total size of all allocated banks in Bytes
    size_t _M_objects{}; //!< Number of banks

    cache() = default;
    ~cache() = default;

    void _M_collect() noexcept;
    static cache& _S_instance() noexcept;

public:
    cache(cache const&) = delete;
    void operator=(cache const&) = delete;

    static resource allocate(size_t) noexcept;
};
