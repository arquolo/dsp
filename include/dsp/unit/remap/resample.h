﻿#pragma once

#include <functional>
#include <numeric>

#include "dsp/io.h"
#include "dsp/math_spline.h"

namespace dsp {

enum class resample_t {
    nearest,
    linear,
    cubic,
    generic,
};

namespace remap {

// clang-format off
template <typename T = float>
tensor<T, 2> spline_precomp(
    const std::function<double(double)>& gen,
    size_t steps,
    size_t radius
) noexcept {
    tensor<T, 2> tmp{steps + 1, radius * 2};
    auto rsteps = 1 / static_cast<double>(steps);
    auto shift0 = static_cast<double>(radius) - 1;

    for (size_t i = 0; i <= steps; ++i) {
        auto shift1 = shift0 + rsteps * static_cast<double>(i);

        auto p = &tmp(i);
        for (size_t j = 0; j < radius * 2; ++j) {
            auto x = std::abs(shift1 - static_cast<double>(j));
            p[j] = static_cast<T>(gen(x));
        }
    }
    return tmp;
}

template <typename T, size_t N>
void resample(
    tensor<T, N>& m,
    std::initializer_list<size_t> dims,
    resample_t e,
    size_t radius = 1,
    size_t smoothness = 0
) noexcept {
    auto [f, r] = [=]() -> std::pair<std::function<double(double)>, size_t> {
        switch (e) {
        case resample_t::generic:
            return std::make_pair(
                [ radius, smoothness ](double x) noexcept {
                    return math::splc_pcos(x, radius, smoothness);
                },
                radius);
        case resample_t::cubic:
            return std::make_pair(math::spl_cubic, size_t{2});
        case resample_t::linear:
            return std::make_pair(math::spl_pyramid, size_t{1});
        case resample_t::nearest:
            return std::make_pair(math::spl_box, size_t{1});
        }
        return std::pair<std::function<double(double)>, size_t>{};
    }();

    size_t size[N];
    if (dims.size() == N)
        std::copy_n(dims.begin(), N, size);
    else
        std::fill_n(size, N, dims.begin()[0]);

    size_t steps[N];
    std::transform(m.size, m.size + N, size, steps, [](auto x, auto y) {
        return std::gcd(x, y);
    });

    std::vector<tensor<double, 2>> spline_maps;
    spline_maps.reserve(N);
    std::transform(
        steps, steps + N, std::back_inserter(spline_maps),
        [&f, r](auto x) { return spline_precomp<double>(f, x, r); });

    for (auto const& it : spline_maps)
        show(it);
    /*
    Frame<T, N> tmp(size);
    for (size_t i = 0; i < N; ++i) {
        !!!borderize!!!
        (m >> tmp) += FSampler(spline_precomp[i], m.step[i], tmp.step[i]);
        m = tmp;
    }
    */
}
// clang-format on

} // namespace remap
} // namespace dsp
