﻿#pragma once

#include "dsp/engine/tensor.h"

namespace dsp {
namespace remap {

template <typename T>
inline constexpr void
transpose_block(T const* m0, T* m1, size_t step0, size_t step1) noexcept {
    for (size_t i = 0; i < block_size<T>; ++i)
        for (size_t j = 0; j < block_size<T>; ++j)
            m1[j * step1 + i] = m0[i * step0 + j];
}

template <typename T>
constexpr void transpose(tensor<T, 2>& m) noexcept {
    size_t sz[2]{m.size[1], m.size[0]};

    tensor<T, 2> tmp{sz};
    size_t size0 = m.size[0];
    size_t size1 = m.size[1];
    size_t step0 = m.step[0];
    size_t step1 = tmp.step[0];

    for (size_t i0 = 0; i0 < size0; i0 += block_size<T>)
        for (size_t i1 = 0; i1 < size1; i1 += block_size<T>) {
            auto p0 = m(i0)(i1);
            auto p1 = tmp(i1)(i0);
            transpose_block(&p0, &p1, step0, step1);
        }
    m = tmp;
}

} // namespace remap
} // namespace dsp
