﻿#pragma once

#include "dsp/engine/tensor.h"

namespace dsp {
namespace remap {

// clang-format off
template <typename T>
constexpr tensor<T, 2> concatenate(
    tensor<T, 2> const& m0, tensor<T, 2> const& m1, size_t axis
) noexcept {
    assert(axis <= 1);
    assert(m0.size[1 - axis] == m1.size[1 - axis]);

    size_t size[2]{m0.size[0], m0.size[1]};
    size[axis] += m1.size[axis];

    size_t edge[2]{0, 0};
    edge[axis] += m0.size[axis];

    tensor<T, 2> tmp{size[0], size[1]};

    m0 >> tmp({0, 0, m0.size[0], m1.size[1]});
    m1 >> tmp({edge[0], edge[1], m0.size[0], m1.size[1]});
    return tmp();
}

template <typename T>
constexpr tensor<T, 2> concatenate(
    tensor<T, 2> const& m00,
    tensor<T, 2> const& m01,
    tensor<T, 2> const& m10,
    tensor<T, 2> const& m11
) noexcept {
    assert((
        m00.size[0] == m01.size[0] && m10.size[0] == m11.size[0] &&
        m00.size[1]  + m01.size[1] == m10.size[1] +  m11.size[1]
    ) || (
        m00.size[0]  + m10.size[0] == m01.size[0] +  m11.size[0] &&
        m00.size[1] == m10.size[1] && m01.size[1] == m11.size[1]
    ));

    tensor<T, 2> tmp{m00.size[0] + m10.size[0], m00.size[1] + m01.size[1]};

    m00 >> tmp({0, 0, m00.size[0], m00.size[1]});
    m01 >> tmp({0, m00.size[1], m01.size[0], m01.size[1]});
    m10 >> tmp({m00.size[0], 0, m10.size[0], m10.size[1]});
    m11 >> tmp({m01.size[0], m10.size[1], m11.size[0], m11.size[1]});
    return tmp();
}
// clang-format on

} // namespace shape
} // namespace dsp
