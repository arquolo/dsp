﻿#pragma once

#include "dsp/engine/tensor.h"

namespace dsp {
namespace blas {

template <typename T>
constexpr double
ssim_block(T const* p0, T const* p1, size_t step0, size_t step1) noexcept {
    double mx(0), my(0);
    double dxx(0), dxy(0), dyy(0);
    for (size_t i = 0; i < block_size<T>; ++i) {
        float Rmx(0), Rmy(0);
        float Rdxx(0), Rdxy(0), Rdyy(0);

        T const* p0t = p0 + i * step0;
        T const* p1t = p1 + i * step1;
        for (size_t j = 0; j < block_size<T>; ++j) {
            float kj = 1.0f / (1 + j);

            float dx = p0t[j] - Rmx;
            float dy = p1t[j] - Rmy;
            // average values
            Rmx += dx * kj;
            Rmy += dy * kj;
            // standard deviations
            Rdxx += ((1 - kj) * dx * dx - Rdxx) * kj;
            Rdxy += ((1 - kj) * dx * dy - Rdxy) * kj;
            Rdyy += ((1 - kj) * dy * dy - Rdyy) * kj;
        }
        double ki = 1.0 / (1 + i);

        // average values
        mx += (static_cast<double>(Rmx) - mx) * ki;
        my += (static_cast<double>(Rmy) - my) * ki;
        // standard deviations
        dxx += (static_cast<double>(Rdxx) - dxx) * ki;
        dxy += (static_cast<double>(Rdxy) - dxy) * ki;
        dyy += (static_cast<double>(Rdyy) - dyy) * ki;
    }
    constexpr auto r2 = static_cast<double>(math::norm<T> * math::norm<T>);
    constexpr double k1 = 1e-4 * r2;
    constexpr double k2 = 9e-4 * r2;

    // clang-format off
    double r0 = (2*mx*my + k1) / (mx*mx + my*my + k1);
    double r1 = (2*dxy + k2) / (dxx + dyy + k2);
    // clang-format on

    return r0 * r1;
}

template <typename T>
constexpr double ssim(tensor<T, 2> const& m0, tensor<T, 2> const& m1) noexcept {
    assert(m0.size[0] == m1.size[0] && m0.size[1] == m1.size[1]);
    size_t const size[2]{
        math::floor(m0.size[0], block_size<T>),
        m0.size[1],
    };
    double res(0);
    for (size_t i = 0; i < size[0]; i += block_size<T>) {
        double Rres(0);
        auto row0 = m0(i);
        auto row1 = m1(i);
        for (size_t j = 0; j < size[1]; j += block_size<T>) {
            auto tmp = ssim_block(&row0(j), &row1(j), m0.step[0], m1.step[0]);
            Rres += (tmp - Rres) / (1 + j / block_size<T>);
        }
        res += (Rres - res) / (1 + i / block_size<T>);
    }
    constexpr double base = 1.0e-10;
    return (-10) * std::log10((1.0 - res) + base);
}

template <typename T>
constexpr double mse(tensor<T, 1> const& m0, tensor<T, 1> const& m1) noexcept {
    assert(m0.size[0] == m1.size[0]);
    size_t const size[1]{m0.size[0]};

    double res0 = 0;
    auto p0 = m0(0);
    auto p1 = m1(0);
    for (size_t i = 0; i < size[0]; ++i, ++p0, ++p1) {
        double t = *p0 - *p1;
        res0 += (t * t - res0) / (1 + i);
    }
    return res0;
}

template <typename T>
constexpr double mse(tensor<T, 2> const& m0, tensor<T, 2> const& m1) noexcept {
    // clang-format off
    assert(
        m0.size[0] == m1.size[0] &&
        m0.size[1] == m1.size[1]
    );
    // clang-format on
    size_t const size[2]{m0.size[0], m0.size[1]};

    double res0 = 0;
    auto p0 = m0(0);
    auto p1 = m1(0);
    for (size_t i = 0; i < size[0]; ++i, ++p0, ++p1) {
        double res1 = 0;
        auto pp0 = p0(0);
        auto pp1 = p1(0);
        for (size_t j = 0; j < size[1]; ++j, ++pp0, ++pp1) {
            double t = *pp0 - *pp1;
            res1 += (t * t - res1) / (1 + j);
        }
        res0 += (res1 - res0) / (1 + i);
    }
    return res0;
}

template <typename T>
constexpr double mse(tensor<T, 3> const& m0, tensor<T, 3> const& m1) noexcept {
    // clang-format off
    assert(
        m0.size[0] == m1.size[0] &&
        m0.size[1] == m1.size[1] &&
        m0.size[2] == m1.size[2]
    );
    // clang-format on
    size_t const size[3]{m0.size[0], m0.size[1], m0.size[2]};

    double res0 = 0;
    auto p0 = m0(0);
    auto p1 = m1(0);
    for (size_t i = 0; i < size[0]; ++i, ++p0, ++p1) {
        double res1 = 0;
        auto pp0 = p0(0);
        auto pp1 = p1(0);
        for (size_t j = 0; j < size[1]; ++j, ++pp0, ++pp1) {
            double res2 = 0;
            auto ppp0 = pp0(0);
            auto ppp1 = pp1(0);
            for (size_t k = 0; k < size[2]; ++k, ++ppp0, ++ppp1) {
                double t = *ppp0 - *ppp1;
                res2 += (t * t - res2) / (1 + k);
            }
            res1 += (res2 - res1) / (1 + j);
        }
        res0 += (res1 - res0) / (1 + i);
    }
    return res0;
}

template <typename T, size_t N>
constexpr double psnr(tensor<T, N> const& m0, tensor<T, N> const& m1) noexcept {
    constexpr double base = 1.0e-10;
    constexpr double scale = std::pow(math::norm<T>, -2);
    return (-10) * std::log10(scale * mse(m0, m1) + base);
}

} // namespace blas
} // namespace dsp
