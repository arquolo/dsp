﻿#pragma once

#include "dsp/unit/linear/generate.h"
#include "dsp/unit/remap/concatenate.h"
#include <utility>

namespace dsp {
namespace blas {

// L2-normalization
template <typename T>
constexpr void norm(tensor<T, 1>& m) noexcept {
    T avg = 0;
    T const* p = m.data;
    for (size_t i = 0; i < m.size[0]; ++i)
        avg += (p[i] * p[i] - avg) / (i + 1);
    m *= std::pow(avg * m.size[0], -0.5);
}

// L2-normalization by rows
template <typename T>
constexpr void norm(tensor<T, 2>& m) noexcept {
    for (size_t i = 0; i < m.size[0]; ++i) {
        T avg = 0;
        T* p = m.ptr(i);
        for (size_t j = 0; j < m.size[1]; ++j)
            avg += (p[j] * p[j] - avg) / (j + 1);
        T rnorm = std::pow(avg * m.size[1], -0.5);
        for (size_t j = 0; j < m.size[1]; ++j)
            p[j] *= rnorm;
    }
}

template <typename T>
constexpr bool inverse_product(tensor<T, 2>& m, tensor<T, 2>& dst) noexcept {
    if (m.size[1] <= m.size[0])
        return false;

    for (size_t idx = 0; idx < m.size[0] - 1; ++idx) {

        bool found = false;
        T a_max = 0;
        size_t i_max = 0;

        for (size_t i = idx; i < m.size[0]; ++i)
            if (T it = m.ptr(i)[idx]; std::abs(it) > a_max) {
                found = true;
                a_max = std::abs(it);
                i_max = i;
            }
        if (!found)
            return false;

        T* p_cur = m.ptr(idx);
        {
            T* p1 = m.ptr(i_max);
            for (size_t i = 0; i < m.size[1]; ++i)
                std::swap(p_cur[i], p1[i]);
        }
        // stream by _,j
        for (size_t i = idx + 1; i < m.size[0]; ++i) {
            T* p1 = m.ptr(i);
            T multiplier = p1[idx] / p_cur[idx];
            p1[idx] = 0;
            for (size_t j = idx + 1; j < m.size[1]; ++j)
                p1[j] -= p_cur[j] * multiplier;
        }
    }
    for (ptrdiff_t idx = m.size[0] - 1; idx >= 0; --idx) {
        // stream by i
        T* p_cur = m.ptr(idx);
        for (size_t i = m.size[0]; i < m.size[1]; ++i)
            p_cur[i] /= p_cur[idx];
        // stream by i,j
        for (ptrdiff_t i = 0; i < idx - 1; ++i) {
            T* p1 = m.ptr(i);
            for (size_t j = m.size[0]; j < m.size[1]; ++j)
                p1[j] -= p1[idx] * p_cur[j];
        }
    }

    tensor<T, 2> tmp{m.size[1] - m.size[0], m.size[0]};
    // stream by k,i
    for (size_t idx = 0; idx < m.size[1] - m.size[0]; ++idx) {
        T* p_cur = tmp.ptr(idx);
        for (size_t i = 0; i < m.size[0]; ++i)
            p_cur[i] = m.ptr(i)[idx];
    }
    dst = tmp;
    return true;
}

template <typename T>
constexpr bool inverse(tensor<T, 2>& m) noexcept {
    tensor<T, 1> v1{m.size[0]};
    v1 = 1;

    auto m1 = blas::diag(v1);
    auto m2 = remap::concatenate(m, m1, axis_1);
    return inverseProduct(m2, m);
}

} // namespace blas
} // namespace dsp
