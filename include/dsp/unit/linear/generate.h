﻿#pragma once

#include "dsp/engine/tensor.h"

namespace dsp {
namespace blas {

template <typename T = float>
constexpr tensor<T, 2> eye(size_t dim) noexcept {
    tensor<T, 2> t{dim, dim};
    t = 0;

    size_t edge = std::inner_product(t.step, t.step + 2, t.size, 0);
    size_t step = std::accumulate(t.step, t.step + 2, 0);

    T* begin = t.data();
    T* end = t.data() + edge;
    for (T* p = begin; p != end; p += step)
        *p = 1;
    return t;
}

template <typename T>
constexpr tensor<T, 2> diag(tensor<T, 1>& d) noexcept {
    size_t dim = d.size[0];
    tensor<T, 2> t{dim, dim};
    t = 0;

    size_t edge = std::inner_product(t.step, t.step + 2, t.size, 0);
    size_t step = std::accumulate(t.step, t.step + 2, 0);

    T const* p_src = d.data();

    T* begin = t.data();
    T* end = t.data() + edge;
    for (T* p = begin; p != end; p += step, ++p_src)
        *p = *p_src;
    return t;
}

template <typename T, size_t Ni, size_t Nj>
constexpr tensor<T, 2> rect(T const (&x)[Ni][Nj]) noexcept {
    tensor<T, 2> t{Ni, Nj};
    for (size_t i = 0; i < Ni; ++i) {
        auto row = t(i);
        for (size_t j = 0; j < Nj; ++j)
            *row(j) = x[i][j];
    }
    return t;
}

/*-------------------------------------------------------*/

tensor<float, 2> haar() noexcept;
tensor<float, 2> walsh(size_t) noexcept;
tensor<float, 2> dct(size_t) noexcept; // L1-normalized

} // namespace blas
} // namespace dsp
