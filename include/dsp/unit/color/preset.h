﻿#pragma once

namespace dsp {
namespace color {

class conv_mat {
public:
    constexpr static float ycgco[3][3]{
        {.25f, .5f, .25f},
        {-.25f, .5f, -.25f},
        {.50f, 0.f, -.50f},
    };

    constexpr static float cielab[3][3]{
        {0.f, 1.16f, 0.f},
        {5.f, -5.00f, 0.f},
        {0.f, 2.00f, -2.f},
    };

    constexpr static float lms_ab[3][3]{
        {.37613f, .70431f, -.05675f},
        {-.21649f, 1.14744f, .05356f},
        {.02567f, .16713f, .74235f},
    };

    constexpr static float lms_tp[3][3]{
        {.3592f, .6976f, -.0358f},
        {-.1922f, 1.1004f, .0755f},
        {.0070f, .0749f, .8434f},
    };

    constexpr static float icacb[3][3]{
        {.4949f, .5037f, .0015f},
        {4.2854f, -4.5462f, .2609f},
        {.3605f, 1.1499f, -1.5105f},
    };

    constexpr static float ictcp[3][3]{
        {.5000f, .5000f, .0000f},
        {1.6137f, -3.3234f, 1.7097f},
        {4.3781f, -4.2455f, -.1325f},
    };
};

} // namespace color
} // namespace dsp
