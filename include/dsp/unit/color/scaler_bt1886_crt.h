﻿#pragma once

#include "dsp/unit/color/type.h"
#include <cmath>

namespace dsp {
namespace color {

// [0,1] <-> [luma_min,luma_max] / 10K
template <color_pass_t Pass>
auto bt1886_crt(float luma_min = 0.1f, float luma_max = 100.f) noexcept {
    // hdr_coverage = std::fmax(luma_max, 100.f) / 10000.f;
    // sdr_coverage = 10000.f / std::fmax(luma_max, 100.f);

    float contrast = luma_min / luma_max;
    float arg_min = 0;
    float arg_max = 1;

    auto zero_ = [&] {
        for (int i = 0; i < 20; ++i) {
            auto arg = (arg_min + arg_max) / 2.0f;
            auto prediction = std::pow(arg / (arg + 1.00f), 2.6f)
                * std::pow(arg / (arg + 0.35f), 0.4f);
            if (prediction < contrast)
                arg_min = arg;
            else if (prediction > contrast)
                arg_max = arg;
            else
                return arg;
        }
        return (arg_min + arg_max) / 2.0f;
    }();

    float scale2_ = std::pow(zero_ + 1.00f, -2.6f) * luma_max / 10000.f;
    float scale1_ = std::pow(zero_ + 0.35f, -0.4f) * scale2_;

    if constexpr (Pass == color_pass_t::linear)
        return [=](float x) noexcept->float {
            if (x >= 0.35f)
                return std::pow(zero_ + x, 2.6f) * scale2_;
            if (x >= 0)
                return std::pow(zero_ + x, 3.0f) * scale1_;
            return 0;
        };
    else {
        float barrier2_ = std::pow(zero_ + 0.35f, 2.6f) * scale2_;
        float barrier1_ = luma_min / 10000.f;
        return [=](float x) noexcept->float {
            if (x >= barrier2_)
                return std::pow(x / scale2_, 1 / 2.6f) - zero_;
            if (x >= barrier1_)
                return std::pow(x / scale1_, 1 / 3.0f) - zero_;
            return 0;
        };
    }
}

} // namespace color
} // namespace dsp
