﻿#pragma once

namespace dsp {
enum class color_pass_t { linear, gamma };
}
