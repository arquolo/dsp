﻿#pragma once
/* any function here keeps range of [0,1] */

namespace dsp {
namespace color {
/*-------------------------------------------------------*/
namespace w3c {

float linear(float) noexcept;
float gamma(float) noexcept;

} // namespace w3c
/*-------------------------------------------------------*/
namespace cie_lab {

float linear(float) noexcept;
float gamma(float) noexcept;

} // namespace cie_lab
/*-------------------------------------------------------*/
namespace cie_luv {

float linear(float) noexcept;
float gamma(float) noexcept;

} // namespace cie_luv
/*-------------------------------------------------------*/
namespace bt709 {

float linear(float) noexcept;
float gamma(float) noexcept;

} // namespace bt709
/*-------------------------------------------------------*/
namespace bt2100_hlg { // Hybrid Log-Gamma, BBC

float linear(float) noexcept;
float gamma(float) noexcept;

} // namespace bt2100_hlg
/*-------------------------------------------------------*/
namespace bt2100_pq { // Perceptual Quantizer, Dolby Labs

float linear(float) noexcept;
float gamma(float) noexcept;

} // namespace bt2100_pq
/*-------------------------------------------------------*/
} // namespace color
} // namespace dsp
