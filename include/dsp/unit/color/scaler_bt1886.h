﻿#pragma once

#include "dsp/unit/color/type.h"
#include <cmath>

namespace dsp {
namespace color {

// [0,1] <-> [luma_min,luma_max] / 10K
template <color_pass_t Pass>
auto bt1886(float luma_min = 0.1f, float luma_max = 100.f) {
    // hdr_coverage = std::fmax(luma_max, 100.f) / 10000.f;
    // sdr_coverage = 10000.f / std::fmax(luma_max, 100.f);

    float level_max = std::pow(luma_max / 10000.f, 1 / 2.4f);
    float level_min = std::pow(luma_min / 10000.f, 1 / 2.4f);
    float contrast = level_max - level_min;

    float scale = std::pow(contrast, 2.4f);
    float zero = level_min / contrast;

    if constexpr (Pass == color_pass_t::linear)
        return [=](float x) noexcept->float {
            if (x >= 0)
                return std::pow(zero + x, 2.4f) * scale;
            return 0;
        };
    else {
        float barrier = luma_min / 10000.f;
        return [=](float x) noexcept->float {
            if (x >= barrier)
                return std::pow(x / scale, 1 / 2.4f) - zero;
            return 0;
        };
    }
}

} // namespace color
} // namespace dsp
