﻿#pragma once

namespace dsp {
namespace color {

constexpr static float whites_xy[][2]{
    {.44757f, .40745f}, // A   : 2856K
    {.34842f, .35161f}, // B   : 4874K - obsolete
    {.34567f, .35850f}, // D50 : 5003K - icc profiles
    {.33333f, .33333f}, // E   : 5454K - equal energy
    {.33242f, .34743f}, // D55 : 5503K
    {.31271f, .32902f}, // D65 : 6504K
    {.31006f, .31616f}, // C   : 6774K - obsolete
    {.29902f, .31485f}, // D75 : 7504K
};

constexpr static float whites_xyz[][3]{
    {1.09850f, 1.f, .35585f},  // A   : 2856K
    {.99072f, 1.f, .85223f},   // B   : 4874K - obsolete
    {.96422f, 1.f, .82521f},   // D50 : 5003K - icc profiles
    {1.00000f, 1.f, 1.00000f}, // E   : 5454K - equal energy
    {.95682f, 1.f, .92149f},   // D55 : 5503K
    {.95047f, 1.f, 1.08883f},  // D65 : 6504K
    {.98074f, 1.f, 1.18232f},  // C   : 6774K - obsolete
    {.94972f, 1.f, 1.22638f},  // D75 : 7504K
};

} // namespace color
} // namespace dsp
