﻿#pragma once

#include "dsp/engine/tensor.h"

namespace dsp {

enum class target_t { min, max };
enum class stat_t { v1, v2 };

namespace shuffle {

template <typename T>
constexpr double cov(tensor<T, 1> const& vec) noexcept {
    double dxy = 0;
    double mx = 0;
    double my = 0;
    for (size_t i = 0; i < vec.size[0]; ++i) {
        double r = 1. / (1 + i);
        double dx = r * (i - mx);
        double dy = r * (static_cast<double>(vec[i]) - my);
        dxy += i * dx * dy - r * dxy;
        mx += dx;
        my += dy;
    }
    return dxy;
}

template <typename T>
constexpr double
cov_delta(tensor<T, 1> const& vec, size_t i0, size_t i1) noexcept {
    assert(i0 < vec.size[0]);
    assert(i1 < vec.size[0]);
    assert(i0 != i1);
    return (static_cast<double>(i1) - static_cast<double>(i0))
        * (static_cast<double>(vec[i0]) - static_cast<double>(vec[i1]))
        / vec.size[0];
}

double cov_profit(double init, double delta) noexcept;

/*-------------------------------------------------------*/

template <typename T>
constexpr double grad(tensor<T, 1> const& vec, size_t i0, size_t i1) noexcept {
    double dx = static_cast<double>(vec[i0]) - static_cast<double>(vec[i1]);
    return dx * dx;
}

template <typename T>
constexpr double ent(tensor<T, 1> const& vec) noexcept {
    double tmp = 0;
    for (size_t i = 0; i < vec.size[0] - 1; ++i)
        tmp += grad(vec, i, i + 1);
    return tmp;
}

template <typename T>
constexpr double
ent_profit(tensor<T, 1> const& vec, size_t i0, size_t i1) noexcept {
    assert(i0 != i1);

    size_t idx0 = i0;
    size_t idx1 = i1;
    if (idx0 > idx1)
        std::swap(idx0, idx1);

    double g0 = 0;
    double g1 = 0;
    if (idx0 > 0) {
        g0 += grad(vec, idx0 - 1, idx0);
        g1 += grad(vec, idx0 - 1, idx1);
    }
    if (idx1 < vec.size[0] - 1) {
        g0 += grad(vec, idx1, idx1 + 1);
        g1 += grad(vec, idx0, idx1 + 1);
    }
    if (idx0 + 1 < idx1) {
        g0 += grad(vec, idx0, idx0 + 1) + grad(vec, idx1 - 1, idx1);
        g1 += grad(vec, idx1, idx0 + 1) + grad(vec, idx1 - 1, idx0);
    }
    return g1 - g0;
}

/*-------------------------------------------------------*/

bool is_perpective(double profit, double temp) noexcept;

template <typename T, target_t target, stat_t es>
void anneal(tensor<T, 1>& vec) noexcept {
    size_t steps = vec.size[0] * 10;
    double init = cov(vec);

    for (size_t i = steps; i != 0; --i) {
        size_t j1, j0 = math::rand() % vec.size[0];
        do
            j1 = math::rand() % vec.size[0];
        while (j0 == j1);

        double delta = cov_delta(vec, j0, j1);
        double profit;
        if constexpr (es == stat_t::v1)
            profit = ent_profit(vec, j0, j1);
        else
            profit = cov_profit(init, delta);

        if constexpr (target == target_t::min)
            profit *= -1;

        double temp = static_cast<double>(i) / static_cast<double>(steps);
        if (is_perpective(profit, temp)) {
            std::swap(vec[j0], vec[j1]);
            init += delta;
        }
    }
    echo("covariance = %.3f", init);
    echo("covariance = %.3f, entropy = %.3f", cov(vec), ent(vec));
}

/*-------------------------------------------------------*/

template <typename T>
constexpr void perfect(tensor<T, 1>& m) noexcept {
    tensor<T, 1> tmp{m.size[0]};
    tmp[m.size[0] - 1] = m[m.size[0] - 1];

    size_t half = m.size[0] / 2;
    for (size_t i = 0; i < half; ++i) {
        tmp[2 * i + 0] = m[i + half];
        tmp[2 * i + 1] = m[i];
    }
    m = tmp;
}

} // namespace shuffle
} // namespace dsp
