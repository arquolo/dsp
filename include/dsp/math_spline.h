﻿#pragma once

#include <cstddef>

namespace math {

double spl_box(double) noexcept;
double spl_pyramid(double) noexcept;
double spl_cubic(double) noexcept;
double spl_sinc(double) noexcept;

double splc_quad(double, size_t, size_t) noexcept;
double splc_pcos(double, size_t, size_t) noexcept;

// box
double splw_0(double) noexcept;

// triangular
double splw_1(double) noexcept;

// parabolic
double splw_2(double) noexcept;

// biweight
double splw_2s2(double) noexcept;

// triweight
double splw_2s3(double) noexcept;

// tricube
double splw_3s3(double) noexcept;

// cosine
double splw_cosine(double) noexcept;

// lanczos
double splw_lanczos(double) noexcept;

} // namespace math
