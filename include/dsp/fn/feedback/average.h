﻿#pragma once

#include "dsp/engine/fn_concepts.h"
#include <cassert>

namespace dsp {
namespace fn {

template <typename T, size_t N>
class average_d : public fn_struct1_<N>, public fn_skip_<N> {
    size_t n0;
    size_t n1;
    size_t n01;

public:
    constexpr average_d(size_t i, size_t j, size_t const (&step)[N]) noexcept
    : fn_struct1_<N>{step}, fn_skip_<N>{} {
        assert(i < N);
        n0 = step[i];
        this->min[i] = 1;

        assert(j < N);
        n1 = step[j];
        this->min[j] = 1;

        assert(i != j);
        n01 = n0 + n1;
    }
    T operator()(T const* p) const noexcept {
        T pred = (*(p - n0) + *(p - n1)) / 2;
        return *p - pred;
    }
};

template <typename T, size_t N>
class average_i : public fn_feedback_<T, N>, public fn_skip_<N> {
    size_t n0;
    size_t n1;
    size_t n01;

public:
    constexpr average_i(size_t i, size_t j, size_t const (&step)[N]) noexcept
    : fn_feedback_<T, N>{step, step}, fn_skip_<N>{} {
        assert(i != j);

        assert(i < N);
        n0 = step[i];
        this->min[i] = 1;
        this->ordered[i] = true;

        assert(j < N);
        n1 = step[j];
        this->min[j] = 1;
        this->ordered[j] = true;

        assert(i != j);
        n01 = n0 + n1;
    }
    void operator()(T const* ps, T* pd) const noexcept {
        T pred = (*(pd - n0) + *(pd - n1)) / 2;
        *pd = *ps + pred;
    }
};

} // namespace fn
} // namespace dsp
