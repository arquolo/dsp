﻿#pragma once

#include "dsp/engine/fn_concepts.h"
#include <cassert>
#include <type_traits>

namespace dsp {
namespace fn {

template <typename T, size_t N>
class smooth : public fn_feedback_<T, N>, public fn_skip_<N> {
    size_t n;
    float w0;
    float w1;

public:
    constexpr smooth(
        size_t axis, size_t const (&step)[N], size_t window) noexcept
    : fn_feedback_<T, N>{step, step}, fn_skip_<N>{} {
        assert(axis < N);
        n = step[axis];
        this->min[axis] = 1;
        this->ordered[axis] = true;

        w0 = std::min(1.f, 1.f / window);
        w1 = 1.f - w0;
    }

    void operator()(T const* ps, T* pd) const noexcept {
        if constexpr (std::is_same<T, float>::value) {
            *pd = *ps * w0 + *(pd - n) * w1;
            return;
        }
        *pd = static_cast<T>(
            w0 * static_cast<float>(*ps) + w1 * static_cast<float>(*(pd - n)));
    }
};

} // namespace fn
} // namespace dsp
