﻿#pragma once

#include "dsp/engine/fn_concepts.h"
#include <cassert>

namespace dsp {
namespace fn {

template <typename T, size_t N>
class last_d : public fn_struct1_<N>, public fn_skip_<N> {
    size_t n;

public:
    constexpr last_d(size_t i, size_t const (&step)[N]) noexcept
    : fn_struct1_<N>{step}, fn_skip_<N>{} {
        assert(i < N);
        n = step[i];
        this->min[i] = 1;
    }
    T operator()(T const* p) const noexcept { return *p - *(p - n); }
};

template <typename T, size_t N>
class last_i : public fn_feedback_<T, N>, public fn_skip_<N> {
    size_t n;

public:
    constexpr last_i(size_t i, size_t const (&step)[N]) noexcept
    : fn_feedback_<T, N>{step, step}, fn_skip_<N>{} {
        assert(i < N);
        n = step[i];
        this->min[i] = 1;
        this->ordered[i] = true;
    }
    void operator()(T const* ps, T* pd) const noexcept {
        *pd = *ps + *(pd - n);
    }
};

} // namespace fn
} // namespace dsp
