﻿#pragma once

#include "dsp/engine/fn_concepts.h"
#include <cassert>
#include <type_traits>

namespace dsp {
namespace fn {

template <typename T, size_t N>
class sharp : public fn_struct1_<N>, public fn_skip_<N> {
    size_t n;
    float w0;
    float w1;

public:
    constexpr sharp(
        size_t axis, size_t const (&step)[N], size_t window) noexcept
    : fn_struct1_<N>{step}, fn_skip_<N>{} {
        assert(axis < N);
        n = step[axis];
        this->min[axis] = 1;

        w0 = std::max(size_t{1}, window);
        w1 = 1 - w0;
    }

    T operator()(T const* p) const noexcept {
        if constexpr (std::is_same<T, float>::value)
            return *p * w0 + *(p - n) * w1;
        return static_cast<T>(
            w0 * static_cast<float>(*p) + w1 * static_cast<float>(*(p - n)));
    }
};

} // namespace fn
} // namespace dsp
