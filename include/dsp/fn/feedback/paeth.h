﻿#pragma once

#include "dsp/engine/fn_concepts.h"
#include <cassert>
#include <cmath>

namespace dsp {
namespace fn {
namespace util {

template <typename T>
constexpr T predict_paeth(T x0, T x1, T x01) {
    using std::abs;

    auto p0 = x0 - x01;
    auto p1 = x1 - x01;
    auto p01 = p1 + p0;

    p1 = abs(p1);
    p0 = abs(p0);
    p01 = abs(p01);

    if ((p0 <= p1) && (p0 <= p01))
        return x1;
    if (p1 <= p01)
        return x0;
    return x01;
}

} // namespace util

template <typename T, size_t N>
class paeth_d : public fn_struct1_<N>, public fn_skip_<N> {
    size_t n0;
    size_t n1;
    size_t n01;

public:
    constexpr paeth_d(size_t i, size_t j, size_t const (&step)[N]) noexcept
    : fn_struct1_<N>{step}, fn_skip_<N>{} {
        assert(i < N);
        n0 = step[i];
        this->min[i] = 1;

        assert(j < N);
        n1 = step[j];
        this->min[j] = 1;

        assert(i != j);
        n01 = n0 + n1;
    }
    T operator()(T const* p) const noexcept {
        using std::abs;

        T x01 = *(p - n01);
        T x0 = *(p - n0);
        T x1 = *(p - n1);

        auto pred = util::predict_paeth(x0, x1, x01);
        return *p - pred;
    }
};

template <typename T, size_t N>
class paeth_i : public fn_feedback_<T, N>, public fn_skip_<N> {
    size_t n0;
    size_t n1;
    size_t n01;

public:
    constexpr paeth_i(size_t i, size_t j, size_t const (&step)[N]) noexcept
    : fn_feedback_<T, N>{step, step}, fn_skip_<N>{} {
        assert(i < N);
        n0 = step[i];
        this->min[i] = 1;
        this->ordered[i] = true;

        assert(j < N);
        n1 = step[j];
        this->min[j] = 1;
        this->ordered[j] = true;

        assert(i != j);
        n01 = n0 + n1;
    }
    void operator()(T const* ps, T* pd) const noexcept {
        using std::abs;

        T x01 = *(pd - n01);
        T x0 = *(pd - n0);
        T x1 = *(pd - n1);

        auto pred = util::predict_paeth(x0, x1, x01);
        *pd = *ps + pred;
    }
};

} // namespace fn
} // namespace dsp
