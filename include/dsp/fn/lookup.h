﻿#pragma once

#include "dsp/math.h"
#include "dsp/memory.h"
#include <functional>

namespace dsp {
namespace fn {

template <typename T>
class lookup {
    static constexpr auto range_ = static_cast<size_t>(math::norm<T>) + 1;
    static constexpr auto scale_ = static_cast<float>(math::norm<T>);

    resource resource_;
    T* data_;

public:
    lookup(std::function<float(float)> const& func) noexcept
    : resource_{cache::allocate(range_ * sizeof(T))}
    , data_{resource_->as<T>()} {
        for (size_t i = 0; i < range_; ++i)
            data_[i] = static_cast<T>(func(i / scale_) * scale_);
    }
    T operator()(T x) const noexcept { return data_[x]; }
};

template <>
class lookup<float> {
    static constexpr auto range_ = size_t{0x100000};
    static constexpr auto scale_ = static_cast<float>(0xFFFFF);

    resource resource_;
    float* data_;

public:
    lookup(std::function<float(float)> const& func) noexcept
    : resource_{cache::allocate(range_ * sizeof(float))}
    , data_{resource_->as<float>()} {
        for (size_t i = 0; i < range_; ++i)
            data_[i] = func(i / scale_);
    }
    float operator()(float x) const noexcept {
        auto rx = static_cast<size_t>(x * scale_);

        float dx = (x * scale_) - rx;
        float y0 = data_[(x >= 0 && x <= 1) * (0 + rx)];
        float y1 = data_[(x >= 0 && x <= 1) * (1 + rx)];
        return math::lerp(y0, y1, dx);
    }
};

} // namespace ops
} // namespace dsp
