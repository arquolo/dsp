﻿#pragma once

#include "dsp/engine/fn_concepts.h"
#include <cassert>

namespace dsp {
namespace fn {

template <typename T, size_t N, fn_type e>
class haar : public fn_struct2_<N>, public fn_skip_<N> {
    size_t ns0, ns1, ns01;
    size_t nd0, nd1, nd01;

public:
    constexpr haar(size_t const (&step)[N], size_t offset = 0) noexcept
    : fn_struct2_<N>{step, step}, fn_skip_<N>{} {
        assert(N >= 2);

        this->min[0] += offset;
        this->min[1] += offset;
        this->max[0] -= offset;
        this->max[1] -= offset;

        this->skip[0] = 2;
        this->skip[1] = 2;

        ns0 = this->ns[N - 2];
        ns1 = this->ns[N - 1];
        ns01 = ns0 + ns1;

        nd0 = this->nd[N - 2];
        nd1 = this->nd[N - 1];
        nd01 = nd0 + nd1;
    }

    void operator()(T const* ps, T* pd) const noexcept {
        T vec[4]{
            ps[0],
            ps[ns1],
            ps[ns0],
            ps[ns01],
        };
        if constexpr (e == fn_type::direct) {
            vec[1] = vec[0] - vec[1];
            vec[2] += vec[3];

            T tmp = (vec[2] - vec[1]) / 2;
            vec[0] += tmp;
            vec[3] -= tmp;

            vec[1] -= vec[3];
            vec[2] = vec[0] - vec[2];
        } else {
            vec[1] += vec[3];
            vec[2] = vec[0] - vec[2];

            T tmp = (vec[2] - vec[1]) / 2;
            vec[0] -= tmp;
            vec[3] += tmp;

            vec[1] = vec[0] - vec[1];
            vec[2] -= vec[3];
        }
        pd[0] = vec[0];
        pd[nd1] = vec[1];
        pd[nd0] = vec[2];
        pd[nd01] = vec[3];
    }
};

} // namespace ops
} // namespace dsp
