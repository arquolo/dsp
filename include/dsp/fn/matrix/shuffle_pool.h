﻿#pragma once

#include "dsp/engine/fn_concepts.h"
#include <cassert>
#include <utility>

namespace dsp {
namespace fn {

template <typename T, size_t N, fn_type e>
class shuffle : public fn_struct2_<N> {
    size_t ns0, ns1, ns01;
    size_t nd0, nd1, nd01;

public:
    constexpr shuffle(size_t const (&step)[N], size_t offset = 0) noexcept
    : fn_struct2_<N>{step, step} {
        assert(N >= 2);

        this->min[0] += offset;
        this->min[1] += offset;
        this->max[0] -= offset;
        this->max[1] -= offset;

        this->skip[0] = 2;
        this->skip[1] = 2;

        ns0 = this->ns[N - 2];
        ns1 = this->ns[N - 1];
        ns01 = ns0 + ns1;

        nd0 = this->nd[N - 2];
        nd1 = this->nd[N - 1];
        nd01 = nd0 + nd1;
    }

    void operator()(T const* ps, T* pd) const noexcept {
        using std::swap;

        // clang-format off
        T vec[4]{
            ps[0],
            ps[ns1],
            ps[ns0],
            ps[ns01]
        };
        // clang-format on
        if constexpr (e & fn_type::rm_horz_top)
            swap(vec[0], vec[1]);
        if constexpr (e & fn_type::rm_horz_bottom)
            swap(vec[2], vec[3]);
        if constexpr (e & fn_type::rm_vert_left)
            swap(vec[0], vec[2]);
        if constexpr (e & fn_type::rm_vert_right)
            swap(vec[1], vec[3]);
        if constexpr (e & fn_type::rm_diag_major)
            swap(vec[0], vec[3]);
        if constexpr (e & fn_type::rm_diag_minor)
            swap(vec[1], vec[2]);
        pd[0] = vec[0];
        pd[nd1] = vec[1];
        pd[nd0] = vec[2];
        pd[nd01] = vec[3];
    }
};

} // namespace fn
} // namespace dsp
