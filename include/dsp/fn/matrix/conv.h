﻿#pragma once

#include "dsp/engine/fn_concepts.h"
#include "dsp/engine/tensor.h"
#include <cassert>

namespace dsp {
namespace fn {

template <size_t N>
class mat : public fn_struct2_<N>, public fn_skip_<N> {
    tensor<float, 2> const& _mat;

public:
    constexpr mat(tensor<float, 2> const& mat, size_t (&step)[N]) noexcept
    : fn_struct2_<N>{step, step}, fn_skip_<N>{}, _mat{mat} {
        assert(N >= 3);
        this->skip[N - 2] = step[N - 2];
    }

    void operator()(float const* src, float* dst) noexcept {
        // TODO: check correctness
        auto it0 = _mat(0);
        for (size_t i = 0; i < _mat.size[0]; ++i, ++it0) {
            auto it1 = it0(i);
            dst[i] = 0;
            for (size_t j = 0; j < _mat.size[1]; ++j, ++it1)
                dst[i] += *it1 * src[j];
        }
    }
};

} // namespace fn
} // namespace dsp
