﻿#pragma once

namespace dsp {
namespace fn {

template <typename T>
auto clip_hard(T threshold) noexcept {
    return [threshold](T x) noexcept->T {
        if (x <= -threshold)
            return x;
        if (x >= +threshold)
            return x;
        return 0;
    };
}

template <typename T>
auto clip_soft(T threshold) noexcept {
    return [threshold](T x) noexcept->T {
        if (x <= -threshold)
            return x + threshold;
        if (x >= +threshold)
            return x - threshold;
        return 0;
    };
}

} // namespace ops
} // namespace dsp
