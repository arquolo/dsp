﻿#pragma once

#include "dsp/engine/fn_concepts.h"
#include "dsp/math.h"
#include <cassert>
#include <initializer_list>
#include <vector>

namespace dsp {
/*-------------------------------------------------------*/

template <size_t N>
struct view_t {
    size_t min[N]{};
    size_t max[N]{};

    constexpr view_t() = default;
    constexpr view_t(std::initializer_list<size_t> s) noexcept;

    constexpr size_t size_v(size_t) const noexcept;

    template <typename F>
    constexpr void shrink(F const&) noexcept;
    template <typename F>
    constexpr void expand(F const&) noexcept;

    constexpr bool operator*(view_t<N> const&)const noexcept;

    constexpr size_t area() const noexcept;
    constexpr auto split(size_t, size_t) const noexcept;
};

/*-------------------------------------------------------*/

template <size_t N>
constexpr view_t<N>::view_t(std::initializer_list<size_t> s) noexcept {
    assert(s.size() == 2 * N);
    for (size_t i = 0; i < N; ++i) {
        min[i] = s.begin()[i];
        max[i] = s.begin()[i] + s.begin()[i + N];
    }
}

template <size_t N>
constexpr size_t view_t<N>::size_v(size_t axis) const noexcept {
    assert(axis < N);
    return max[axis] - min[axis];
}

/*-------------------------------------------------------*/

template <size_t N>
template <typename F>
constexpr void view_t<N>::shrink(F const& f) noexcept {
    if constexpr (std::is_base_of_v<fn_skip_<N>, F>)
        for (size_t i = 0; i < N; ++i) {
            min[i] += f.min[i];
            max[i] -= f.max[i];
        }
}

/*-------------------------------------------------------*/

template <size_t N>
template <typename F>
constexpr void view_t<N>::expand(F const& f) noexcept {
    if constexpr (std::is_base_of_v<fn_skip_<N>, F>)
        for (size_t i = 0; i < N; ++i) {
            min[i] -= f.min[i];
            max[i] += f.max[i];
        }
}

/*-------------------------------------------------------*/

template <size_t N>
constexpr auto view_t<N>::split(size_t axis, size_t parts) const noexcept {
    assert(parts != 0);
    if (axis == N)
        return std::vector{*this};

    std::vector<view_t<N>> v;
    v.reserve(parts);
    for (size_t i = 0; i < parts; ++i) {
        auto vt = *this;
        for (size_t j = 0; j < N; ++j) {
            if (j != axis)
                continue;

            vt.min[j] = static_cast<size_t>(math::lerp<float>(
                min[j], max[j],
                static_cast<float>(i + 0) / static_cast<float>(parts)));

            vt.max[j] = static_cast<size_t>(math::lerp<float>(
                min[j], max[j],
                static_cast<float>(i + 1) / static_cast<float>(parts)));
        }
        v.push_back(vt);
    }
    return v;
}

/*-------------------------------------------------------*/

template <size_t N>
constexpr bool view_t<N>::operator*(view_t<N> const& v) const noexcept {
    for (size_t i = 0; i < N; ++i)
        if (std::max(min[i], v.min[i]) > std::min(max[i], v.max[i]))
            return true;
    return false;
}

template <size_t N>
constexpr size_t view_t<N>::area() const noexcept {
    size_t a = 1;
    for (size_t i = 0; i < N; ++i)
        a *= size_v(i);
    return a;
}

/*-------------------------------------------------------*/
} // namespace dsp
