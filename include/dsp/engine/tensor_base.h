﻿#pragma once

#include "dsp/engine/tensor.h"

namespace dsp {
/*-------------------------------------------------------*/
namespace {
/* line_size
 * = 8  for __mm64  vectorization (MMX)
 * = 16 for __mm128 vectorization (SSE,SSE2,SSE3,SSSE3,SSE4.1,SSE4.2)
 * = 32 for __mm256 vectorization (AVX,AVX2)
 * = 64 for __mm512 vectorization (AVX512,KNC,SVML)
 *
 * __mm64 :  8 x Binary8,  4 x Binary16,  2 x Binary32, 1 x Binary64
 * __mm128: 16 x Binary8,  8 x Binary16,  4 x Binary32, 2 x Binary64
 * __mm265: 32 x Binary8, 16 x Binary16,  8 x Binary32, 4 x Binary64
 * __mm512: 64 x Binary8, 32 x Binary16, 16 x Binary32, 8 x Binary64
 */
static constexpr size_t line_size = 16;
} // namespace

template <typename T>
static constexpr size_t block_size
    = sizeof(T) <= line_size ? line_size / sizeof(T) : 1;

/*-------------------------------------------------------*/

template <typename T, size_t N>
void tensor<T, N>::clear() noexcept {
    _M_data = nullptr;
    if (_M_resource.unique())
        return;
    _M_resource.reset();
}

template <typename T, size_t N>
void tensor<T, N>::_M_create() noexcept {
    // fill steps
    step[N - 1] = 1;
    if constexpr (N >= 2) {
        step[N - 2] = math::ceil(size[N - 1], block_size<T>);
        for (size_t i = N - 2; i > 0; --i)
            step[i - 1] = size[i] * step[i];
    }
    auto capacity = size[0] * step[0];
    if (capacity > 0) {
        _M_resource = cache::allocate(sizeof(T) * capacity);
        _M_data = _M_resource->as<T>();
    }
    v();
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
constexpr tensor<T, N>::tensor(size_t const (&_size)[N]) noexcept
: tensor<T, N>() {
    std::copy_n(_size, N, this->size);
    _M_create();
}

template <typename T, size_t N>
template <typename... Args, typename>
constexpr tensor<T, N>::tensor(Args... sizes) noexcept : tensor<T, N>() {
    std::copy_n(
        std::initializer_list<size_t>({static_cast<size_t>(sizes)...}).begin(),
        sizeof...(Args), this->size);
    _M_create();
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
bool tensor<T, N>::empty() const noexcept {
    if (!_M_resource)
        return true;
    return _M_resource->empty();
}

template <typename T, size_t N>
void tensor<T, N>::create(size_t (&sizes)[N]) noexcept {
    clear();
    std::copy_n(std::begin(sizes), N, this->size);
    _M_create();
    echo_trace("->Tensor<T, N>::resize(size_t (&)[N])");
}

template <typename T, size_t N>
template <typename... Args, typename>
void tensor<T, N>::create(Args... sizes) noexcept {
    clear();
    std::copy_n(
        std::initializer_list<size_t>({static_cast<size_t>(sizes)...}).begin(),
        sizeof...(Args), this->size);
    _M_create();
    echo_trace("->Tensor<T, N>::resize(size_t (&)[N])");
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
constexpr auto& tensor<T, N>::v() noexcept {
    std::fill_n(_M_view.min, N, 0);
    std::copy_n(size, N, _M_view.max);
    return *this;
}

template <typename T, size_t N>
constexpr auto& tensor<T, N>::v(std::initializer_list<size_t> v_list) noexcept {
    _M_view = view_t<N> {v_list};
    return *this;
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
template <typename F>
constexpr auto& tensor<T, N>::operator+=(F const& func) noexcept {
    if constexpr (!std::is_base_of<fn_struct1_<N>, F>::value)
        return (*this >> *this) += func;

    if constexpr (
        std::is_base_of<fn_struct2_<N>, F>::value
        && !std::is_base_of<fn_feedback_<T, N>, F>::value)
        return (*this >> *this) += func;

    tensor<T, N> tmp {size};
    (*this >> tmp).copy_externs(func);
    (*this >> tmp) += func;
    (*this = tmp);
    return *this;
}

/*-------------------------------------------------------*/
} // namespace dsp
