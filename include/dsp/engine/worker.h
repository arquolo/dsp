﻿#pragma once

#include "dsp/engine/fn_traits.h"

namespace dsp {
/*-------------------------------------------------------*/

template <typename T, size_t N>
class worker {
    size_t _M_diff_src[N];
    size_t _M_size_src[N];
    size_t _M_last_src[N];

    size_t _M_diff_dst[N];
    size_t _M_size_dst[N];
    size_t _M_last_dst[N];

public:
    T const* ps;
    T* pd;

    constexpr worker(
        T const*,
        T*,
        size_t const (&)[N],
        size_t const (&)[N],
        size_t const (&)[N],
        size_t const (&)[N]) noexcept;

    // index-based iterator
    template <typename F, fn_cycle, size_t = 0>
    void for_idx(F const&) noexcept;

    // pointer-based iterator
    template <typename F, fn_cycle, size_t = 0>
    void for_ptr(F const&, T const*, T*) noexcept;
};

template <typename T, size_t N, typename F>
auto make_threads(std::vector<worker<T, N>>& workers, F const& func) noexcept;

/*-------------------------------------------------------*/
} // namespace dsp

#include "dsp/engine/worker_impl.h"
