﻿#pragma once

#include "dsp/engine/fn_concepts.h"
#include <experimental/type_traits>

namespace dsp {
namespace traits {

template <typename _Function>
using ordered_tag = decltype(std::declval<_Function&>().ordered);

template <typename _Function>
using skip_tag = decltype(std::declval<_Function&>().skip);

template <typename _FunctionObjectType>
static constexpr auto cycle
    = std::is_base_of_v<fn_inverse_tag, _FunctionObjectType>
    ? fn_cycle::decrement
    : fn_cycle::increment;

template <typename _Function>
constexpr bool ordered(_Function const& f, size_t axis) noexcept {
    if constexpr (std::experimental::is_detected_v<ordered_tag, _Function>)
        return f.ordered[axis];
    return false;
}

template <typename _Function>
constexpr size_t skip(_Function const& f, size_t axis) noexcept {
    if constexpr (std::experimental::is_detected_v<skip_tag, _Function>)
        return f.skip[axis];
    return 1;
}

} // namespace traits
} // namespace dsp
