﻿#pragma once

namespace dsp {

enum class fn_cycle {
    increment,
    decrement,
};

// clang-format off
enum class fn_type : unsigned {
    // direction
    direct  = 0,
    inverse = 1,

    // pass
    pass_low  = 0,
    pass_high = 1,

    // iir func
    iir_last    = 0,
    iir_smooth  = 1,
    iir_average = 2,
    iir_paeth   = 3,

    rm_horz_top    = 0x04,
    rm_horz_bottom = 0x08,
    rm_vert_left   = 0x10,
    rm_vert_right  = 0x20,
    rm_diag_major  = 0x40,
    rm_diag_minor  = 0x80
};
// clang-format on

inline constexpr fn_type operator|(fn_type lhs, fn_type rhs) noexcept {
    return static_cast<fn_type>(
        static_cast<unsigned>(lhs) | static_cast<unsigned>(rhs));
}

inline constexpr fn_type& operator|=(fn_type& lhs, fn_type rhs) noexcept {
    return lhs = static_cast<fn_type>(
               static_cast<unsigned>(lhs) | static_cast<unsigned>(rhs));
}

inline constexpr bool operator&(fn_type lhs, fn_type rhs) noexcept {
    return static_cast<unsigned>(lhs) & static_cast<unsigned>(rhs);
}

} // namespace dsp
