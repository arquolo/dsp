﻿#pragma once

#include "dsp/engine/worker.h"
#include <thread>
#include <vector>

namespace dsp {
/*-------------------------------------------------------*/

template <typename T, size_t N>
constexpr worker<T, N>::worker(
    T const* p_src,
    T* p_dst,
    size_t const (&d_src)[N],
    size_t const (&d_dst)[N],
    size_t const (&s_src)[N],
    size_t const (&s_dst)[N]) noexcept
: ps{p_src}, pd{p_dst} {
    std::copy_n(d_src, N, _M_diff_src);
    std::copy_n(s_src, N, _M_size_src);
    std::copy_n(d_dst, N, _M_diff_dst);
    std::copy_n(s_dst, N, _M_size_dst);

    for (size_t i = 0; i < N; ++i) {
        _M_last_src[i] = s_src[i] / d_src[i];
        _M_last_dst[i] = s_dst[i] / d_dst[i];
    }
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
template <typename F, fn_cycle _Cycle, size_t _Axis>
void worker<T, N>::for_idx(F const& func) noexcept {
    static_assert(_Axis < N, "No dimensions to iterate");
    static_assert(std::is_invocable<F>::value, "Argument is not callable");

    if constexpr (_Axis == 0) {
        func.src = ps;
        func.dst = pd;
        std::copy_n(_M_diff_src, N, func.diff_s);
        std::copy_n(_M_diff_dst, N, func.diff_d);
    }

    if constexpr (_Cycle == fn_cycle::increment) {
        for (func.idx[_Axis] = 0; func.idx[_Axis] != _M_last_dst[_Axis];) {
            if constexpr (_Axis != N - 1)
                for_idx<F, _Cycle, _Axis + 1>(func);
            else
                func();
            ++func.idx[_Axis];
        }
    } else {
        for (func.idx[_Axis] = _M_last_src[_Axis]; func.idx[_Axis] != 0;) {
            --func.idx[_Axis];
            if constexpr (_Axis != N - 1)
                for_idx<F, _Cycle, _Axis + 1>(func);
            else
                func();
        }
    }
}

template <typename T, size_t N>
template <typename F, fn_cycle _Cycle, size_t _Axis>
void worker<T, N>::for_ptr(F const& func, T const* init_s, T* init_d) noexcept {
    static_assert(_Axis < N, "No dimensions to iterate");
    static_assert(
        std::is_invocable<F, T const*, T*>::value
            || std::is_invocable_r<T, F, T const*>::value
            || std::is_invocable_r<T, F, T>::value,
        "Argument is not callable");

    T const* ptr_s = init_s;
    T const* end_s = init_s;

    T* ptr_d = init_d;
    T* end_d = init_d;

    if constexpr (_Cycle == fn_cycle::increment) {
        end_s += _M_size_src[_Axis];
        end_d += _M_size_dst[_Axis];
    } else {
        ptr_s += _M_size_src[_Axis];
        ptr_d += _M_size_dst[_Axis];
    }

    while (ptr_s != end_s && ptr_d != end_d) {
        // decrement
        if constexpr (_Cycle != fn_cycle::increment) {
            if constexpr (std::is_base_of_v<fn_skip_<N>, F> || _Axis != N - 1) {
                ptr_s -= _M_diff_src[_Axis];
                ptr_d -= _M_diff_dst[_Axis];
            } else {
                --ptr_s;
                --ptr_d;
            }
        }
        // main action
        if constexpr (_Axis != N - 1)
            for_ptr<F, _Cycle, _Axis + 1>(func, ptr_s, ptr_d);
        else if constexpr (std::is_invocable_v<F, T const*, T*>)
            func(ptr_s, ptr_d);
        else if constexpr (std::is_invocable_r_v<T, F, T const*>)
            *ptr_d = func(ptr_s);
        else if constexpr (std::is_invocable_r_v<T, F, T>)
            *ptr_d = func(*ptr_s);

        // increment
        if constexpr (_Cycle == fn_cycle::increment) {
            if constexpr (std::is_base_of_v<fn_skip_<N>, F> || _Axis != N - 1) {
                ptr_s += _M_diff_src[_Axis];
                ptr_d += _M_diff_dst[_Axis];
            } else {
                ++ptr_s;
                ++ptr_d;
            }
        }
    }
}

/*-------------------------------------------------------*/

template <typename T, size_t N, typename F>
auto make_threads(std::vector<worker<T, N>>& workers, F const& func) noexcept {
    std::vector<std::thread> threads;
    threads.reserve(workers.size());

    for (auto&& w : workers)
        if constexpr (std::is_base_of_v<fn_index_<T, N>, F>)
            threads.emplace_back(
                &worker<T, N>::template for_idx<F, traits::cycle<F>>, &w,
                std::ref(func));
        else
            threads.emplace_back(
                &worker<T, N>::template for_ptr<F, traits::cycle<F>>, &w,
                std::ref(func), w.ps, w.pd);
    return threads;
}

/*-------------------------------------------------------*/
} // namespace dsp
