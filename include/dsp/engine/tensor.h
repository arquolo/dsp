﻿#pragma once

#include "dsp/algorithm.h"
#include "dsp/engine/view.h"
#include "dsp/engine/worker.h"
#include "dsp/logging.h"
#include "dsp/memory.h"
#include <numeric>

namespace dsp {

enum axis_mask_t {
    axis_0 = 1u,
    axis_1 = 2u,
    axis_2 = 4u,
    axis_3 = 8u,
    axis_all = 15u
};

template <typename T, size_t N>
class tensor {
    static_assert(N != 0, "N cannot be zero");

    template <size_t _Axis, typename _Storage, typename _Pointer>
    class _Iterator : public std::bidirectional_iterator_tag {
        _Pointer _M_ptr;
        _Storage& _M_src;
        size_t _M_step;

    public:
        constexpr _Iterator(_Storage& src, _Pointer ptr) noexcept
        : _M_ptr(ptr), _M_src(src), _M_step(src.step[_Axis]) {}
        //! provides access to data
        constexpr T& operator*() const noexcept { return *_M_ptr; }

        //! provides access to raw pointer
        constexpr operator _Pointer() noexcept { return _M_ptr; }
        constexpr auto operator&() const noexcept { return _M_ptr; }

        //! increment
        constexpr auto& operator++() {
            if constexpr (_Axis != N - 1)
                _M_ptr += _M_step;
            else
                ++_M_ptr;
            return *this;
        }

        //! decrement
        constexpr auto& operator--() {
            if constexpr (_Axis != N - 1)
                _M_ptr -= _M_step;
            else
                --_M_ptr;
            return *this;
        }

        //! returns deeper iterator
        constexpr auto operator()(size_t n) const noexcept {
            static_assert(_Axis < N, "No dimensions to iterate");
            return _Iterator<_Axis + 1, _Storage, _Pointer>(
                _M_src, _M_ptr + n * _M_src.step[_Axis + 1]);
        }
    };

    class _Pair {
        // captured tensors
        tensor<T, N>& _M_src;
        tensor<T, N>& _M_dst;
        bool _M_processed{false};

    public:
        constexpr _Pair(tensor<T, N>&, tensor<T, N>&) noexcept;
        ~_Pair() noexcept;

        void copy_externs(...) noexcept {}
        void copy_externs(fn_skip_<N> const&) noexcept;

        constexpr void zero_cut(size_t = axis_mask_t::axis_all) noexcept;
        constexpr void zero_exp(size_t = axis_mask_t::axis_all) noexcept;

        template <typename F>
        auto& operator+=(F const&) noexcept;

        template <
            template <typename, fn_type...> typename F,
            fn_type... _Ops,
            typename... _Args>
        constexpr void go(_Args&&... args) noexcept {
            *this += F<T, _Ops...>{std::forward<_Args>(args)...};
        }

        template <
            template <typename, size_t, fn_type...> typename F,
            fn_type... _Ops,
            typename... _Args>
        constexpr void go(_Args&&... args) noexcept {
            *this += F<T, N, _Ops...>{std::forward<_Args>(args)...};
        }

        template <
            template <size_t, fn_type...> typename F,
            fn_type... _Ops,
            typename... _Args>
        constexpr void go(_Args&&... args) noexcept {
            *this += F<N, _Ops...>{std::forward<_Args>(args)...};
        }
    };

    //! data storage
    resource _M_resource{};
    view_t<N> _M_view{};
    T* _M_data{nullptr};

    //! updates contents according to this->size
    void _M_create() noexcept;

public:
    // header
    size_t size[N]{};
    size_t step[N]{};

    //! provides access to data
    T* data() const noexcept { return _M_data; }

    /*-------------------------------------------------------*/
    // constructors
    constexpr tensor() = default;
    constexpr tensor(size_t const (&)[N]) noexcept;

    template<typename... Args,
             typename = std::enable_if_t<(sizeof... (Args) == N)
                                         && (... && std::is_integral_v<Args>)>>
    constexpr tensor(Args...) noexcept;

    constexpr tensor(const tensor<T, N>&) = default;
    constexpr tensor(tensor<T, N>&&) = default;

    tensor<T, N>& operator=(const tensor<T, N>&) = default;
    tensor<T, N>& operator=(tensor<T, N>&&) = default;

    /*-------------------------------------------------------*/
    // helpers
    bool empty() const noexcept;
    void clear() noexcept;

    void create(size_t (&sizes)[N]) noexcept;

    template<typename... Args,
             typename = std::enable_if_t<(sizeof... (Args) == N)
                                         && (... && std::is_integral_v<Args>)>>
    void create(Args...) noexcept;

    /*-------------------------------------------------------*/
    // access
    T& operator[](size_t idx) noexcept { return _M_data[idx]; }
    T& operator[](size_t idx) const noexcept { return _M_data[idx]; }

    auto operator()(size_t n) noexcept {
        return _Iterator<0, std::remove_const_t<decltype(*this)>, T*>(
            *this, data() + n * step[0]);
    }

    auto operator()(size_t n) const noexcept {
        return _Iterator<0, std::add_const_t<decltype(*this)>, T const*>(
            *this, data() + n * step[0]);
    }

    /*-------------------------------------------------------*/
    // reset view
    constexpr auto& v() noexcept;
    constexpr auto& v(std::initializer_list<size_t>) noexcept;

    /*-------------------------------------------------------*/
    // apply F for each
    template <typename F>
    constexpr auto& operator+=(F const&) noexcept;

    template <
        template <typename, fn_type...> typename F,
        fn_type... _Fns,
        typename... _Args>
    constexpr auto& go(_Args&&... args) noexcept {
        return (*this += F<T, _Fns...>(std::forward<_Args>(args)...));
    }

    template <
        template <typename, size_t, fn_type...> typename F,
        fn_type... _Fns,
        typename... _Args>
    constexpr auto& go(_Args&&... args) noexcept {
        return (*this += F<T, N, _Fns...>(std::forward<_Args>(args)...));
    }

    template <
        template <size_t, fn_type...> typename F,
        fn_type... _Fns,
        typename... _Args>
    constexpr auto& go(_Args&&... args) noexcept {
        return (*this += F<N, _Fns...>(std::forward<_Args>(args)...));
    }

    /*-------------------------------------------------------*/
    // element-wise modification
    constexpr auto& operator=(T value) noexcept {
        return (*this += [value](T) noexcept { return value; });
    }

    constexpr auto& operator+=(T value) noexcept {
        return (*this += [value](T x) noexcept { return x + value; });
    }

    constexpr auto& operator*=(T value) noexcept {
        return (*this += [value](T x) noexcept { return x * value; });
    }

    /*-------------------------------------------------------*/
    // group to pair
    constexpr _Pair operator>>(tensor<T, N>& dst) noexcept {
        return _Pair{*this, dst};
    }
};

} // namespace dsp

#include "dsp/engine/tensor_base.h"
#include "dsp/engine/tensor_pair.h"
