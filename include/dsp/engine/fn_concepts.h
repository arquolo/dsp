﻿#pragma once

#include "dsp/engine/fn_enum.h"
#include <algorithm>
#include <cstddef>

namespace dsp {

//! Function object which treats source as Bidirectional iterator.
//! Not restricted in order of invocation
template <size_t N>
struct fn_struct1_ {
    size_t ns[N];

    constexpr fn_struct1_(size_t const (&n_src)[N]) noexcept {
        std::copy(n_src, n_src + N, ns);
    }
};

//! Function object which treats destination as Bidirectional iterator.
//! Not restricted in order of invocation
template <size_t N>
struct fn_struct2_ : public fn_struct1_<N> {
    size_t nd[N];

    constexpr fn_struct2_(
        size_t const (&n_src)[N], size_t const (&n_dst)[N]) noexcept
    : fn_struct1_<N>(n_src) {
        std::copy(n_dst, n_dst + N, nd);
    }
};

//! Function object which treats source and destination
//! as Bidirectional iterator. Invokes sequentially.
template <typename T, size_t N>
struct fn_feedback_ : public fn_struct2_<N> {
    bool ordered[N]{};

    constexpr fn_feedback_(
        size_t const (&n_src)[N], size_t const (&n_dst)[N]) noexcept
    : fn_struct2_<N>(n_src, n_dst) {}
};

//! Function object which invokes sequentially in backward direction
struct fn_inverse_tag {};

//! Function object which treats destination as RandomAccess iterator.
//! Not restricted in order of invocation
template <typename T, size_t N>
struct fn_index_ {
    T const* ps;
    T* pd;

    size_t idx[N]; //! index for destination
    size_t diff_s[N];
    size_t diff_d[N];
};

//! Function object which invokes sparsly
template <size_t N>
struct fn_skip_ {
    size_t min[N]{};
    size_t max[N]{};
    size_t skip[N];

    constexpr fn_skip_() noexcept { std::fill_n(skip, N, 1); }
};

} // namespace dsp
