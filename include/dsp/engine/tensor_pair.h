﻿#pragma once

#include "dsp/engine/tensor.h"

namespace dsp {
/*-------------------------------------------------------*/

namespace {
static constexpr size_t hw_threads = 4;
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
constexpr tensor<T, N>::_Pair::_Pair(
    tensor<T, N>& src, tensor<T, N>& dst) noexcept: _M_src(src), _M_dst(dst) {
    if (&_M_src == &_M_dst || _M_dst._M_resource != nullptr)
        return;
    _M_dst.create(_M_src.size);
}

template <typename T, size_t N>
tensor<T, N>::_Pair::~_Pair() noexcept {
    if (_M_processed)
        return;
    *this += [](T x) { return x; };
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
void tensor<T, N>::_Pair::copy_externs(fn_skip_<N> const& f) noexcept {
    _M_processed = true;
    if (&_M_src == &_M_dst)
        return;

    _M_src.v();
    _M_dst.v();

    auto& vs = _M_src._M_view;
    auto& vd = _M_dst._M_view;

    for (size_t i = 0; i < N; ++i) {
        vs.max[i] = f.min[i];
        vd.max[i] = f.min[i];
        _M_src >> _M_dst;

        if (i + 1 == N)
            break;

        vs.min[i] = f.min[i];
        vs.max[i] = _M_src.size[i] - f.max[i];
        vd.min[i] = f.min[i];
        vd.max[i] = _M_dst.size[i] - f.max[i];
    }

    for (std::ptrdiff_t i = N - 1; i >= 0; --i) {
        vs.min[i] = _M_src.size[i] - f.max[i];
        vs.max[i] = _M_src.size[i];
        vd.min[i] = _M_dst.size[i] - f.max[i];
        vd.max[i] = _M_dst.size[i];
        _M_src >> _M_dst;

        if (i == 0)
            break;

        vs.min[i] = 0;
        vd.min[i] = 0;
    }
    _M_src.v();
    _M_dst.v();
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
constexpr void tensor<T, N>::_Pair::zero_cut(size_t axis_mask) noexcept {
    _M_processed = true;
    if (&_M_src == &_M_dst)
        return;

    _M_src.v();
    size_t s[N];
    std::copy_n(_M_src.size, N, s);

    for (size_t i = 0; i < N; ++i)
        if ((axis_mask >> i) & 1u) {
            --s[i];
            ++_M_src._M_view.min[i];
        }
    _M_dst.create(s);

    _M_src >> _M_dst;
    _M_src.v();
}

template <typename T, size_t N>
constexpr void tensor<T, N>::_Pair::zero_exp(size_t axis_mask) noexcept {
    _M_processed = true;
    if (&_M_src == &_M_dst)
        return;

    size_t s[N];
    std::copy_n(_M_src.size, N, s);

    for (size_t i = 0; i < N; ++i)
        if ((axis_mask >> i) & 1u)
            ++s[i];
    _M_dst.create(s);

    if (axis_mask & 1u) {
        _M_dst._M_view.max[0] = 1;
        _M_dst = 0;
    }
    for (size_t i = 0; i < N; ++i) {
        if ((axis_mask >> i) & 1u) {
            _M_dst._M_view.min[i] = 1;
            _M_dst._M_view.max[i] = _M_dst.size[i];
        }
        if (i + 1 != N && ((axis_mask >> (i + 1)) & 1u)) {
            _M_dst._M_view.max[i + 1] = 1;
            _M_dst = 0;
        }
    }
    _M_src >> _M_dst;
    _M_dst.v();
}

/*-------------------------------------------------------*/

template <typename T, size_t N>
template <typename F>
auto& tensor<T, N>::_Pair::operator+=(F const& f) noexcept {
    _M_processed = true;

    if (&_M_src != &_M_dst)
        _M_dst._M_view.shrink(f);
    _M_src._M_view.shrink(f);

    // find axis for splitting
    auto axis_s = [&] {
        for (size_t axis = 0; axis < N; ++axis) {
            if (traits::ordered(f, axis))
                continue;

            auto fs = traits::skip(f, axis);
            if (_M_src._M_view.size_v(axis) < hw_threads * fs)
                continue;
            if (_M_dst._M_view.size_v(axis) < hw_threads * fs)
                continue;

            return axis;
        }
        return N;
    }();

    size_t fs = traits::skip(f, axis_s);
    size_t d_src[N];
    size_t d_dst[N];
    std::transform(
        _M_src.step, _M_src.step + N, d_src, [fs](auto x) { return x * fs; });
    std::transform(
        _M_dst.step, _M_dst.step + N, d_dst, [fs](auto x) { return x * fs; });

    // split views
    auto views_src = _M_src._M_view.split(axis_s, hw_threads);
    auto views_dst = _M_dst._M_view.split(axis_s, hw_threads);

    // create workers
    std::vector<worker<T, N>> workers;
    for (const auto& [v_src, v_dst] : zip(views_src, views_dst)) {

        T const* p_src = std::inner_product(
            std::begin(v_src.min), std::end(v_src.min), std::begin(_M_src.step),
            _M_src.data());
        T* p_dst = std::inner_product(
            std::begin(v_dst.min), std::end(v_dst.min), std::begin(_M_dst.step),
            _M_dst.data());

        size_t s_src[N];
        size_t s_dst[N];
        for (size_t axis = 0; axis < N; ++axis) {
            s_src[axis] = math::floor(
                v_src.size_v(axis) * _M_src.step[axis], d_src[axis]);
            s_dst[axis] = math::floor(
                v_dst.size_v(axis) * _M_dst.step[axis], d_dst[axis]);
        }
        workers.emplace_back(p_src, p_dst, d_src, d_dst, s_src, s_dst);
    }

    // create and invoke threads
    auto threads = make_threads<T, N, F>(workers, f);
    for (auto&& t : threads)
        t.join();

    if (&_M_src != &_M_dst)
        _M_dst._M_view.expand(f);
    _M_src._M_view.expand(f);

    return _M_dst;
}

/*-------------------------------------------------------*/
} // namespace dsp
