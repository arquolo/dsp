﻿#pragma once

#include <vector>

namespace math {

// linear solver
std::vector<double> roots(double, double) noexcept;

// quadratic solver
std::vector<double> roots(double, double, double) noexcept;

// cubic solver
std::vector<double> roots(double, double, double, double) noexcept;

// quadric solver
std::vector<double> roots(double, double, double, double, double) noexcept;

} // namespace math
