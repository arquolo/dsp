﻿#pragma once

#include <iterator>
#include <tuple>
#include <utility>

template <typename... T>
auto zip(T&&... sequences) {
    using value_type = std::tuple<decltype(*std::begin(std::declval<T>()))...>;
    using item_type = std::tuple<decltype(std::begin(std::declval<T>()))...>;

    class zipper {
        class _Iterator : std::iterator<std::forward_iterator_tag, value_type> {
            item_type _M_its;

        public:
            explicit _Iterator(item_type its) noexcept
            : _M_its{std::move(its)} {}

            auto operator*() const noexcept {
                return std::apply(
                    [](auto&... it) { return std::make_tuple((*it)...); },
                    _M_its);
            }

            _Iterator& operator++() noexcept {
                std::apply([](auto&... it) { (++it, ...); }, _M_its);
                return *this;
            }

            bool operator!=(_Iterator const& other) const noexcept {
                return _M_its != other._M_its;
            }
        };

        _Iterator _M_begin;
        _Iterator _M_end;

    public:
        zipper(T&... sequences)
        : _M_begin{std::make_tuple(std::begin(sequences)...)}
        , _M_end{std::make_tuple(std::end(sequences)...)} {}

        _Iterator begin() const { return _M_begin; }
        _Iterator end() const { return _M_end; }
    };

    return zipper{sequences...};
}
