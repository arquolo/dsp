type Region struct { //region of Bipmap
	Min    Rect //start of region
	Max    Rect //end of region
	Size   Rect //size of region
	Quant  Rect //size of block
	Blocks Rect //number of blocks

	Unit [][]Vector
}

var FZero float64 = 0.1

func (r *Region) Init(minX, minY, maxX, maxY, stepX, stepY int) {
	r.Min = Rect{minX, minY}
	r.Max = Rect{maxX, maxY}
	r.Size = Rect{maxX - minX, maxY - minY}
	r.Quant = Rect{stepX, stepY}
	r.Blocks = Rect{(maxX - minX) / stepX, (maxY - minY) / stepY}
}

func (r *Region) DataLoad(b *Bitmap, blocky bool) {
	rsx, bkx, qsx := r.Min.X, r.Blocks.X, r.Quant.X
	rsy, bky, qsy := r.Min.Y, r.Blocks.Y, r.Quant.Y
	streams := qsx * qsy

	r.Unit = make([][]Vector, bky)
	for i := 0; i < bky; i++ {
		r.Unit[i] = make([]Vector, bkx)
		for j := 0; j < bkx; j++ {
			r.Unit[i][j] = NewVector(streams)
		}
	}
	if blocky {
		for sy := 0; sy < bky; sy++ {
			for y := 0; y < qsy; y++ {
				for sx := 0; sx < bkx; sx++ {
					for x := 0; x < qsx; x++ {
						r.Unit[sy][sx][y*qsx+x] = b.Pix[sy*qsy+y+rsy][sx*qsx+x+rsx]
					}
				}
			}
		}
	} else {
		for sy := 0; sy < bky; sy++ {
			for y := 0; y < qsy; y++ {
				for sx := 0; sx < bkx; sx++ {
					for x := 0; x < qsx; x++ {
						r.Unit[sy][sx][y*qsx+x] = b.Pix[sy+y*bky+rsy][sx+x*bkx+rsx]
					}
				}
			}
		}
	}
}

func (r *Region) DataUnload(b *Bitmap, blocky bool) {
	rsx, bkx, qsx := r.Min.X, r.Blocks.X, r.Quant.X
	rsy, bky, qsy := r.Min.Y, r.Blocks.Y, r.Quant.Y
	if blocky {
		for sy := 0; sy < bky; sy++ {
			for sx := 0; sx < bkx; sx++ {
				for y := 0; y < qsy; y++ {
					for x := 0; x < qsx; x++ {
						b.Pix[sy*qsy+y+rsy][sx*qsx+x+rsx] = r.Unit[sy][sx][y*qsx+x]
					}
				}
			}
		}
	} else {
		for sy := 0; sy < bky; sy++ {
			for sx := 0; sx < bkx; sx++ {
				for y := 0; y < qsy; y++ {
					for x := 0; x < qsx; x++ {
						b.Pix[sy+y*bky+rsy][sx+x*bkx+rsx] = r.Unit[sy][sx][y*qsx+x]
					}
				}
			}
		}
	}
}

func (r *Region) DataShift(dx, dy int, cover int) {
	var (
		bkx, qsx int = r.Blocks.X, r.Quant.X
		bky, qsy int = r.Blocks.Y, r.Quant.Y
		streams  int = qsx * qsy
		bsize    int = qsx * qsy / cover
		dbas     [][][]float64
	)
	dbas = make([][][]float64, bky)
	for y := 0; y < bky; y++ {
		dbas[y] = make([][]float64, bkx)
		for x := 0; x < bkx; x++ {
			dbas[y][x] = make([]float64, streams)
		}
	}
	for y := 0; y < bky; y++ {
		for x := 0; x < bkx; x++ {
			for k := 1; k < cover; k++ {
				for i := k * bsize; i < (k+1)*bsize; i++ {
					dbas[y][x][i] = r.Unit[(bky+y+k*dy)%bky][(bkx+x+k*dx)%bkx][i]
				}
			}
		}
	}
	for y := 0; y < bky; y++ {
		for x := 0; x < bkx; x++ {
			for k := 1; k < cover; k++ {
				for i := k * bsize; i < (k+1)*bsize; i++ {
					r.Unit[y][x][i] = dbas[y][x][i]
				}
			}
		}
	}
}

func (r *Region) Pack(f *Filter, bZeroMean, bScal bool) {
	var (
		bkx, bky int = r.Blocks.X, r.Blocks.Y
		dims     int = r.Quant.X * r.Quant.Y
	)
	for y := 0; y < bky; y++ {
		for x := 0; x < bkx; x++ {
			r.Unit[y][x] = r.Unit[y][x].SpaceForw(f.Orth, dims)
		}
	}
	if bZeroMean {
		for y := 0; y < bky; y++ {
			for x := 0; x < bkx; x++ {
				for i := 0; i < dims; i++ {
					r.Unit[y][x][i] -= f.Mean[i]
				}
			}
		}
	}
	if bScal {
		for y := 0; y < bky; y++ {
			for x := 0; x < bkx; x++ {
				for i := 0; i < dims; i++ {
					r.Unit[y][x][i] /= f.Scal[i]
				}
			}
		}
	}

}

func (r *Region) Unpack(f *Filter, bZeroMean, bScal bool) {
	var (
		bkx, bky int = r.Blocks.X, r.Blocks.Y
		dims     int = r.Quant.X * r.Quant.Y
	)
	if bScal {
		for y := 0; y < bky; y++ {
			for x := 0; x < bkx; x++ {
				for i := 0; i < dims; i++ {
					r.Unit[y][x][i] *= f.Scal[i]
				}
			}
		}
	}
	if bZeroMean {
		for y := 0; y < bky; y++ {
			for x := 0; x < bkx; x++ {
				for i := 0; i < dims; i++ {
					r.Unit[y][x][i] += f.Mean[i]
				}
			}
		}
	}
	for y := 0; y < bky; y++ {
		for x := 0; x < bkx; x++ {
			r.Unit[y][x] = r.Unit[y][x].SpaceBack(f.Orth, dims) //f.Crop
		}
	}
}

func (r *Region) Quantize(f *Filter) {
	var (
		bkx, bky int = r.Blocks.X, r.Blocks.Y
		dims     int = r.Quant.X * r.Quant.Y
	)
	if BThres {
		for y := 0; y < bky; y++ {
			for x := 0; x < bkx; x++ {
				for k := 0; k < dims; k++ {
					r.Unit[y][x][k] = Zip(r.Unit[y][x][k], FZero)
				}
			}
		}
	} else {
		for y := 0; y < bky; y++ {
			for x := 0; x < bkx; x++ {
				for k := f.Crop; k < dims; k++ {
					r.Unit[y][x][k] = 0
				}
			}
		}
	}
}

func (b *Bitmap) InitThreshold(keep float64) {
	w, h := b.Size.X, b.Size.Y
	s := w * h
	m := make([]float64, s)
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			m[w*y+x] = math.Abs(b.Pix[y][x])
		}
	}
	sort.Float64s(m)
	FZero = m[int((1-keep)*float64(s-1))]
}

func (b *Bitmap) ZipAll() {
	w, h := b.Size.X, b.Size.Y
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			b.Pix[y][x] = Zip(b.Pix[y][x], FZero)
		}
	}
}
