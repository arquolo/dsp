const (
	FLoss  float64 = 0.01 //variance to lose in channels
	BThres bool    = true
)

type Filter struct {
	Dims      int
	Orth      Matrix //orthogonal matrix
	Mean      Vector //mean values for outputs
	Disp      Vector //dispersion of outputs
	DispTotal float64
	Scal      Vector
	Crop      int
}

type FilterChain struct {
	id     []Filter
	stages int
}

func (f *Filter) Init(dims int) {
	f.Dims = dims
	f.Orth = NewMatrix(dims, dims)
	f.Mean = NewVector(dims)
	f.Disp = NewVector(dims)
	f.Scal = NewVector(dims)
}

//initialize covariance matrix
func (f *Filter) InitKLT(r *Region, bZeroMean bool) {
	var (
		dims     int = r.Quant.X * r.Quant.Y
		counts   int = r.Blocks.X * r.Blocks.Y
		bkx, bky int = r.Blocks.X, r.Blocks.Y
		means    Vector
		sigma    Matrix
	)
	f.Init(dims)
	means = NewVector(dims)
	sigma = NewMatrix(dims, dims)
	for ky := 0; ky < bky; ky++ {
		for kx := 0; kx < bkx; kx++ {
			for i := 0; i < dims; i++ {
				means[i] += r.Unit[ky][kx][i]
				for j := i; j < dims; j++ {
					sigma[i][j] += r.Unit[ky][kx][i] * r.Unit[ky][kx][j]
				}
			}
		}
	}
	for i := 0; i < dims; i++ {
		means[i] /= float64(counts)
	}
	if bZeroMean {
		for i := 0; i < dims; i++ {
			sigma[i][i] = sigma[i][i]/float64(counts) - means[i]*means[i]
		}
		for i := 0; i < dims-1; i++ {
			for j := i + 1; j < dims; j++ {
				tmp := sigma[i][j]/float64(counts) - means[i]*means[j]
				sigma[i][j], sigma[j][i] = tmp, tmp
			}
		}
	} else {
		for i := 0; i < dims; i++ {
			sigma[i][i] = sigma[i][i] / float64(counts)
		}
		for i := 0; i < dims-1; i++ {
			for j := i + 1; j < dims; j++ {
				tmp := sigma[i][j] / float64(counts)
				sigma[i][j], sigma[j][i] = tmp, tmp
			}
		}
	}
	EigenSearch(sigma, f.Orth)
	f.Mean = f.Orth.AimTo(means)
	for i := 0; i < dims; i++ {
		f.Disp[i] = math.Abs(sigma[i][i])
		f.DispTotal += f.Disp[i]
	}
	return
}

func (f *Filter) InitScal() {
	var (
		tmps float64
	)
	for f.Crop = f.Dims; (tmps < f.DispTotal*FLoss) && (f.Crop > 1); f.Crop-- {
		tmps += f.Disp[f.Crop-1]
	}
	for i := 0; i < f.Dims; i++ {
		f.Scal[i] = math.Max(1, math.Sqrt(f.Disp[i]))
	}
}
