func (x Vector) SpaceForw(m Matrix, depth int) Vector {
	var (
		cin  int    = len(m[0])
		cout int    = Min(len(m), depth)
		y    Vector = NewVector(cout)
	)
	for c1 := 0; c1 < cout; c1++ {
		for c0 := 0; c0 < cin; c0++ {
			y[c1] += m[c1][c0] * x[c0]
		}
	}
	return y
}

func (x Vector) SpaceBack(m Matrix, depth int) Vector {
	var (
		cin  int    = len(m[0])
		cout int    = Min(len(m), depth)
		y    Vector = NewVector(cin)
	)
	for c1 := 0; c1 < cout; c1++ {
		for c0 := 0; c0 < cin; c0++ {
			y[c0] += x[c1] * m[c1][c0]
		}
	}
	return y
}
