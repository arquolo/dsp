const (
	BHorz = 1
	BVert = 2
)

type LagVector struct {
	Dims int
	Coef Vector //values for outputs
}

func (f *LagVector) InitLag(r *Region, dims int, kind int) {
	f.Dims = dims
	f.Coef = NewVector(dims)

	var (
		bkx2, bky2 int
		chunkarr   []Matrix = make([]Matrix, dims+1)
		shift      Vector   = NewVector(dims)
		sigma      Matrix   = NewMatrixSquare(dims)
		orth       Matrix   = NewMatrixSquare(dims)
	)
	switch kind {
	case BHorz:
		{
			bkx2, bky2 = r.Blocks.X-2*(dims+1), r.Blocks.Y
			for k := 0; k <= dims; k++ {
				chunkarr[k] = NewMatrix(bky2, bkx2)
			}
			for i := 0; i < bky2; i++ {
				for j := 0; j < bkx2; j++ {
					chunkarr[0][i][j] = r.Unit[i][j+dims+1][1]
				}
			}
			for k := 1; k <= dims; k++ {
				for i := 0; i < bky2; i++ {
					for j := 0; j < bkx2; j++ {
						chunkarr[k][i][j] = r.Unit[i][j+dims+1-k][0] - r.Unit[i][j+dims+1+k][0]
					}
				}
			}
		}
	case BVert:
		{
			bkx2, bky2 = r.Blocks.X, r.Blocks.Y-2*(dims+1)
			for k := 0; k <= dims; k++ {
				chunkarr[k] = NewMatrix(bky2, bkx2)
			}
			for i := 0; i < bky2; i++ {
				for j := 0; j < bkx2; j++ {
					chunkarr[0][i][j] = r.Unit[i+dims+1][j][1]
				}
			}
			for k := 1; k <= dims; k++ {
				for i := 0; i < bky2; i++ {
					for j := 0; j < bkx2; j++ {
						chunkarr[k][i][j] = r.Unit[i+dims+1-k][j][0] - r.Unit[i+dims+1+k][j][0]
					}
				}
			}
		}
	}
	for k := 1; k <= dims; k++ {
		for i := 0; i < bky2; i++ {
			tmp := 0.0
			for j := 0; j < bkx2; j++ {
				tmp += chunkarr[0][i][j] * chunkarr[k][i][j]
			}
			shift[k-1] += tmp
		}
		shift[k-1] /= float64(bkx2 * bky2)
		for p := k; p <= dims; p++ {
			for i := 0; i < bky2; i++ {
				tmp := 0.0
				for j := 0; j < bkx2; j++ {
					tmp += chunkarr[k][i][j] * chunkarr[p][i][j]
				}
				sigma[k-1][p-1] += tmp
			}
			sigma[k-1][p-1] /= float64(bkx2 * bky2)
		}
	}
	for k := 0; k < dims-1; k++ {
		for p := k + 1; p < dims; p++ {
			sigma[p][k] = sigma[k][p]
		}
	}
	if dims != 1 {
		EigenSearch(sigma, orth)
		EigenInvers(sigma, orth)
	} else {
		f.Coef[0] = shift[0] / sigma[0][0]
	}

	fmt.Println()
	for k := 0; k < dims; k++ {
		for p := 0; p < dims; p++ {
			f.Coef[k] = sigma[k][p] * shift[p]
		}
		fmt.Printf("%.6e ", f.Coef[k])
	}
	fmt.Println()
}

func (r *Region) ApplyLag(f *LagVector, kind int) {
	var (
		dims     int = f.Dims
		bkx, bky int
	)
	switch kind {
	case BHorz:
		{
			bkx, bky = r.Blocks.X-2*(dims+1), r.Blocks.Y
			for i := 0; i < bky; i++ {
				for k := 1; k <= dims; k++ {
					for j := (dims + 1); j < bkx-(dims+1); j++ {
						r.Unit[i][j][1] -= f.Coef[k-1] * (r.Unit[i][j-k][0] - r.Unit[i][j+k][0])
					}
				}
			}
		}
	case BVert:
		{
			bkx, bky = r.Blocks.X, r.Blocks.Y-2*(dims+1)
			for k := 1; k < dims+1; k++ {
				for i := (dims + 1); i < bky-(dims+1); i++ {
					for j := 0; j < bkx; j++ {
						r.Unit[i][j][1] -= f.Coef[k-1] * (r.Unit[i-k][j][0] - r.Unit[i-k][j][0])
					}
				}
			}
		}
	}
}

func (r *Region) RemoveLag(f *LagVector, kind int) {
	var (
		dims     int = f.Dims
		bkx, bky int
	)
	switch kind {
	case BHorz:
		{
			bkx, bky = r.Blocks.X-2*(dims+1), r.Blocks.Y
			for i := 0; i < bky; i++ {
				for k := 1; k <= dims; k++ {
					for j := (dims + 1); j < bkx-(dims+1); j++ {
						r.Unit[i][j][1] += f.Coef[k-1] * (r.Unit[i][j-k][0] - r.Unit[i][j+k][0])
					}
				}
			}
		}
	case BVert:
		{
			bkx, bky = r.Blocks.X, r.Blocks.Y-2*(dims+1)
			for k := 1; k < dims+1; k++ {
				for i := (dims + 1); i < bky-(dims+1); i++ {
					for j := 0; j < bkx; j++ {
						r.Unit[i][j][1] += f.Coef[k-1] * (r.Unit[i-k][j][0] - r.Unit[i-k][j][0])
					}
				}
			}
		}
	}
}
