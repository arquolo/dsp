const (
	bZeroMean bool    = true
	fDataKeep float64 = 0.025 //non zero values to keep
	split     bool    = false //keep data grouped in blocks
	block     int     = 8     //block size
	lv        int     = 2     //levels to dive
	stg       int     = 4     //stages to analyze (really [4*stg+1])
)

func main() {
	for q := 1; q < len(os.Args); q++ {
		str := strings.TrimRight(os.Args[q], ".png")
		sourc, _ := os.Open(str + ".png")
		src, _ := png.Decode(sourc)
		w, h := src.Bounds().Dx(), src.Bounds().Dy()
		img := image.NewRGBA(image.Rect(0, 0, w, h))
		im0 := image.NewGray(image.Rect(0, 0, w*3, h))
		im1 := image.NewRGBA(image.Rect(0, 0, w, h))
		for y := 0; y < h; y++ {
			for x := 0; x < w; x++ {
				oldColor := src.At(x, y)
				rgbaColor := color.RGBAModel.Convert(oldColor)
				img.Set(x, y, rgbaColor)
			}
		}
		var (
			b imgtf.Bitmap
			r [3*lv - 2]imgtf.Region
			f [3*lv - 2][4*stg + 1]imgtf.Filter
		)
		r[0].Init(0, 0, 3*w, h, 3*block, block)
		mp := 1
		for z := 1; z < 3*lv-2; z += 3 {
			mp *= block
			r[z+0].Init(0, 0, w/mp, h/mp, block, block)
			r[z+1].Init(w/block, 0, w/block+w/mp, h/mp, block, block)
			r[z+2].Init(2*w/block, 0, 2*w/block+w/mp, h/mp, block, block)
		}
		b.LoadBitmap(img)
		for z := 0; z < 3*lv-2; z++ {
			r[z].DataLoad(&b, true)

			f[z][0].InitKLT(&r[z], bZeroMean)
			for i := 0; i < 4*stg; i += 4 {
				r[z].Pack(&f[z][i], bZeroMean, false)
				r[z].DataShift(1, 0, 2)
				f[z][i+1].InitKLT(&r[z], bZeroMean)

				r[z].Pack(&f[z][i+1], bZeroMean, false)
				r[z].DataShift(-1, 0, 2)
				f[z][i+2].InitKLT(&r[z], bZeroMean)

				r[z].Pack(&f[z][i+2], bZeroMean, false)
				r[z].DataShift(0, 1, 2)
				f[z][i+3].InitKLT(&r[z], bZeroMean)

				r[z].Pack(&f[z][i+3], bZeroMean, false)
				r[z].DataShift(0, -1, 2)
				f[z][i+4].InitKLT(&r[z], bZeroMean)
			}
			//f[z][4*stg].InitScal()
			//r[z].Pack(&f[z][4*stg], bZeroMean, true)
			r[z].Pack(&f[z][4*stg], bZeroMean, false)
			r[z].DataUnload(&b, split)
		}
		b.InitThreshold(fDataKeep)
		b.ZipAll()
		b.SaveBitmapSplit(im0)

		for z := 3*lv - 3; z != -1; z-- {
			r[z].DataLoad(&b, split)
			r[z].Quantize(&f[z][4*stg])
			//r[z].Unpack(&f[z][4*stg], bZeroMean, true)
			r[z].Unpack(&f[z][4*stg], bZeroMean, false)
			for i := 4 * stg; i != 0; i -= 4 {
				r[z].DataShift(0, 1, 2)
				r[z].Unpack(&f[z][i-1], bZeroMean, false)
				r[z].DataShift(0, -1, 2)
				r[z].Unpack(&f[z][i-2], bZeroMean, false)
				r[z].DataShift(1, 0, 2)
				r[z].Unpack(&f[z][i-3], bZeroMean, false)
				r[z].DataShift(-1, 0, 2)
				r[z].Unpack(&f[z][i-4], bZeroMean, false)
			}
			r[z].DataUnload(&b, true)
		}
		b.SaveBitmap(im1)

		file0, _ := os.Create(str + "_mid_" + strconv.FormatInt(int64(block), 10) + "x" + strconv.FormatInt(int64(lv), 10) + "+" + strconv.FormatInt(int64(stg), 10) + "c3_" + strconv.FormatFloat(imgtf.PSNR(img, im1), 'f', 5, 64) + ".png")
		file1, _ := os.Create(str + "_" + strconv.FormatInt(int64(block), 10) + "x" + strconv.FormatInt(int64(lv), 10) + "+" + strconv.FormatInt(int64(stg), 10) + "c3_" + strconv.FormatFloat(imgtf.PSNR(img, im1), 'f', 5, 64) + ".png")
		png.Encode(file0, im0)
		png.Encode(file1, im1)
	}
}
