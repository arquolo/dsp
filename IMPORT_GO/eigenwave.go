const (
	bZeroMean bool    = true
	fDataKeep float64 = 0.025 //non zero values to keep
	split     bool    = false //keep data grouped in blocks
	block     int     = 8     //block size
	lv        int     = 2     //levels to dive
	stg       int     = 4     //stages to analyze (really [4*stg+1])
)

func main() {
	for q := 1; q < len(os.Args); q++ {
		str := strings.TrimRight(os.Args[q], ".png")
		sourc, _ := os.Open(str + ".png")
		src, _ := png.Decode(sourc)
		w, h := src.Bounds().Dx(), src.Bounds().Dy()
		img := image.NewRGBA(image.Rect(0, 0, w, h))
		im0 := image.NewRGBA(image.Rect(0, 0, w, h))
		im1 := image.NewRGBA(image.Rect(0, 0, w, h))
		for y := 0; y < h; y++ {
			for x := 0; x < w; x++ {
				oldColor := src.At(x, y)
				rgbaColor := color.RGBAModel.Convert(oldColor)
				img.Set(x, y, rgbaColor)
			}
		}
		var (
			b [3]imgtf.Bitmap
			r [lv][3]imgtf.Region
			f [lv][4*stg + 1][3]imgtf.Filter
		)
		for k := 0; k < 3; k++ {
			var mp int = 1
			for z := 0; z < lv; z++ {
				r[z][k].Init(0, 0, w/mp, h/mp, block, block)
				mp *= block
			}
		}
		for k := 0; k < 3; k++ {
			b[k].LoadBitmapChannel(img, k)
			for z := 0; z < lv; z++ {
				r[z][k].DataLoad(&b[k], true)

				f[z][0][k].InitKLT(&r[z][k], bZeroMean)
				for i := 0; i < 4*stg; i += 4 {
					r[z][k].Pack(&f[z][i][k], bZeroMean, false)
					r[z][k].DataShift(1, 0, 2)
					f[z][i+1][k].InitKLT(&r[z][k], bZeroMean)

					r[z][k].Pack(&f[z][i+1][k], bZeroMean, false)
					r[z][k].DataShift(-1, 0, 2)
					f[z][i+2][k].InitKLT(&r[z][k], bZeroMean)

					r[z][k].Pack(&f[z][i+2][k], bZeroMean, false)
					r[z][k].DataShift(0, 1, 2)
					f[z][i+3][k].InitKLT(&r[z][k], bZeroMean)

					r[z][k].Pack(&f[z][i+3][k], bZeroMean, false)
					r[z][k].DataShift(0, -1, 2)
					f[z][i+4][k].InitKLT(&r[z][k], bZeroMean)
				}
				//f[z][4*stg][k].InitScal()
				//r[z][k].Pack(&f[z][4*stg][k], bZeroMean, true)
				r[z][k].Pack(&f[z][4*stg][k], bZeroMean, false)
				r[z][k].DataUnload(&b[k], split)
			}
			b[k].InitThreshold(fDataKeep)
			b[k].ZipAll()
			b[k].SaveBitmapChannel(im0, k)
		}
		for k := 0; k < 3; k++ {
			for z := lv - 1; z != -1; z-- {
				r[z][k].DataLoad(&b[k], split)
				//r[z][k].Quantize(&f[z][4*stg][k])
				//r[z][k].Unpack(&f[z][4*stg][k], bZeroMean, true)
				r[z][k].Unpack(&f[z][4*stg][k], bZeroMean, false)
				for i := 4 * stg; i != 0; i -= 4 {
					r[z][k].DataShift(0, 1, 2)
					r[z][k].Unpack(&f[z][i-1][k], bZeroMean, false)
					r[z][k].DataShift(0, -1, 2)
					r[z][k].Unpack(&f[z][i-2][k], bZeroMean, false)
					r[z][k].DataShift(1, 0, 2)
					r[z][k].Unpack(&f[z][i-3][k], bZeroMean, false)
					r[z][k].DataShift(-1, 0, 2)
					r[z][k].Unpack(&f[z][i-4][k], bZeroMean, false)
				}
				r[z][k].DataUnload(&b[k], true)
			}
			b[k].SaveBitmapChannel(im1, k)
		}
		file0, _ := os.Create(str + "_mid_" + strconv.FormatInt(int64(block), 10) + "x" + strconv.FormatInt(int64(lv), 10) + "+" + strconv.FormatInt(int64(stg), 10) + "_" + strconv.FormatFloat(imgtf.PSNR(img, im1), 'f', 5, 64) + ".png")
		file1, _ := os.Create(str + "_" + strconv.FormatInt(int64(block), 10) + "x" + strconv.FormatInt(int64(lv), 10) + "+" + strconv.FormatInt(int64(stg), 10) + "_" + strconv.FormatFloat(imgtf.PSNR(img, im1), 'f', 5, 64) + ".png")
		png.Encode(file0, im0)
		png.Encode(file1, im1)
	}
}
