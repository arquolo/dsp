const (
	stepMax    int     = 1000000
	rAccuracy  float64 = 1e-12
	eigenOrder bool    = true
)

//rotates M from left, applicable for Eigenvectors
func (m Matrix) Rotate1(p, q int, s, c float64) {
	var (
		n      int = len(m)
		t1, t2 float64
		wp, wq float64
	)
	for i := 0; i < n; i++ {
		t1 = m[p][i]
		t2 = m[q][i]
		m[p][i] = t1*c + t2*s
		m[q][i] = -t1*s + t2*c
	}
	for i := 0; i < n; i++ {
		wp += m[p][i] * m[p][i]
		wq += m[q][i] * m[q][i]
	}
	wp, wq = 1/math.Sqrt(wp), 1/math.Sqrt(wq)
	for i := 0; i < n; i++ {
		m[p][i] *= wp
		m[q][i] *= wq
	}
}

//rotates M from both sides, applicable for Eigenvalues
func (m Matrix) Rotate2(p, q int, s, c float64) {
	var (
		n          int = len(m)
		t1, t2, t3 float64
	)
	for i := 0; i < n; i++ {
		if (i != p) && (i != q) {
			t1 = m[p][i]
			t2 = m[q][i]
			m[p][i] = t1*c + t2*s
			m[q][i] = -t1*s + t2*c
			m[i][p] = m[p][i]
			m[i][q] = m[q][i]
		}
	}
	t1, t2, t3 = m[p][p], m[p][q], m[q][q]
	m[p][p] = t1*c*c + 2*t2*s*c + t3*s*s
	m[p][q] = (t3-t1)*s*c + t2*(c*c-s*s)
	m[q][q] = t1*s*s - 2*t2*s*c + t3*c*c
	m[q][p] = m[p][q]
}

//returns position of maximal element from lateral elements of M
func (m Matrix) MaxLateral() (im, jm int, rmf float64) {
	var (
		rm, tmp float64
		n       int = len(m)
	)
	im, jm = 0, 1
	for i := 0; i < n-1; i++ {
		for j := i + 1; j < n; j++ {
			tmp = math.Abs(m[i][j])
			if tmp > rm {
				rm = tmp
				im, jm = i, j
			}
		}
	}
	rmf = rm * rm
	return
}

//returns not-less order of diagonal of M
func (m Matrix) SortCentral() (qu []int) {
	var (
		n        int = len(m)
		tmi, tmj int
		pmi, pmj float64
	)
	qu = make([]int, n)
	for i := 0; i < n; i++ {
		qu[i] = i
	}
	for i := 0; i < n-1; i++ {
		for j := i + 1; j < n; j++ {
			tmi, tmj = qu[i], qu[j]
			pmi, pmj = math.Abs(m[tmi][tmi]), math.Abs(m[tmj][tmj])
			if pmi < pmj {
				qu[i], qu[j] = tmj, tmi
			}
		}
	}
	return
}

//reorganises lines of M in order of Qu
func (m Matrix) Reorder(qu []int) {
	var (
		n  int = len(m)
		id int
		p  Matrix = NewMatrix(n, n)
	)
	for i := 0; i < n; i++ {
		id = qu[i]
		for j := 0; j < n; j++ {
			p[i][j] = m[id][j]
		}
	}
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			m[i][j] = p[i][j]
		}
	}
	p = nil
}

//reorganises lines and columns of M in order of Qu
func (m Matrix) ReorderAll(qu []int) {
	var (
		n  int = len(m)
		id int
		p  Matrix = NewMatrix(n, n)
	)
	for i := 0; i < n; i++ {
		id = qu[i]
		for j := 0; j < n; j++ {
			p[i][j] = m[id][j]
		}
	}
	for i := 0; i < n; i++ {
		id = qu[i]
		for j := 0; j < n; j++ {
			m[j][i] = p[j][id]
		}
	}
	p = nil
}

//modifies M to its eigenvalues, and U to its Eigenvectors
func EigenSearch(m, u Matrix) {
	var (
		n            int = len(m)
		im, jm, step int
		rmf          float64 = 1
		t, s, c      float64
	)
	for i := 0; i < n; i++ {
		u[i][i] = 1
	}
	for (rmf > rAccuracy) && (step < stepMax) {
		im, jm, rmf = m.MaxLateral()
		t = 0.5 * math.Atan(2*m[im][jm]/(m[im][im]-m[jm][jm]))
		s, c = math.Sin(t), math.Cos(t)
		u.Rotate1(im, jm, s, c)
		m.Rotate2(im, jm, s, c)
		step++
	}
	if eigenOrder {
		var sr []int = m.SortCentral()
		u.Reorder(sr)
		m.ReorderAll(sr)
	}
}
