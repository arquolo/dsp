//saves (U*V) result to V
func (v Vector) Pack(u Matrix) {
	var (
		cout int    = len(u)
		cin  int    = len(u[0])
		vt   Vector = NewVector(cout)
	)
	for c1 := 0; c1 < cout; c1++ {
		for c0 := 0; c0 < cin; c0++ {
			vt[c1] += u[c1][c0] * v[c0]
		}
	}
	for i := 0; i < cout; i++ {
		v[i] = vt[i]
	}
	vt = nil
}

//saves (V*U)^T result to V
func (v Vector) Unpack(u Matrix) {
	var (
		cout int    = len(u)
		cin  int    = len(u[0])
		vt   Vector = NewVector(cin)
	)
	for c1 := 0; c1 < cout; c1++ {
		for c0 := 0; c0 < cin; c0++ {
			vt[c0] += v[c1] * u[c1][c0]
		}
	}
	for i := 0; i < cin; i++ {
		v[i] = vt[i]
	}
	vt = nil
}

//returns (U*V)
func (v Vector) PackEx(u Matrix) Vector {
	var (
		cout int    = len(u)
		cin  int    = len(u[0])
		vt   Vector = NewVector(cout)
	)
	for c1 := 0; c1 < cout; c1++ {
		for c0 := 0; c0 < cin; c0++ {
			vt[c1] += u[c1][c0] * v[c0]
		}
	}
	return vt
}

//returns (V*U)^T
func (v Vector) UnpackEx(u Matrix) Vector {
	var (
		cout int    = len(u)
		cin  int    = len(u[0])
		vt   Vector = NewVector(cin)
	)
	for c1 := 0; c1 < cout; c1++ {
		for c0 := 0; c0 < cin; c0++ {
			vt[c0] += v[c1] * u[c1][c0]
		}
	}
	return vt
}
