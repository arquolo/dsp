const (
	BSaveDelta bool    = true //keep only delta
	BNormDelta bool    = true //normalize by variance, actual if BSaveDelta==true
	BLossy     bool    = true
	FKeep      float64 = 0.995 //variance to keep
	normThres  float64 = 1
)

type Region struct { //region of Bipmap
	Min  Rect //start of region
	Max  Rect //end of region
	Size Rect //size of region
	Step Rect //size of block
	Blks Rect //number of blocks

	Unit []Vector

	Outputs    int //number of outputs
	Redundancy int //number of inputs/outputs

	MeansIn  Vector //mean values for inputs
	MeansOut Vector //mean values for outputs

	Ortho         Matrix //orthogonal matrices
	Sigma         Matrix //covariance matrices
	Variance      Vector //dispersion of outputs
	VarianceTotal float64
	STSkip        int
}

func (r *Region) Init(minX, minY, maxX, maxY, stepX, stepY int) {
	r.Min = Rect{minX, minY}
	r.Max = Rect{maxX, maxY}
	r.Step = Rect{stepX, stepY}
	r.Size = Rect{maxX - minX, maxY - minY}
	r.Blks = Rect{(maxX - minX) / stepX, (maxY - minY) / stepY}
	r.Outputs = stepX * stepY
	r.Redundancy = 1
	r.MeansIn = NewVector(stepX * stepY)
	r.MeansOut = NewVector(stepX * stepY)
	r.Ortho = NewMatrix(stepX*stepY, stepX*stepY)
	r.Sigma = NewMatrix(stepX*stepY, stepX*stepY)
	r.Variance = NewVector(stepX * stepY)
}

func (r *Region) DataLoad(b *Bitmap, blocky bool) {
	rsx, bkx, ssx := r.Min.X, r.Blks.X, r.Step.X
	rsy, bky, ssy := r.Min.Y, r.Blks.Y, r.Step.Y
	blocks, streams := bkx*bky, ssx*ssy

	r.Unit = make([]Vector, blocks)
	for i := 0; i < blocks; i++ {
		r.Unit[i] = NewVector(streams)
	}
	if blocky {
		for sy := 0; sy < bky; sy++ {
			for y := 0; y < ssy; y++ {
				for sx := 0; sx < bkx; sx++ {
					for x := 0; x < ssx; x++ {
						r.Unit[sy*bkx+sx][y*ssx+x] = b.Pixf[sy*ssy+y+rsy][sx*ssx+x+rsx]
					}
				}
			}
		}
	} else {
		for sy := 0; sy < bky; sy++ {
			for y := 0; y < ssy; y++ {
				for sx := 0; sx < bkx; sx++ {
					for x := 0; x < ssx; x++ {
						r.Unit[sy*bkx+sx][y*ssx+x] = b.Pixf[sy+y*bky+rsy][sx+x*bkx+rsx]
					}
				}
			}
		}
	}
}

func (r *Region) DataUnload(b *Bitmap, blocky bool) {
	rsx, bkx, ssx := r.Min.X, r.Blks.X, r.Step.X
	rsy, bky, ssy := r.Min.Y, r.Blks.Y, r.Step.Y
	if blocky {
		for sy := 0; sy < bky; sy++ {
			for sx := 0; sx < bkx; sx++ {
				for y := 0; y < ssy; y++ {
					for x := 0; x < ssx; x++ {
						b.Pixf[sy*ssy+y+rsy][sx*ssx+x+rsx] = r.Unit[sy*bkx+sx][y*ssx+x]
					}
				}
			}
		}
	} else {
		for sy := 0; sy < bky; sy++ {
			for sx := 0; sx < bkx; sx++ {
				for y := 0; y < ssy; y++ {
					for x := 0; x < ssx; x++ {
						b.Pixf[sy+y*bky+rsy][sx+x*bkx+rsx] = r.Unit[sy*bkx+sx][y*ssx+x]
					}
				}
			}
		}
	}
	/*
		blocks := bkx * bky
		for i := 0; i < blocks; i++ {
			r.Unit[i] = nil
		}
		r.Unit = nil
	*/
}

//initialize covariance matrix
func (r *Region) InitCovariance() {
	var (
		blocks int = r.Outputs * r.Redundancy
		counts int = r.Blks.X * r.Blks.Y
	)
	for k := 0; k < counts; k++ {
		for i := 0; i < blocks; i++ {
			r.MeansIn[i] += r.Unit[k][i]
			for j := i; j < blocks; j++ {
				r.Sigma[i][j] += r.Unit[k][i] * r.Unit[k][j]
			}
		}
	}
	for i := 0; i < blocks; i++ {
		r.MeansIn[i] /= float64(counts)
		r.Sigma[i][i] = r.Sigma[i][i]/float64(counts) - r.MeansIn[i]*r.MeansIn[i]
	}
	for i := 0; i < blocks-1; i++ {
		for j := i + 1; j < blocks; j++ {
			tmp := r.Sigma[i][j]/float64(counts) - r.MeansIn[i]*r.MeansIn[j]
			r.Sigma[i][j], r.Sigma[j][i] = tmp, tmp
		}
	}
	EigenSearch(r.Sigma, r.Ortho)
	r.MeansOut = r.MeansIn.PackEx(r.Ortho)
	for i := 0; i < blocks; i++ {
		if r.MeansOut[i] < 0 {
			for j := 0; j < 0; j++ {
				r.Ortho[i][j] = -r.Ortho[i][j]
			}
		}
	}
	r.MeansOut = r.MeansIn.PackEx(r.Ortho)
	for i := 0; i < blocks; i++ {
		//r.Variance[i] = math.Abs(r.Sigma[i][i])
		r.Variance[i] = math.Sqrt(math.Abs(r.Sigma[i][i]))
		r.VarianceTotal += r.Sigma[i][i]
	}
	var (
		s float64
		m int
	)
	for (s < r.VarianceTotal*FKeep) && (m < blocks) {
		s += r.Sigma[m][m]
		m++
	}
	r.STSkip = m
}

func (r *Region) Pack() {
	blocks := r.Blks.X * r.Blks.Y
	for i := 0; i < blocks; i++ {
		r.Unit[i].Pack(r.Ortho)
	}
	if BSaveDelta {
		streams := r.Outputs
		for i := 0; i < blocks; i++ {
			for j := 0; j < streams; j++ {
				r.Unit[i][j] -= r.MeansOut[j]
			}
		}
	}
	if BNormDelta {
		streams := r.Outputs
		for i := 0; i < blocks; i++ {
			for j := 0; j < streams; j++ {
				r.Unit[i][j] /= math.Max(1, normThres*r.Variance[j])
			}
		}
	}
}

func (r *Region) Unpack() {
	blocks := r.Blks.X * r.Blks.Y
	if BNormDelta {
		streams := r.Outputs
		for i := 0; i < blocks; i++ {
			for j := 0; j < streams; j++ {
				r.Unit[i][j] *= math.Max(1, normThres*r.Variance[j])
			}
		}
	}
	if BLossy {
		streams := r.Outputs
		for i := 0; i < blocks; i++ {
			for j := r.STSkip; j < streams; j++ {
				r.Unit[i][j] = 0
			}
		}
	}
	if BSaveDelta {
		streams := r.Outputs
		for i := 0; i < blocks; i++ {
			for j := 0; j < streams; j++ {
				r.Unit[i][j] += r.MeansOut[j]
			}
		}
	}
	for i := 0; i < blocks; i++ {
		r.Unit[i].Unpack(r.Ortho)
	}
}
