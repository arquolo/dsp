type Bitmap struct {
	Pix     [][]int16
	Pixf    [][]float64 //internal workspace
	Size    Rect        //dimensions of bits2d
	SizeOrg Rect        //original size
	ZigZag  bool
}

//converts Bitmap to ZigZag, uses [][]float64
func UpdateZigzag(b, z *Bitmap) {
	var (
		i, j int
		n    int = b.Size.X * b.Size.Y
	)
	for k := 0; k < n; k++ {
		z.Pixf[0][k] = b.Pixf[i][j]
		if (i+j)%2 == 1 {
			i++ // walk down ...
			j-- // ... and left
			if i == b.Size.Y {
				i--
				j += 2
			}
			if j < 0 {
				j = 0
			}
		} else {
			i-- // walk up ...
			j++ // ... and right
			if j == b.Size.X {
				i += 2
				j--
			}
			if i < 0 {
				i = 0
			}
		}
	}
	return
}

//converts ZigZag to Bitmap, uses [][]float64
func UpdateBitmap(b, z *Bitmap) {
	var (
		i, j int
		n    int = b.Size.X * b.Size.Y
	)
	for k := 0; k < n; k++ {
		b.Pixf[i][j] = z.Pixf[0][k]
		if (i+j)%2 == 1 {
			i++ // walk down ...
			j-- // ... and left
			if i == b.Size.Y {
				i--
				j += 2
			}
			if j < 0 {
				j = 0
			}
		} else {
			i-- // walk up ...
			j++ // ... and right
			if j == b.Size.X {
				i += 2
				j--
			}
			if i < 0 {
				i = 0
			}
		}
	}
}
