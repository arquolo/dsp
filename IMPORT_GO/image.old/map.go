type Map struct {
	DepthUsed int //number of last updated layer
	Depth     int //number of layers
	Layer     []Layer
}

//adds layer to Bitmap
func (m *Map) AddLayer() {
	m.Depth++
	m.Layer = make([]Layer, m.Depth)
}

func (m *Map) Pack(b *Bitmap) {
	for i := m.DepthUsed; i < m.Depth; i++ {
		m.Layer[i].Pack(b)
	}
	m.DepthUsed = m.Depth
}

func (m *Map) Unpack(b *Bitmap) {
	for i := m.DepthUsed - 1; i >= 0; i-- {
		m.Layer[i].Unpack(b)
	}
	m.DepthUsed = 0
}
