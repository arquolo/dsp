const bKeepBlocks bool = false

type Layer struct {
	Size   int //number of regions
	Region []Region
}

//wrapper
//adds region started from [minX,minY] and sized by [sizeX,sizeY] to Bitmap
func (s *Layer) AddRegionSimple(minX, minY, maxX, maxY, stepX, stepY int) {
	i := s.Size
	s.Size++
	s.Region = make([]Region, i+1)
	s.Region[i].Init(minX, minY, maxX, maxY, stepX, stepY)
}

//wrapper
//adds region started from [minX,minY] and sized by [sizeX,sizeY] to Bitmap
func (s *Layer) AddRegion(minX, minY, maxX, maxY, step int, dir string) {
	switch dir {
	case "horz":
		s.AddRegionSimple(minX, minY, maxX, maxY, step, 1)
	case "vert":
		s.AddRegionSimple(minX, minY, maxX, maxY, 1, step)
	case "both":
		s.AddRegionSimple(minX, minY, maxX, maxY, step, step)
	}
}

//wrapper
//adds zone started from [minX,minY] and sized by [sizeX,sizeY] to Bitmap
func (s *Layer) AddRegionLapped(minX, minY, maxX, maxY, stepX, stepY int, overlap int) {
}

func (s *Layer) Pack(b *Bitmap) {
	for i := 0; i < s.Size; i++ {
		s.Region[i].DataLoad(b, true)
		s.Region[i].InitCovariance()
		s.Region[i].Pack()
		s.Region[i].DataUnload(b, bKeepBlocks)
	}
}

func (s *Layer) Unpack(b *Bitmap) {
	for i := s.Size - 1; i >= 0; i-- {
		s.Region[i].DataLoad(b, bKeepBlocks)
		s.Region[i].Unpack()
		s.Region[i].DataUnload(b, true)
	}
}
