var (
	ss float64 = 0.5
	kk float64 = math.Atan(1 / ss)
)

func foo(x float64) float64 {
	return 0.5 * (1 + ss*math.Tan(kk*(2*x-1)))
}

func main() {
	for q := 1; q < len(os.Args); q++ {
		var (
			str  string = strings.TrimRight(os.Args[q], ".png")
			h, w int
		)
		src, _ := os.Open(str + ".png")
		im0, _ := png.Decode(src)

		w, h = im0.Bounds().Dx(), im0.Bounds().Dy()
		im1 := image.NewRGBA(image.Rect(0, 0, w, h))
		var (
			rf, gf, bf    [256]int
			rss, gss, bss int
			ra, ga, ba    float32
			rs, gs, bs    [256]int
			rb, gb, bb    [256]byte
		)
		for y := 0; y < h; y++ {
			for x := 0; x < w; x++ {
				r, g, b, _ := im0.At(x, y).RGBA()
				rf[r%256]++
				gf[g%256]++
				bf[b%256]++
			}
		}
		for i := 0; i < 256; i++ {
			rss += rf[i]
			gss += gf[i]
			bss += bf[i]
		}
		ra, ga, ba = 1/float32(rss), 1/float32(gss), 1/float32(bss)
		rs[0], gs[0], bs[0] = rf[0], gf[0], bf[0]
		for i := 1; i < 256; i++ {
			rs[i] = rs[i-1] + rf[i]
			gs[i] = gs[i-1] + gf[i]
			bs[i] = bs[i-1] + bf[i]
		}
		for i := 0; i < 256; i++ {
			/*
				rb[i] = byte(255 * foo(ra*rf[i]))
				gb[i] = byte(255 * foo(ga*gf[i]))
				bb[i] = byte(255 * foo(ba*bf[i]))
			*/
			rb[i] = byte(255 * ra * float32(rs[i]))
			gb[i] = byte(255 * ga * float32(gs[i]))
			bb[i] = byte(255 * ba * float32(bs[i]))
		}
		for y := 0; y < h; y++ {
			for x := 0; x < w; x++ {
				r, g, b, _ := im0.At(x, y).RGBA()
				col := color.RGBA{rb[r%256], gb[g%256], bb[b%256], 255}
				im1.Set(x, y, col)
			}
		}

		files, _ := os.Create(str + "_eqAll.png")
		png.Encode(files, im1)
	}
}
