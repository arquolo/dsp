const (
	fDataKeep float64 = 0.025 //non zero values to keep //0.025
	split     bool    = false //keep data grouped in blocks
	lv        int     = 6     //levels to go
	sc        int     = 5
)

func main() {
	for q := 1; q < len(os.Args); q++ {
		str := strings.TrimRight(os.Args[q], ".png")
		sourc, _ := os.Open(str + ".png")
		src, _ := png.Decode(sourc)
		w, h := src.Bounds().Dx(), src.Bounds().Dy()
		img := image.NewRGBA(image.Rect(0, 0, w, h))
		im0 := image.NewRGBA(image.Rect(0, 0, w, h))
		im1 := image.NewRGBA(image.Rect(0, 0, w, h))
		for y := 0; y < h; y++ {
			for x := 0; x < w; x++ {
				oldColor := src.At(x, y)
				rgbaColor := color.RGBAModel.Convert(oldColor)
				img.Set(x, y, rgbaColor)
			}
		}
		var (
			b      [3]imgtf.Bitmap
			rv, rh [lv][3]imgtf.Region
			f      imgtf.Filter
			fv, fh [lv][3]imgtf.LagVector
			scr    int
		)
		f.InitHaar()
		for k := 0; k < 3; k++ {
			var mp int = 1
			for z := 0; z < lv; z++ {
				rh[z][k].Init(0, 0, w/mp, h/mp, 2, 1)
				rv[z][k].Init(0, 0, w/mp, h/mp, 1, 2)
				mp *= 2
			}
		}
		for k := 0; k < 3; k++ {
			b[k].LoadBitmapChannel(img, k)
			for z := 0; z < lv; z++ {
				scr = imgtf.Min(rh[z][k].Blocks.X/4, sc)

				rh[z][k].DataLoad(&b[k], true)
				rh[z][k].Pack(&f, false, false)
				fh[z][k].InitLag(&rh[z][k], scr, imgtf.BHorz)
				rh[z][k].ApplyLag(&fh[z][k], imgtf.BHorz)
				rh[z][k].DataUnload(&b[k], split)

				scr = imgtf.Min(rv[z][k].Blocks.Y/4, sc)

				rv[z][k].DataLoad(&b[k], true)
				rv[z][k].Pack(&f, false, false)
				fv[z][k].InitLag(&rv[z][k], scr, imgtf.BVert)
				rv[z][k].ApplyLag(&fv[z][k], imgtf.BVert)
				rv[z][k].DataUnload(&b[k], split)
			}
			b[k].InitThreshold(fDataKeep)
			b[k].ZipAll()
			b[k].SaveBitmapChannel(im0, k)
		}
		for k := 0; k < 3; k++ {
			for z := lv - 1; z >= 0; z-- {
				rv[z][k].DataLoad(&b[k], split)
				//rv[k].Quantize(&f)
				rv[z][k].RemoveLag(&fv[z][k], imgtf.BVert)
				rv[z][k].Unpack(&f, false, false)
				rv[z][k].DataUnload(&b[k], true)

				rh[z][k].DataLoad(&b[k], split)
				//r[k].Quantize(&f)
				rh[z][k].RemoveLag(&fh[z][k], imgtf.BHorz)
				rh[z][k].Unpack(&f, false, false)
				rh[z][k].DataUnload(&b[k], true)
			}
			b[k].SaveBitmapChannel(im1, k)
		}
		file0, _ := os.Create(str + "_mid_2x" + strconv.FormatInt(int64(lv), 10) + "_int_" + strconv.FormatFloat(imgtf.PSNR(img, im1), 'f', 5, 64) + ".png")
		file1, _ := os.Create(str + "_2x" + strconv.FormatInt(int64(lv), 10) + "_int_" + strconv.FormatFloat(imgtf.PSNR(img, im1), 'f', 5, 64) + ".png")
		png.Encode(file0, im0)
		png.Encode(file1, im1)
	}
}
