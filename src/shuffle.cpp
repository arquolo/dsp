﻿
#include "dsp/unit/shuffle.h"

namespace dsp {
namespace shuffle {

double cov_profit(double init, double delta) noexcept {
    return delta * (2 * init + delta);
}

bool is_perpective(double profit, double temp) noexcept {
    if (profit > 0)
        return true;
    if (temp > 0)
        if (std::rand() < RAND_MAX * std::exp(profit / temp))
            return true;
    return false;
}

} // namespace shuffle
} // namespace dsp
