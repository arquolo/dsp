﻿
#include "dsp/math.h"
#include "dsp/math_roots.h"

namespace math {

std::vector<double> roots(double a0, double a1) noexcept {
    std::vector<double> result;
    if (is_null(a0) && is_null(a1))
        result.push_back(0);
    if (!is_null(a0))
        result.push_back(-a1 / a0);
    return result;
}

std::vector<double> roots(double a0, double a1, double a2) noexcept {
    // clang-format off
    std::vector<double> result;
    if (is_null(a0)) {
        result = roots(a1, a2);
        result.push_back(0);
    }
    if (!is_null(a0)) {
        double b1 =-a1/a0/2;
        double b2 = a2/a0;
        double d = b1*b1 - b2;
        if (is_null(d))
            result.push_back(b1);
        if (is_plus(d)) {
            result.push_back(b1 - std::sqrt(d));
            result.push_back(b1 + std::sqrt(d));
        }
    }
    return result;
    // clang-format on
}

std::vector<double> roots(double a0, double a1, double a2, double a3) noexcept {
    // clang-format off
    std::vector<double> result;
    if (is_null(a0)) {
        result = roots(a1, a2, a3);
        result.push_back(0);
    }
    if (!is_null(a0)) {
        double b1 =-a1/a0/3;
        double b2 = a2/a0;
        double b3 = a3/a0;
        double p = b2/3 - b1*b1;
        double q =-b1*b1*b1 + b1*b2/6 + b3/2;
        double s = p*p*p + q*q;

        if (is_minus(p)) {
            if (is_minus(s)) {
                double arg =  std::acos(q * std::pow(-p, -1.5)) / 3;
                double amp = -std::sqrt(-p) * 2;
                result.push_back(b1 + amp * std::cos(arg));
                result.push_back(b1 + amp * std::cos(arg - 2 * M_PI / 3));
                result.push_back(b1 + amp * std::cos(arg + 2 * M_PI / 3));
            }
            if (is_null(s)) {
                double amp = std::sqrt(-p);
                result.push_back(b1 + amp*2);
                result.push_back(b1 - amp  );
            }
            if (is_plus(s)) {
                double f = std::log(
                        std::abs(q)*std::pow(-p,-1.5) +
                        std::sqrt(-s/(p*p*p))
                      ) / 3;
                result.push_back(-sign(q) * std::sqrt(-p) * std::cosh(f)*2 - b1);
            }
        }
        if (is_null(p))
            result.push_back(-sign(q) * std::sqrt(2*std::fabs(q)) + b1);
        if (is_plus(p)) {
            double f = std::log(q*std::pow(p,-1.5) + std::sqrt(s/(p*p*p))) / 3;
            result.push_back(-2 * std::sqrt(p) * std::sinh(f) + b1);
        }
    }
    return result;
    // clang-format on
}

std::vector<double>
roots(double a0, double a1, double a2, double a3, double a4) noexcept {
    // clang-format off
    std::vector<double> result;
    if (is_null(a0)) {
        result = roots(a1, a2, a3, a4);
        result.push_back(0);
    }
    if (!is_null(a0)) {
        double b1 =-a1/a0/4;
        double b2 = a2/a0;
        double b3 = a3/a0;
        double b4 = a4/a0;
        double p = b2 - 6*b1*b1;
        double q = b3 - 8*b1*b1*b1 + 2*b1*b2;
        double r = b1*b1*b2 + b1*b3 - 3*b1*b1*b1*b1 + b4;

        auto t = roots(2, -p, -2*r, r*p-0.25*q*q);
        bool qq = false;
        double s = 0;
        for (double it : t)
            if (!is_minus(2*it - p) && !is_minus(it*it - r)) {
                s = it;
                qq = true;
            }
        if (qq) {
            std::vector<double> yt;
            if (is_null(2*s - p) && is_null(s*s - r))
                result = roots(1, 0, s);
            if (is_null(2*s - p) && is_plus(s*s - r)) {
                result = roots(1, 0, s + sign(q) * std::sqrt(s*s - r));
                yt     = roots(1, 0, s - sign(q) * std::sqrt(s*s - r));
            }
            if (is_plus(2*s - p)) {
                result = roots(
                    1, -std::sqrt(2*s - p),
                    s + 0.5 * q * std::pow(2*s - p, -0.5)
                );
                yt = roots(
                    1, std::sqrt(2*s - p),
                    s - 0.5 * q * std::pow(2*s - p, -0.5)
                );
            }
            for (double j : yt)
                result.push_back(j);
            for (double & j : result)
                j += b1;
        }
    }
    return result;
    // clang-format on
}

} // namespace math
