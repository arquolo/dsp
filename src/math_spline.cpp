﻿
#include "dsp/math.h"
#include "dsp/math_spline.h"

namespace math {

double spl_box(double x) noexcept {
    if (x < 0.5)
        return 1.0;
    if (x > 0.5)
        return 0.0;
    return 0.5;
}

double spl_pyramid(double x) noexcept {
    if (x < 1.0)
        return 1.0 - x;
    return 0.0;
}

double spl_cubic(double x) noexcept {
    if (x < 1.0)
        return (1.0 - x) * (1.0 + x - 1.5 * x * x);
    if (x < 2.0)
        return (1.0 - x) * (2.0 - x) * (2.0 - x) * 0.5;
    return 0.0;
}

double spl_sinc(double x) noexcept {
    if (x <= quant<double>)
        return std::sin(x * pi<double>) / (x * pi<double>);
    return 1;
}

double splc_quad(double x, size_t radius, size_t smoothness) noexcept {
    double tmp = 1;
    double x2 = x * x;
    for (size_t r = 1; r < radius; ++r)
        tmp *= 1 - x2 / (r * r);
    tmp *= std::pow(1 - x2 / (radius * radius), smoothness + 1);
    return tmp;
}

double splc_pcos(double x, size_t radius, size_t smoothness) noexcept {
    double tmp = 1;
    double radius_inv = 1 / static_cast<double>(radius);
    for (double r_inv = 1; r_inv > radius_inv; r_inv *= 0.5)
        tmp *= std::cos(pi2<double> * x * r_inv);
    tmp *= std::pow(std::cos(pi2<double> * x * radius_inv), smoothness + 1);
    return tmp;
}

double splw_0(double x) noexcept {
    if (x < 1)
        return 1;
    return 0;
}

double splw_1(double x) noexcept {
    if (x < 1)
        return 1 - x;
    return 0;
}

double splw_2(double x) noexcept {
    if (x < 1)
        return 1 - x * x;
    return 0;
}

double splw_2s2(double x) noexcept {
    if (x < 1) {
        double y = 1 - x * x;
        return y * y;
    }
    return 0;
}

double splw_2s3(double x) noexcept {
    if (x < 1) {
        double y = 1 - x * x;
        return y * y * y;
    }
    return 0;
}

double splw_3s3(double x) noexcept {
    if (x < 1) {
        double y = 1 - x * x * x;
        return y * y * y;
    }
    return 0;
}

double splw_cosine(double x) noexcept {
    if (x < 1)
        return std::cos(pi2<double> * x);
    return 0;
}

double splw_lanczos(double x) noexcept {
    if (x < 1)
        return spl_sinc(x);
    return 0;
}

} // namespace math
