﻿
#include "dsp/unit/color/scaler.h"
#include <cmath>

namespace dsp {
namespace color {
/*-------------------------------------------------------*/
namespace w3c {

float linear(float x) noexcept {
    if (x >= 0.04045f)
        return std::pow((x + 0.055f) / 1.055f, 2.4f);
    if (x >= 0)
        return x * 1 / 12.92f;
    return 0;
}

float gamma(float x) noexcept {
    if (x >= 0.0031308f)
        return 1.055f * std::pow(x, 1 / 2.4f) - 0.055f;
    if (x >= 0)
        return x * 12.92f;
    return 0;
}

} // namespace w3c
/*-------------------------------------------------------*/
namespace cie_lab {

float linear(float x) noexcept {
    if (x >= 216.f / 24389.f)
        return std::pow(x, 1 / 3.f);
    if (x >= 0)
        return x * (841.f / 108.f) + (4.f / 29.f);
    return 0;
}

float gamma(float x) noexcept {
    if (x >= 6.f / 29.f)
        return std::pow(x, 3.f);
    if (x >= 4.f / 29.f)
        return (x - 4.f / 29.f) * (108.f / 841.f);
    return 0;
}

} // namespace cie_lab
/*-------------------------------------------------------*/
namespace cie_luv {

float linear(float x) noexcept {
    if (x >= 0.08f)
        return std::pow((x + .16f) / 1.16f, 3.f);
    if (x >= 0)
        return x * 27.f / 24389.f;
    return 0;
}

float gamma(float x) noexcept {
    if (x >= 216.f / 24389.f)
        return std::pow(x, 1 / 3.f) * 1.16f - .16f;
    if (x >= 4.f / 29.f)
        return x * 24389.f / 2700.f;
    return 0;
}

} // namespace cie_luv
/*-------------------------------------------------------*/
namespace bt709 {

float linear(float x) noexcept {
    if (x >= 0.08145f)
        return std::pow((x + 0.0993f) / 1.0993f, 1 / 0.45f);
    if (x >= 0)
        return x * (1 / 4.5f);
    return 0;
}

float gamma(float x) noexcept {
    if (x >= 0.0181f)
        return 1.0993f * std::pow(x, 0.45f) - 0.0993f;
    if (x >= 0)
        return x * 4.5f;
    return 0;
}

} // namespace bt709
/*-------------------------------------------------------*/
namespace bt2100_hlg {

float linear(float x) noexcept {
    if (x >= 0.5f)
        return std::exp((x - 1.00429347f) / 0.17883277f) + 0.02372241f;
    if (x >= 0)
        return x * x / 3;
    return 0;
}

float gamma(float x) noexcept {
    if (x >= 1 / 12.f)
        return std::log(x - 0.02372241f) * 0.17883277f + 1.00429347f;
    if (x >= 0)
        return std::sqrt(3 * x);
    return 0;
}

} // namespace bt2100_hlg
/*-------------------------------------------------------*/
namespace bt2100_pq {

float linear(float x) noexcept {
    float t = std::pow(x, 32.f / 2523.f);
    if (t >= 107.f / 128.f)
        return std::pow((128 * t - 107) / (2413 - 2392 * t), 8192.f / 1305.f);
    return 0;
}

float gamma(float x) noexcept {
    float t = std::pow(x, 1305.f / 8192.f);
    return std::pow((2413 * t + 107) / (2392 * t + 128), 2523.f / 32.f);
}

} // namespace bt2100_pq
/*-------------------------------------------------------*/
} // namespace color
} // namespace dsp
