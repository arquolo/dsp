﻿
#include "dsp/logging.h"
#include "dsp/memory.h"
#include <algorithm>

bank::bank(size_t size) : _M_data(new char[size]), _M_size(size) {
    static auto& pager = cache::_S_instance();
    pager._M_size += _M_size;
    ++pager._M_objects;
    echo_debug("%zu : %zu KiB -> new", pager._M_objects, pager._M_size >> 10);
}

bank::~bank() {
    static auto& pager = cache::_S_instance();
    pager._M_size -= _M_size;
    --pager._M_objects;
    echo_debug(
        "%zu : %zu KiB -> delete", pager._M_objects, pager._M_size >> 10);
    delete[] _M_data;
}

size_t bank::size() const noexcept {
    return _M_size;
}
bool bank::empty() const noexcept {
    return _M_size == 0;
}

/*-------------------------------------------------------*/

cache& cache::_S_instance() noexcept {
    static cache pager;
    return pager;
}

/*-------------------------------------------------------*/

resource cache::allocate(size_t size) noexcept {
    static auto& pager = _S_instance();
    static auto& rs = pager._M_resources;

    auto it1 = std::find_if(rs.begin(), rs.end(), [&size](auto const& r) {
        return r.unique() && r->size() >= size;
    });

    if (it1 != rs.end()) {
        echo_trace("->Cache::get(size_t)->move");
        return *it1;
    }

    echo_trace("->Cache::get(size_t)->allocate");
    auto it2 = std::find_if(rs.begin(), rs.end(), [&size](auto const& r) {
        return r->size() >= size;
    });

    auto r = std::make_shared<bank>(size);
    rs.insert(it2, r);
    return r;
}

void cache::_M_collect() noexcept {
    _M_resources.sort([](auto const& x1, auto const& x2) {
        return x1->size() <= x2->size();
    });
    _M_resources.remove_if(
        [this](auto const& x) { return x.unique() && _M_size > _S_max_size; });
    echo_trace("->Cache::collect()");
}
