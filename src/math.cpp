﻿
#include "dsp/math.h"
#include <random>

namespace math {

size_t floor(size_t size, size_t block_size) noexcept {
    size_t blocks = size / block_size;
    return blocks * block_size;
}

size_t ceil(size_t size, size_t block_size) noexcept {
    if (size % block_size != 0 && size != 1)
        return (size / block_size + 1) * block_size;
    return size;
}

/*-------------------------------------------------------*/

namespace {

auto& generator() noexcept {
    static std::random_device rd;
    static std::mt19937_64 gen{rd()};
    return gen;
}

} // namespace

size_t rand() noexcept {
    static auto dis = std::uniform_int_distribution<size_t>{};
    return dis(generator());
}

double rand_p() noexcept {
    static auto dis = std::uniform_real_distribution<double>(0, 1);
    return dis(generator());
}

double rand_uniform(double _disp) noexcept {
    static auto amp = 0.5 * _disp * std::sqrt(3.0);
    static auto dis = std::uniform_real_distribution<double>(-amp, amp);
    return dis(generator());
}

double rand_normal(double _disp) noexcept {
    static auto g = std::normal_distribution<double>(0, _disp);
    return g(generator());
}

} // namespace math
