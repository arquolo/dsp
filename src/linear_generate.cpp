﻿
#include "dsp/unit/linear/generate.h"

namespace dsp {
namespace blas {

tensor<float, 2> haar() noexcept
{
    tensor<float, 2> t{2, 2};
    t = static_cast<float>(M_SQRT1_2);
    *t(1)(1) *= -1;
    return t;
}

tensor<float, 2> dct(size_t dim) noexcept
{
    tensor<float, 2> t{dim, dim};
    float amp1 = std::pow(static_cast<float>(dim), -0.5f);
    float amp2 = std::pow(static_cast<float>(dim) * 0.5f, -0.5f);
    float freq = std::pow(static_cast<float>(dim) / math::pi2<float>, -1.0f);

    std::fill_n(&t(0), dim, amp1);
    for (size_t k = 1; k < dim; ++k) {
        float* p = &t(k);
        for (size_t i = 0; i < dim; ++i)
            p[i] = amp2 * std::cos(freq * (2 * i + 1) * k);
    }
    return t;
}

tensor<float, 2> walsh(size_t power) noexcept
{
    size_t dim = 1u << power;
    tensor<float, 2> res{dim, dim};

    size_t d = 1;

    res.v({0, 0, d, d}) = 1;

    tensor<float, 2> t(res);
    for (size_t p = 0; p < power; ++p) {
        res >> t.v({0, d, d, d});
        res >> t.v({d, 0, d, d});
        res >> t.v({d, d, d, d});

        t.v({d, d, d, d}) *= -1;

        d *= 2;
        res.v({0, 0, d, d});
    }
    res.v();
    return res;
}

} // namespace blas
} // namespace dsp
