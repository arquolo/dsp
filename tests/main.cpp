﻿
#include "dsp/fn_fwd.h"
#include "dsp/unit/color_fwd.h"
#include "dsp/unit/linear_fwd.h"
#include "dsp/unit/remap/resample.h"
#include "dsp/unit/remap/transpose.h"
#include "dsp/unit/shuffle.h"

namespace {
static constexpr size_t range = 256;
static constexpr size_t dims = 2;
static constexpr size_t steps = 1000;
} // namespace

using data_type = float;
using namespace dsp;

int main(int argc, char* argv[]) {
    {
        tensor<data_type, 1> t{range};
        for (size_t i = 0; i < range; ++i)
            t[i] = static_cast<data_type>(i)
                * math::norm<data_type> / static_cast<data_type>(range - 1);
        show(t);
        // t += color::bt1886<color_pass_t::gamma>(0.1f, 100.f);
        t += fn::lookup<data_type>(color::bt2100_pq::gamma);
        show(t);
        // t += color::bt1886<color_pass_t::linear>(0.1f, 100.f);
        t += fn::lookup<data_type>(color::bt2100_pq::linear);
        show(t);
    }
    /*-------------------------------------------------------*/
    {
        tensor<data_type, 1> t{range};
        for (size_t i = 0; i < range; ++i)
            t[i] = static_cast<data_type>(i)
                * math::norm<data_type> / static_cast<data_type>(range - 1);
        shuffle::anneal<data_type, target_t::max, stat_t::v1>(t);
        show(t);
    }
    /*-------------------------------------------------------*/
    {
        tensor<data_type, 1> t{range};
        for (size_t i = 0; i < range; ++i)
            t[i] = static_cast<data_type>(i)
                * math::norm<data_type> / static_cast<data_type>(range - 1);
        shuffle::anneal<data_type, target_t::min, stat_t::v2>(t);
        show(t);
    }
    /*-------------------------------------------------------*/
    {
        tensor<data_type, 1> t{range};
        for (size_t i = 0; i < range; ++i)
            t[i] = static_cast<data_type>(i)
                * math::norm<data_type> / static_cast<data_type>(range - 1);
        for (size_t i = 0; i <= 16; ++i) {
            show(t);
            shuffle::perfect(t);
        }
    }
    /*-------------------------------------------------------*/

    show(blas::dct(512));
    show(blas::eye(512));
    show(blas::walsh(9));

    /*-------------------------------------------------------*/
    {
        std::string path = argc > 1
            ? argv[1]
            : "/home/arquolo/pCloudDrive/Pictures/std/lena512.png";

        tensor<data_type, dims> t5;
        t5 << path;
        show(t5);

        tensor<data_type, dims> t5t;
        (t5 >> t5t).zero_exp(axis_0 + axis_1);
        show(t5t);
        (t5t >> t5).zero_cut(axis_0 + axis_1);
        show(t5);
        t5t.clear();

        tensor<data_type, dims> t5x;
        (t5 >> t5t).go<fn::householder, fn_type::direct>(t5.step);
        show(t5t);
        (t5t >> t5x).go<fn::householder, fn_type::inverse>(t5.step);
        show(t5x);
        echo("ssim = %f", blas::ssim(t5, t5x));
        t5x >> t5;

        t5.go<fn::haar, fn_type::direct>(t5.step);
        show(t5);
        t5.go<fn::haar, fn_type::inverse>(t5.step);
        show(t5);

        t5.go<fn::smooth>(0u, t5.step, 2u).go<fn::smooth>(1u, t5.step, 2u);
        show(t5);
        t5.go<fn::sharp>(1u, t5.step, 2u).go<fn::sharp>(0u, t5.step, 2u);
        show(t5);

        t5.go<fn::last_d>(0u, t5.step).go<fn::last_d>(1u, t5.step);
        show(t5);
        t5.go<fn::last_i>(1u, t5.step).go<fn::last_i>(0u, t5.step);
        show(t5);

        t5.go<fn::average_d>(0u, 1u, t5.step);
        show(t5);
        t5.go<fn::average_i>(0u, 1u, t5.step);
        show(t5);

        t5.go<fn::paeth_d>(0u, 1u, t5.step);
        show(t5);
        t5.go<fn::paeth_i>(0u, 1u, t5.step);
        show(t5);

        if constexpr (dims == 2) {
            remap::transpose(t5);
            show(t5);
        }

        {
            tensor<data_type, dims> fr1{t5};
            tensor<data_type, dims> fr2{};

            auto ful = fn::lookup<data_type>(color::bt2100_pq::linear);
            auto fug = fn::lookup<data_type>(color::bt2100_pq::gamma);

            (fr1 >> fr2) += ful;
            show(fr2);
            (fr2 >> fr1) += fug;
            show(fr1);

            dsp_speed_loop(steps, "Lookup") {
                (fr1 >> fr2) += ful;
                (fr2 >> fr1) += fug;
            }
            dsp_speed_end();
        }

        dsp_speed_loop(steps, "Copy") {
            (t5 >> t5t).zero_exp(axis_0 + axis_1);
            (t5t >> t5).zero_cut(axis_0 + axis_1);
        }
        dsp_speed_end();

        tensor<data_type, dims> tz6{t5.size};

        dsp_speed_loop(steps, "Householder") {
            t5.go<fn::householder, fn_type::direct>(t5.step)
                .go<fn::householder, fn_type::inverse>(t5.step);
        }
        dsp_speed_end();

        dsp_speed_loop(steps, "Haar") {
            t5.go<fn::haar, fn_type::direct>(t5.step)
                .go<fn::haar, fn_type::inverse>(t5.step);
        }
        dsp_speed_end();

        dsp_speed_loop(steps, "Last") {
            t5.go<fn::last_d>(0u, t5.step)
                .go<fn::last_d>(1u, t5.step)
                .go<fn::last_i>(1u, t5.step)
                .go<fn::last_i>(0u, t5.step);
        }
        dsp_speed_end();

        dsp_speed_loop(steps, "Smooth-Sharp") {
            t5.go<fn::smooth>(0u, t5.step, 2u)
                .go<fn::smooth>(1u, t5.step, 2u)
                .go<fn::sharp>(1u, t5.step, 2u)
                .go<fn::sharp>(0u, t5.step, 2u);
        }
        dsp_speed_end();

        dsp_speed_loop(steps, "Average") {
            t5.go<fn::average_d>(0u, 1u, t5.step)
                .go<fn::average_i>(0u, 1u, t5.step);
        }
        dsp_speed_end();

        dsp_speed_loop(steps, "Paeth") {
            t5.go<fn::paeth_d>(0u, 1u, t5.step)
                .go<fn::paeth_i>(0u, 1u, t5.step);
        }
        dsp_speed_end();

        remap::resample(t5, {768, 768}, resample_t::generic, 10, 20);
    }
    return 0;
}
