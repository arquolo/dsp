# FEATURES

## Basic data transforms

- by value:
  - [x] range conversion w/o rounding/floatization
  - [x] color conversions
  - [ ] lifting scheme
  - [ ] block based transforms - DCT, DWT, DFT
- by type
  - [ ] quantization
  - [x] rounding
  - [ ] upconversion
- by organization
  - [ ] (N+1)-dimensional array <-> N-dimensional array of chunks/strides
  - [ ] (N+1)-dimensional array <-> tree of N-dimensional arrays ("leaves")
  - [ ] linearize

## Customized data transforms

- by value
  - [ ] raw data <-> packed entropy (arithmetic, huffman, deflate)
  - [ ] color conversion with specific basis
  - [ ] swapchunk
  - [ ] (staged) karunen-loeve transform / eigen-wave transform - KLT, SKLT, EWT
  - [ ] lifting scheme with decorrelation

- etc...


