# TODO

- `engine/tensor_pair.h`
  - distinct `pair` from `pair`
  - put `pair` and `iterator` to anonymous namespace
  - add `std::begin` and `std::end` wrappers for `tensor` and `iterator`
  - put `auto operator>>(tensor& lhs, tensor& rhs)` outside of `tensor`
- `engine/worker.h`::`for_*()` and `core/tensor_pair.h`::`operator+=()`
  - add `return func`
  - try to use `auto` and lambda instead of `template<typename F>`, DO NOT use `std::function`
  - try to use iterators instead of pointer arithmetics
  - add `reverse()` wrapper for iteration in range-based loop
- `io.h`
  - use fn's
  - partial visualization
  - palettes
  - navigation by keyboard
- `fn/binary.h`
  - binary threshold filters
- `fn/matrix/common.h`
  - general filters such as Prewitt, Gauss, Canny, Median
- `fn/matrix/conv.h`
  - add area convolution
- `unit/linear/compute.h`
  - use fn's
  - add more linear operations
- `unit/linear/metricd.h`
  - hist computation
  - dynamic range keeping
  - dynamic value convertion via `pow()`
- `memory.h`
  - implement list of regions
  - implement defrag (store `bool updated` inside `block`)
- `search.h`
  - binary search - `Binary`
  - generalized search - `Amoeba/Simplex`
- `fsm.h`
  - finite state machine
- for each
  - add `static_assert` as template type check
  - integrate with `<execution>`
